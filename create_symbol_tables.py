#!ENV python

import sys
from optparse import OptionParser
from fstlib.fst import fst
from fstlib.fst.factory import FSTFactory
import os

usage = "USAGE: %prog <output_dir>"

parser = OptionParser(usage)

parser.add_option("-s", "--symbol-file", action = "store", type = "string", dest = "symbol_file", default = "cnv_symbols.txt", metavar = "FILE", 
				  help = "write symbol file to FILE")
parser.add_option("-p", "--parentheses-file", action = "store", type = "string", dest = "parens_file", default = "pdtparens.txt", metavar = "FILE", 
				  help = "write parentheses file to FILE")
parser.add_option("-n", "--nparentheses", action = "store", type = "int", dest = "nparens", default = 4, metavar = "FILE", 
				  help = "number of parentheses pairs to generate (min 4)")
parser.add_option("-a", "--alphabet", action = "store", dest = "alphabet", default = "", 
				  help = "alphabet to use if not 0:f")
parser.add_option("-x", "--separator", action="store", dest = "separator", default = None, help = "add separator")

(options, args) = parser.parse_args()

if len(args) != 1:
	parser.error("incorrect number of arguments")

outdir = args[0]

if options.alphabet == "":
	alphabet = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f']
else:
	alphabet = [s for s in options.alphabet] 

if options.separator is not None:
	alphabet.append(options.separator)

symbol_table = FSTFactory.create_symbol_table(alphabet)

try:
	with open(os.path.join(outdir, options.symbol_file), "wb") as fd:
		fd.write(symbol_table)
except IOError:
	print "ERROR: Couldn't write FST symbol file. Exiting."

if options.nparens>0:
	parens, pairs = FSTFactory.create_parentheses_tables(options.nparens)

	try:
		if options.symbol_file != "":
			with open(os.path.join(outdir, options.symbol_file), "ab") as fd:
				fd.write(parens)
		
		with open(os.path.join(outdir, options.parens_file), "wb") as fd:
			fd.write(pairs)
	except IOError:
		print "ERROR: Couldn't write FST symbol file or parentheses file. Exiting."
