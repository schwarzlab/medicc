﻿# ASCAT version 2.3, 15/07/2014
# author: Peter Van Loo
# PCF and ASPCF: Gro Nilsen
# GC correction: Jiqiu Cheng
# Python port: Roland F Schwarz (rfs32@cam.ac.uk)

from . import CnvPhyloError
import numpy as np
import pandas as pd

class ASCAT(object):
    def __init__(self, data, platform="AffySNP6"):
        self.data = data
        self.platform = platform

    def __split_genome(self):
        nprobes, _ = self.data.probe_loc.shape
        # look for gaps of more than 5Mb (arbitrary treshold to account for big centremeres or other gaps) and chromosome borders
        bigHoles = np.flatnonzero(self.data.probe_loc["position"].diff()[1:] >= 5000000) + 1
        chrBorders = np.flatnonzero(self.data.probe_loc["chr"].diff()[1:]) + 1

        holes = np.unique(np.hstack((bigHoles, chrBorders)))
        startseg = np.hstack((1,holes))
        endseg = np.hstack((holes-1, nprobes))

        self.chunks = np.array([startseg,endseg]).T


    def segment(self):
        """ main segmentation function """
        homozygous = self.predict_germline_genotypes()



    def predict_germline_genotypes(self):
        platform = self.platform

        if platform=="Custom10k":
            maxHomozygous = 0.05
            proportionHetero = 0.59
            proportionHomo = 0.38
            proportionOpen = 0.02
            segmentLength = 20
        elif platform=="Illumina109k":
            maxHomozygous = 0.05
            proportionHetero = 0.35
            proportionHomo = 0.60
            proportionOpen = 0.02
            segmentLength = 20
        elif platform=="IlluminaCytoSNP":
            maxHomozygous = 0.05
            proportionHetero = 0.28
            proportionHomo = 0.62
            proportionOpen = 0.03
            segmentLength = 100
        elif platform=="Illumina610k":
            maxHomozygous = 0.05
            proportionHetero = 0.295
            proportionHomo = 0.67
            proportionOpen = 0.015
            segmentLength = 30
        elif platform=="Illumina660k":
            maxHomozygous = 0.05
            proportionHetero = 0.295
            proportionHomo = 0.67
            proportionOpen = 0.015
            segmentLength = 30
        elif platform=="Illumina700k":
            maxHomozygous = 0.05
            proportionHetero = 0.295
            proportionHomo = 0.67
            proportionOpen = 0.015
            segmentLength = 30
        elif platform=="Illumina1M":
            maxHomozygous = 0.05
            proportionHetero = 0.22
            proportionHomo = 0.74
            proportionOpen = 0.02
            segmentLength = 100
            #previousvalues:
            #proportionHetero = 0.24
            #proportionOpen = 0.01
            #segmentLength = 60
        elif platform=="Illumina2.5M":
            maxHomozygous = 0.05
            proportionHetero = 0.21
            proportionHomo = 0.745
            proportionOpen = 0.03
            segmentLength = 100
        elif platform=="IlluminaOmni5":
            maxHomozygous = 0.05
            proportionHetero = 0.13
            proportionHomo = 0.855
            proportionOpen = 0.01
            segmentLength = 100
        elif platform=="Affy10k":
            maxHomozygous = 0.04
            proportionHetero = 0.355
            proportionHomo = 0.605
            proportionOpen = 0.025
            segmentLength = 20
        elif platform=="Affy100k":
            maxHomozygous = 0.05
            proportionHetero = 0.27
            proportionHomo = 0.62
            proportionOpen = 0.09
            segmentLength = 30
        elif platform=="Affy250k_sty":
            maxHomozygous = 0.05
            proportionHetero = 0.26
            proportionHomo = 0.66
            proportionOpen = 0.05
            segmentLength = 50
        elif platform=="Affy250k_nsp":
            maxHomozygous = 0.05
            proportionHetero = 0.26
            proportionHomo = 0.66
            proportionOpen = 0.05
            segmentLength = 50
        elif platform=="AffySNP6":
            maxHomozygous = 0.05
            proportionHetero = 0.25
            proportionHomo = 0.67
            proportionOpen = 0.04
            segmentLength = 100
        elif platform=="AffyOncoScan":
            maxHomozygous = 0.05
            proportionHetero = 0.24
            proportionHomo = 0.69
            proportionOpen = 0.04
            segmentLength = 30
        elif platform=="AffyCytoScanHD":
            maxHomozygous = 0.05
            proportionHetero = 0.26
            proportionHomo = 0.69
            proportionOpen = 0.03
            segmentLength = 30
        else:
            raise ASCATError("Error: platform unknown")


        failedarrays = []

        nprobes, nsamples = self.data.BAF.shape
        cnindices = self.data.probe_loc.ix[self.data.probe_loc["cnprobe"], "idx"]
        snpindices = self.data.probe_loc.ix[~self.data.probe_loc["cnprobe"], "idx"]
        nsnpprobes = len(snpindices)
        ncnprobes = len(cnindices)
        nhetero = proportionHetero * nsnpprobes
        nhomo = proportionHomo * nsnpprobes

        baf = self.data.BAF.mean(1) ## average over samples
        baf[baf>=0.5] = 1-baf[baf>=0.5]
        
        homo_limit = np.maximum(np.sort(baf[snpindices])[nhomo-1], maxHomozygous)
        homo_idx = np.flatnonzero(baf < homo_limit)

        diff = np.zeros(nprobes, dtype="float")
        diff[cnindices] = np.NaN

        for chr in self.data.chrs:
            idx = self.data.probe_loc.ix[(self.data.probe_loc["chr"]==chr) & (~self.data.probe_loc["cnprobe"]), "idx"]
            window_median = pd.rolling_median(baf[idx], segmentLength, center=True, min_periods=0)
            diff[idx] = np.abs(baf[idx] - window_median)

        diff_sorted = np.sort(diff[np.setdiff1d(snpindices, homo_idx)])
        if len(diff_sorted) <= nhetero:
            raise ASCATError("Not enough heterozygous probe candidates")

        het_limit = diff_sorted[nhetero-1]
        homozygous = diff < het_limit

        return(homozygous)



class ASCATError(CnvPhyloError):
    def __init__(self, message):
        super(ASCATError, self).__init__(message)
