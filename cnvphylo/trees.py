'''
Created on 26 Nov 2010

@author: rfs
'''

import commons.tools.log
import numpy
import Bio
import Bio.Phylo
import Bio.Nexus
import numbers
import copy
import sys
import tempfile
import biotools
import os
import subprocess

SS_TAXON_SEPERATOR = ","
CHR_SEPARATOR = "X"

class NeighbourJoining(object):
    """ computes a neighbour joining tree from a distance matrix """
    
    def __init__(self, distance_matrix, taxa, logger=None):
        """ constructor:
        requires distance matrix as numpy array and list of taxa 
        names in the same order as in the matrix """
        
        self.distmat = distance_matrix
        self.taxa = taxa
        self.tree = None
        self.internal_nodes = []
        if logger == None:
            self.log = commons.tools.log.Log(is_logging=True)
        else:
            self.log = logger
        self._run_nj()
    
    
    def _run_nj(self):
        """ main algorithm routine """
        
        self.log.writeln("Reconstructing tree:")
        name_clade_map = dict()
        internal_node_list = list()
        sequence_files = list(self.taxa) ## create mutable copy
        distances = self.distmat.copy()
        
        while len(sequence_files) > 2: ## do while still more than two files present
            q_matrix = self._nj_create_q_matrix(distances)
            (left, right) = self._nj_nearest_neighbours(q_matrix)
            left_name = sequence_files[left]
            right_name = sequence_files[right]
            self.log.writeln("Choosing %s with distance %f" % ((left_name, right_name), distances[left, right]))
            new_distances = self._nj_get_distances_to_new_node((left, right), distances)
            distances = self._nj_forge_new_distance_matrix((left, right), distances, new_distances)
            ## ancestor_filename = "(%s-%s)" % (left_name, right_name)
            internal_node_count = len(internal_node_list) + 1
            ancestor_filename = "internal_%d" % internal_node_count
        
            clade_ancestor = Bio.Phylo.PhyloXML.Clade(branch_length=0)
            clade_ancestor.name = ancestor_filename
            
            try:
                clade_left = name_clade_map[left_name]
            except KeyError:
                clade_left = Bio.Phylo.PhyloXML.Clade()
                clade_left.name = left_name
                name_clade_map[left_name] = clade_left
            finally:
                clade_left.branch_length = new_distances[left]
            
            try:
                clade_right = name_clade_map[right_name]
            except KeyError:
                clade_right = Bio.Phylo.PhyloXML.Clade()
                clade_right.name = right_name
                name_clade_map[right_name] = clade_right        
            finally:
                clade_right.branch_length = new_distances[right]
            
            clade_ancestor.clades = [clade_left, clade_right]
            name_clade_map[ancestor_filename] = clade_ancestor
            internal_node_list.append(clade_ancestor)
            
            sequence_files.remove(left_name)
            sequence_files.remove(right_name)
            sequence_files.append(ancestor_filename)
            
            self.log.writeln("Remaining number of sequences: %d" % len(sequence_files))
        
        ## here we have exactly 2 sequences left
        last_index = (sequence_files.index(ancestor_filename) + 1) % 2
        last_name = sequence_files[last_index]
        
        try:
            clade_last = name_clade_map[last_name]
        except KeyError:
            clade_last = Bio.Phylo.PhyloXML.Clade()
            clade_last.name = last_name
            name_clade_map[last_name] = clade_last        
        finally:
            clade_last.branch_length = new_distances[last_index]
        
        clade_ancestor.clades.append(clade_last) ## this one has 3 descendants
        
        self.tree = Bio.Phylo.PhyloXML.Phylogeny(root = clade_ancestor)
        self.internal_nodes = internal_node_list
    
    def _nj_create_q_matrix(self, D):
        """ computes the values of a neighbour-joining Q matrix out of the distance matrix D """
        Q = numpy.zeros(D.shape)
        r = D.shape[0]
        
        for i in range(0, r):
            for j in range(0, r):
                Q[i, j] =  (r - 2) * D[i, j] - sum(D[i,]) - sum(D[j, ])
        
        return Q
    
    def _nj_nearest_neighbours(self, D):
        """ returns the nearest neighbours from the distance matrix D """
        r = D.shape[0]
        the_min = D[0,1]
        min_i = 0
        min_j = 1
        for i in range(0, r):
            for j in range(i+1, r):
                if D[i,j] < the_min:
                    the_min = D[i,j]
                    min_i = i
                    min_j = j
        
        ##indices = numpy.unravel_index(numpy.argmin(D), D.shape)
    
        return (min_i, min_j)
    
    def _nj_get_distances_to_new_node(self, (left, right), D):
        r = D.shape[0]
        distances = numpy.zeros((r))
        
        ## first the dist from the joined nodes to the new node
        distances[left] = 0.5 * D[left, right] + 1.0 / (2 * (r - 2)) * ( sum(D[left,]) - sum(D[right,]) )
        distances[right] = 0.5 * D[left, right] + 1.0 / (2 * (r - 2)) * ( sum(D[right,]) - sum(D[left,]) )
    
        ## now the dist from all other nodes to the new node
        for k in range(0, r):
            if k != left and k != right:
                distances[k] = 0.5 * ( D[left, k] - distances[left] ) + 0.5 * ( D[right, k] - distances[right] )
        
        return distances 
    
    def _nj_forge_new_distance_matrix(self, (left, right), D, new_distances):
        r = D.shape[0]
        idx = numpy.repeat(True, r)
        idx[left] = False
        idx[right] = False
        
        E = D[idx][...,idx].copy()
    
        F = numpy.append(E, [new_distances[idx]], 0)
        G = numpy.append(F.transpose(), [numpy.append(new_distances[idx], [0])], 0).transpose()
        
        return G

class FitchMargoliash(object):
    """ computes a tree from a distance matrix using the FitchMargoliash method"""
    
    def __init__(self, distance_matrix, taxa, assumeClock=False, logger=None):
        """ constructor:
        requires distance matrix as numpy array and list of taxa 
        names in the same order as in the matrix. Can also be lists of numpy arrays
       and lists of lists of taxon names for multiple FM. """
        
        self.distmat = distance_matrix
        self.taxa = taxa
        ## check if we are dealing with multiple distance matrices/ taxon sets
        if isinstance(self.distmat, list):
            self.multiple=True
        else:
            self.multiple=False

        self.tree = None
        self.internal_nodes = []
        self.assumeClock = assumeClock

        if logger == None:
            self.log = commons.tools.log.Log(is_logging=True)
        else:
            self.log = logger
        self._runPhylipFM()
        
    def _runPhylipFM(self):
        """ does the tree computation """
        ## save the current working dir
        oldWorkingDir = os.getcwd()

        ## write the distance matrix to a temporary file
        ## the taxon names need to be replaced by intermediates b/c of this
        ## stupid phylip 10 character restriction and later replaced back

        tmpTaxaNames=list()
        if self.multiple:
            tmpTaxaNames = [["t%d" % i for i in range(0, len(self.taxa[j]))] for  j in range(0, len(self.taxa))]
        else:
            tmpTaxaNames = ["t%d" % i for i in range(0, len(self.taxa))]

        fdTempDistanceFile = tempfile.NamedTemporaryFile(delete=False)
        fdTempDistanceFile.writelines( biotools.to_phylip_format(self.distmat, tmpTaxaNames, sort=True) )
        fdTempDistanceFile.close()

        self.log.writeln("Temp distance file: %s" % fdTempDistanceFile.name)

        ## change working dir to temp directory
        tempDir = os.path.dirname(fdTempDistanceFile.name)
        os.chdir(tempDir)

        ## make sure all infile/outfile/outtrees are gone from dir
        if os.path.exists("./infile"):
            os.unlink("./infile")
        if os.path.exists("./outfile"):
            os.unlink("./outfile")
        if os.path.exists("./outtree"):
            os.unlink("./outtree")

        ## determine if we're dealing with multiple datasets
        nSamples=1
        if self.multiple:
            nSamples=len(self.distmat)
            assert len(self.taxa) == nSamples

        if self.assumeClock:
            cmd = "kitsch"
            self.log.write("Computing tree using Fitch-Margoliash (w/ clock) on %d sample(s)..." % nSamples)
        else:
            cmd = "fitch"
            self.log.write("Computing tree using Fitch-Margoliash (w/o clock) on %d sample(s)..." % nSamples)

        ## open process
        p = subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=os.name=="nt")
        ## enter filename
        p.stdin.write(fdTempDistanceFile.name+"\n")
        ## multiple samples
        if nSamples>1:
            p.stdin.write("m\n") ## multiple
            p.stdin.write("%d\n" % nSamples) ## number
            p.stdin.write("1\n") ## random seed
            p.stdin.write("j\n") ## disable randomization
        ## say yes
        p.stdin.write("y\n")
        res=p.communicate()
        
        if p.returncode !=0:
            raise(TreesError(res[1]))
        else:
            self.log.writeln("done!")

        ## now read the tree
        self.log.writeln("Temp tree file: %s/outtree" % os.getcwd())
        parsedTrees = Bio.Phylo.parse("outtree", format = "newick")
        self.tree = list()
        for (indT, T) in enumerate(parsedTrees):
            
            ## correct the structure such that the root is a clade with three subclades
            is_nonterminal = [not c.is_terminal() for c in T.root]
            if len(T.root) == 2:
                which_nonterminal = [i for i in range(0,len(is_nonterminal)) if is_nonterminal[i]]
                if (len(which_nonterminal)==0): ## two terminals on the root
                    raise TreesError("Phylip tree only had two taxa!")
                else: ## at least one non-terminal
                    theclade = T.root.clades.pop(which_nonterminal[0])
                    b = theclade.branch_length
                    ## divide branch lenght onto remaining branches
                    T.root.clades[0].branch_length += b
                    #theclade.clades[0].branch_length += b
                    #theclade.clades[1].branch_length += b
                    T.root.clades += theclade.clades
                    del(theclade)


            ## and correct the terminal names
            terminal_nodes = T.get_terminals()
            for node in terminal_nodes:
                if self.multiple:
                    node.name = self.taxa[indT][tmpTaxaNames[indT].index(node.name)]
                else:
                    node.name = self.taxa[tmpTaxaNames.index(node.name)]

            ## now set the non-terminal node names
            internal_nodes = T.get_nonterminals(order="level")
            internalCount = 1
            for node in internal_nodes:
                node.name = "internal_%d" % internalCount
                internalCount+=1
            self.internal_nodes.append(internal_nodes)
            self.tree.append(T)

        if not self.multiple:
            self.tree=self.tree[0]
            self.internal_nodes=self.internal_nodes[0]

        ## clean up
        os.unlink(fdTempDistanceFile.name)
        os.unlink("outtree")
        os.unlink("outfile")
        os.chdir(oldWorkingDir)
        
        

class ConsensusTree(object):
    """ builds a consensus out of multiple trees """
    def __init__(self, trees, percentage=0.5, use_branch_lengths=False, label=None):
        """ constructor """
        self.trees = trees
        self.thresh = percentage
        self.use_branch_lengths = use_branch_lengths
        self.label = label
        self.consensus = None
        self.taxa = list()
        self._sanity_checks()
        
    def _sanity_checks(self):
        """ check trees for same taxa etc. """
        
        for tree in self.trees:
            if not self.taxa:
                self.taxa = [c.name for c in tree.get_terminals()]
            else:
                not_identical = sum([t.name not in self.taxa for t in tree.get_terminals()])
                if not_identical:
                    raise ConsensusSanityError("Error: The taxa in the trees are not identical!")
                
        if len(set(self.taxa)) != len(self.taxa):
            raise ConsensusSanityError("Error: The taxa names must be unique!")
        
    def run(self):
        """ main routine """
        self._build_consensus()
        
    def _build_consensus(self):
        """ consensus function """
        
        if not len(self.trees): ## nothing to compute, no trees there
            return
        
        ## create SplitSystems
        splitSystems = list()
        for tree in self.trees:
            splitSys = SplitSystemFactory.splitsystem_from_tree(tree)
            splitSystems.append(splitSys)
        
        ## build average split system
        freqs = SplitSystemTools.table(splitSystems)
        avg = SplitSystem(self.taxa)
        for system in splitSystems:
            avg += system
        
        avg = avg / float(len(splitSystems))
        
        ## build majority-rule consensus
        SplitSystemTools.prune(avg, freqs, threshold=0.5)
        
        ## construct tree from split system
        splits = sorted(avg, key=lambda x:len(x))
        splits_temp = list()
        tree = Bio.Phylo.PhyloXML.Phylogeny()
        splitToClades = dict() ## name clade map
        internal_count = 0
        
        for split in splits:
            try:
                clade = splitToClades[split]
            except KeyError:
                clade = Bio.Phylo.PhyloXML.Clade()
                splitToClades[split] = clade
                if len(split) == 1:
                    clade.name = sorted(split)[0]
                else:
                    clade.name = "cinternal_%d" % internal_count
                    internal_count += 1
                clade.branch_length = avg.splitData[split]
                clade.confidences.append(Bio.Phylo.PhyloXML.Confidence(freqs[split], type="frequency" if not self.label else self.label))
        
            sub_splits = [s for s in splits_temp if s<split]
            sub_clades = [splitToClades[s] for s in sub_splits]
            clade.clades = sub_clades
            for s in sub_splits: splits_temp.remove(s)
            splits_temp.append(split)
        
        root = Bio.Phylo.PhyloXML.Clade(name="root", branch_length=0.0)
        for s in splits_temp:
            root.clades.append(splitToClades[s])
        
        tree.root = root
        
        self.consensus = tree 
        
class SplitSystem(object):
    """ Class for storing arbitrary split systems.
    The taxa can be any object which return a unique label upon
    calling str(.) and which can be sorted. """
    
    def __init__(self, taxa):
        self.taxa = set(taxa)
        self.splits = set()
        self.splitData = dict()
    
    def add_split(self, group1, group2, length=1):
        """ creates a new split object separating self.taxa into groups
        _set1 and _set2 """
        
        set1 = set(group1)
        set2 = set(group2)
        
        if len(group1) == 0 or len(group2) == 0:
            raise SplitSystemError("Error creating split: None of the two sets must be empty.")
        
        if not set1.isdisjoint(set2): 
            raise SplitSystemError("Error creating split: the two sets are not disjoint.")
        
        if not (set1 | set2) == self.taxa: ## union equal to domain? 
            raise SplitSystemError("Error creating split: Not all taxa covered or split taxa not in domain.") 
        
        split = Split(set1, set2)
        self.splits.add(split)
        self.splitData[split] = length
    
    def remove(self, split):
        """ removes the split object from the splitsystem """
        
        if not isinstance(split, Split):
            raise SplitSystemNotImplementedError("Error removing split: Needs to be object of class Split")
        
        self.splits.remove(split)

            
    def __str__(self):
        text = "SplitSystem of " + SS_TAXON_SEPERATOR.join(sorted(self.taxa)) + "\n"
        text += "\n".join(["%s: %f" %(str(s), self.splitData[s]) for s in self.splits])
        
        return text
    
    def __iter__(self):
        return self.splits.__iter__()
    
    def __mul__(self, scalar):
        """ multiplication of splitsystems, only implemented for scalars """
        if not isinstance(scalar, numbers.Number):
            raise SplitSystemNotImplementedError("Error: SplitSystem multiplication only implemented for scalar values")
        
        newsys = copy.deepcopy(self)
        for key in newsys.splitData:
            newsys.splitData[key]*=scalar
            
        return newsys
            
    def __div__(self, scalar):
        """ division of splitsystems, only implemented for scalars """
        if not isinstance(scalar, numbers.Number):
            raise SplitSystemNotImplementedError("Error: SplitSystem division only implemented for scalar values")
            
        return self.__mul__(1 / scalar)
    
    def __add__(self, other):
        """ addition of splitsystem with either scalar or other splitsystem """
        if not isinstance(other, numbers.Number) and not isinstance(other, SplitSystem):
            raise SplitSystemNotImplementedError("Error: SplitSystem addition only implemented for scalars and other SplitSystems.")
        
        newsys = copy.deepcopy(self)        
        if isinstance(other, numbers.Number):
            for key in newsys.splitData:
                newsys.splitData[key]+=other    
        else:
            for split in other.splits:
                newsys.splits.add(split)
                try:
                    newsys.splitData[split] += other.splitData[split]
                except KeyError:
                    newsys.splitData[split] = other.splitData[split]
                
        return newsys
            
        
class Split(object):
    def __init__(self, set1, set2):
        """ constructor for split, set1 and set2 must be sets """
        if len(set1) < len(set2):
            self._setLeft = set1
            self._setRight = set2
        elif len(set1) > len(set2):
            self._setLeft = set2
            self._setRight = set1
        else: ## equal size
            first = sorted(set1 | set2)[0] ## first element of sorted union
            if first in set1:
                self._setLeft = set1
                self._setRight = set2
            else:
                self._setLeft = set2
                self._setRight = set1


    def get_group(self):
        return self._setLeft.copy()
    
    def __len__(self):
        return len(self._setLeft)
    
    def __iter__(self):
        return self._setLeft.__iter__()
            
    def __eq__(self, other):
        return self._setLeft.__eq__(other.get_group())
    
    def __str__(self):
        ## we need to determine a unique ordering of set elements
        ## and transform it into a string
            
        text = SS_TAXON_SEPERATOR.join([str(t) for t in sorted(self._setLeft)]) +\
        "|" + SS_TAXON_SEPERATOR.join([str(t) for t in sorted(self._setRight)])
        return text    
        
    def __hash__(self):
        return self.__str__().__hash__()
    
    def __lt__(self, other):
        return self._setLeft.__lt__(other.get_group())
    
    def __le__(self, other):
        return self._setLeft.__le__(other.get_group())
    
    def __ne__(self, other):
        return self._setLeft.__ne__(other.get_group())
    
    def __gt__(self, other):
        return self._setLeft.__gt__(other.get_group())
    
    def __ge__(self, other):
        return self._setLeft.__ge__(other.get_group())
    
class TreeTools(object):
    """ Utility methods for trees """
    
    @classmethod
    def to_distance_matrix(cls, tree, sort=False):
        """Create a distance matrix (NumPy array) from terminals in tree """
        
        terminals = tree.get_terminals()
        names = [t.name for t in terminals]
        
        if sort and len(names) != len(set(names)):
            raise TreesError("ERROR: TreeTools: Requested sort but names are not unique!")
        
        if sort:
            names.sort()
            terminals.sort(key=lambda x:x.name)
            
        distmat = numpy.zeros((len(terminals), len(terminals)))
        for i in terminals:
            for j in terminals:
                distmat[terminals.index(i), terminals.index(j)] =  tree.distance(i, j)
        return (distmat, names)

    @classmethod
    def combine_trees(cls, *trees):
        """ makes one tree out of several by adding the branch lengths. Trees need to have identical topologies. """
        
        result = None

        for tree in trees:
            if not result:
                result = Bio.Phylo.BaseTree.copy.deepcopy(tree)
            else:
                for clade in result.find_clades():
                    search = [c for c in tree.find_clades(name=clade.name)]
                    if len(search) != 1:
                        print >> sys.stderr, "ERROR: Trees do not have identical topologies."
                        return None
                    
                    clade.branch_length += search[0].branch_length
                    
                    ## add confidence scores (ambiguities)
                    if len(clade.confidences) == len(search[0].confidences):
                        for i in range(0, len(clade.confidences)):
                            clade.confidences[i].value += search[0].confidences[i].value

                    ## concatenate sequences if found or add them
                    for seq in search[0].sequences:
                        matches = filter(lambda x:x.accession.value == seq.accession.value, clade.sequences)
                        if len(matches):
                            for m in matches:
                                m.name += (CHR_SEPARATOR + seq.name)
                        else:
                            clade.sequences.append(Bio.Phylo.BaseTree.copy.deepcopy(seq))
                    
            
        return result

class SplitSystemTools(object):
    """ Utility methods for SplitSystems """
    
    @classmethod
    def table(cls, splitsystems, freqs=True):
        """ Returns a dictionary that contains the union of all splits in the
        given SplitSystems and a count how often each split appears. Computes counts
        or frequencies """
        
        counts = dict()
        
        for system in splitsystems:
            for split in system:
                try:
                    counts[split] += 1
                except KeyError:
                    counts[split] = 1
        
        if freqs:
            for split in counts:
                counts[split] /= float(len(splitsystems))
        
        return counts
    
    @classmethod
    def prune(cls, splitsystem, freqs, threshold=0.5):
        """ Removes all splits from a splitsystem whose frequencies are 
        below or equal to a certain threshold. In place. """
        
        for split in splitsystem.splits.copy():
            freq = 0
            try:
                freq = freqs[split]
            except KeyError:
                pass
            if freq <= threshold:
                splitsystem.remove(split)
                

                
class SplitSystemFactory(object):
    """ Factory class for split systems """
    
    @classmethod
    def splitsystem_from_tree(cls, tree):
        taxa = [t.name for t in tree.get_terminals()]
        if len(taxa) != len(set(taxa)):
            raise SplitSystemSanityError("Error: Taxa not unique!")
        
        splitSys = SplitSystem(taxa)
        ## add singleton splits
        for extnode in tree.get_terminals():
            splitSys.add_split([extnode.name,], set(taxa) - set([extnode.name,]), extnode.branch_length)
            
        ## add internal splits
        for intnode in tree.get_nonterminals():
            tree.root_with_outgroup(intnode)
            if len(tree.root.clades) != 3:
                raise ConsensusSanityError("Error: Internal root node %s does not have 3 children" % tree.root.name)
            
            termA = tree.root.clades[0].get_terminals()
            lengthA = tree.root.clades[0].branch_length
            termB = tree.root.clades[1].get_terminals()
            lengthB = tree.root.clades[1].branch_length
            termC = tree.root.clades[2].get_terminals()
            lengthC = tree.root.clades[2].branch_length
            
            splitSys.add_split([t.name for t in termA], [t.name for t in (termB+termC)], lengthA if lengthA else 0.0) 
            splitSys.add_split([t.name for t in termB], [t.name for t in (termA+termC)], lengthB if lengthB else 0.0)
            splitSys.add_split([t.name for t in termC], [t.name for t in (termA+termB)], lengthC if lengthC else 0.0)

        return splitSys
                
              
        
            


class TreesError(Exception):
    def __init__(self, message):
        self.msg = message

class SplitSystemError(TreesError):
    def __init__(self, message):
        super(SplitSystemError, self).__init__(message)

class SplitSystemNotImplementedError(SplitSystemError):
    def __init__(self, message):
        super(SplitSystemNotImplementedError, self).__init__(message)

class SplitSystemSanityError(SplitSystemError):
    def __init__(self, message="Sanity error"):
        super(SplitSystemSanityError, self).__init__(message)
            
class ConsensusError(TreesError):
    def __init__(self, message):
        super(ConsensusError, self).__init__(message)

class ConsensusSanityError(ConsensusError):
    def __init__(self, message="Sanity error"):
        super(ConsensusSanityError, self).__init__(message)
    
    
    