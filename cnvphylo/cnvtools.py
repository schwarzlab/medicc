'''
Created on 26 Nov 2010

@author: rfs

'''
from commons.tools.log import Log
import os
import subprocess
from subprocess import Popen 
import shlex
import tempfile
from fstlib.fst.factory import FSTFactory
from fstlib.fst.fst import Fst 
import sys
from Bio import SeqIO, Phylo
from subprocess import CalledProcessError
import numpy
import scipy
from StringIO import StringIO
import fstlib.fst.io 
import fstlib.fst.algos
from cnvphylo import *
import threading
import shutil
import csv
import glob
import Bio.SeqIO
import platform
from fstlib.fst.openfst import *

HOME = os.getenv("HOME")

class CnvToolBox(OpenFST):
	""" Toolbox next version for doing lots of basic things with CNV FSTs """
	
	def __init__(self, workingDir, configuration, logger=None):
		""" constructor """
		if not isinstance(configuration, CnvBaseConfig):
			raise CnvToolsError("Need configuration object of class CnvBaseConfig")

		self.config = configuration
		self.workingDir = workingDir
		super(CnvToolBox, self).__init__(self.config.alphabet, self.config.symbol_file, self.config.pdt_parens_file)

		if logger==None:
			self.log = Log(is_logging=True)
		else:
			self.log = logger

	def mass_intersect_quick(self, inputfsts, intersect_filename, prune_weight = -1, prune_level = 0, detmin=True):
		""" intersects a number of acceptors """
		
		filename = self.bmulticommand(COMMAND_INTERSECT, inputfsts, prune_weight, prune_level, 
									  detmin=detmin, sort=SORT_FIRST, return_filename=True )
		shutil.move(filename, "%s/%s.bin" % (self.workingDir, intersect_filename))

	def _input_binary(self, param):
		""" either reads file if file handle or returns input. In any case the output
		is a binary transducer or None """
		result = None
		
		if isinstance(param, file) and not param.closed:
			result = param.read()
		else:
			try:
				if os.path.exists(param): ## is absolute or relative path
					with open(param, "rb") as fd:
						result = fd.read()
				else: ## could be a filename
					path = "%s/%s.bin" % (self.workingDir, param)
					if os.path.exists(path): ## let's see if it exists now...
						with open(path, "rb") as fd:
							result = fd.read()
			except TypeError: ## probably a binary fst
				result = param
			
		return result

	def _input_filename(self, param):
		""" either writes file if param is binary or returns input if filename. In any case the output
		is a filename. It's in the responsibility of the caller to delete it if necessary."""
		
		filename = None
		if isinstance(param, file):
			filename = param.name
		else:
			try:
				if os.path.exists(param): ## is absolute or relative path
					filename = param
				else: ## could be a filename
					path = "%s/%s.bin" % (self.workingDir, param)
					if os.path.exists(path): ## let's see if it exists now...
						filename = path
			except TypeError: ## probably a binary fst
				with tempfile.NamedTemporaryFile(delete=False) as fd_tmp:                
					fd_tmp.write(param)
					filename = fd_tmp.name
		
		return filename

	def identify_diploid(self, diploid):
		if not os.path.exists("%s/%s.bin" % (self.workingDir, diploid)):
			candidates = glob.glob("%s/*diploid*fst*" % self.workingDir)
			if len(candidates) == 0:
				raise CnvToolsError("No diploid found and nothing looked like one (filename should contain 'diploid')!")
			else:
				self.log.writeln("No diploid found, but %s looks like one!" % candidates[0])
				return os.path.basename(candidates[0]).split(".")[0] ## first part of filename before dot and without path
		else:
			self.log.writeln("Diploid is as defined: %s" % diploid)
			return diploid

		
	def write_tree_files(self, tree, filename, output_dir=None, write_newick=True, 
						 write_nexus=False, write_xml=True, write_drawing=True, write_picture=True):
		outdir = output_dir if output_dir else self.workingDir
		if write_xml:
			with open("%s/%s.xml" % ( outdir , filename), "w") as fd:
				Phylo.write(tree, fd, "phyloxml")

		if write_newick:
			with open("%s/%s.new" % ( outdir, filename), "w") as fd:
				Phylo.write(tree, fd, "newick", branch_length_only=True)
		
		if write_nexus:
			with open("%s/%s.nex" % ( outdir, filename), "w") as fd:
				Phylo.write(tree, fd, "nexus")
		
		if write_drawing:            
			drawing = StringIO()
			Phylo.draw_ascii(tree, file = drawing)
			with open("%s/%s.graph" % (outdir, filename), "w") as fd:
				fd.write(drawing.getvalue())
			drawing.close()
			
		#        import pylab
		#        Phylo.draw_graphviz(tree)
		#        pylab.show()
		#        pylab.savefig('apaf.png')

	def sum_sequences(self, fst1, fst2):
		""" sums the CNs of two sequences
		can be filenames, or binary data. """
		
		fstobj1 = fst.io.OpenFstTextReader(StringIO(self.bprint(fst1))).create_fst()
		fstobj2 = fst.io.OpenFstTextReader(StringIO(self.bprint(fst2))).create_fst()
		
		path1 = fst.algos.PathDepthFirstSearchNew(fstobj1).get_paths().next()
		path2 = fst.algos.PathDepthFirstSearchNew(fstobj2).get_paths().next()
		
		textsequence1 = [t.symbol_from for t in path1]
		textsequence2 = [t.symbol_from for t in path2]
		
		result = self.sum_text_sequences(textsequence1, textsequence2)

		fstobj_sum = fst.factory.FSTFactory.create_fixed_length_hmm_from_sequences([result,], self.config.alphabet, pseudo_counts=0) 
		fstobj_bin = self.bcompile(fstobj_sum, acceptor=True)
		
		return fstobj_bin
		
	def sum_text_sequences(self, textsequence1, textsequence2):
		""" sums two textsequences """
		
		maxval = max([int(s, 16) for s in self.config.alphabet])
		retval = list()
		for s1, s2 in zip(textsequence1, textsequence2):
			try:
				val1 = int(s1, 16)
				val2 = int(s2, 16)
				result = hex(min( val1 + val2, maxval ))[2:]
			except ValueError:
				if s1 == s2:
					result = s1
				else:
					raise CnvToolsError("Error adding sequences: %s and %s not \
						  identical" % (s1, s2))
			retval.append(result)
				
		#result = [hex(min( int(s1, 16) + int(s2, 16), maxval ))[2:] for s1, s2 in zip(textsequence1, textsequence2)]
		
		return retval
			

class CNVAligner(object):
	""" Toolbox next version for doing lots of basic things with CNV FSTs """
	
	def __init__(self, workingDir, configuration, path_to_alignment_fst, logger=None, asymmetric=False):
		""" constructor """
		if not isinstance(configuration, CnvBaseConfig):
			raise CnvToolsError("Need configuration object of class CnvBaseConfig")
			
		self.config = configuration
		self.fstpath = path_to_alignment_fst
		if logger==None:
			self.log = Log(is_logging=True)
		else:
			self.log = logger
		self.cnvtools = CnvToolBox(workingDir, configuration, logger=self.log)
		self.workingDir = workingDir
		self.asymmetric = asymmetric
	
	def project(self, filename, detweight=-1, minimize = True, lazy = False, from_right=False, project=True, rmweights=False):
		""" composes the given sequence with the aligner and projects to output (unless project=false)"""
		
		self.log.write("Composing " + filename + " with aligner " + self.fstpath + "...")
		if not os.path.isabs(filename):
			composed_filename = self.workingDir + "/" + filename + "_edit.bin"
		else:
			composed_filename = filename + "_edit.bin"
		
		if not lazy or not os.path.exists(self.workingDir + "/" + composed_filename + ".bin"):
			p = []  
			if not from_right: ## build SEQ o ED, project to output
				p.append( Popen(shlex.split("fstarcsort --sort_type='olabel' %s" 
											% self.cnvtools._input_filename(filename), posix=not IS_WINDOWS), stdout = subprocess.PIPE, 
											stderr=subprocess.PIPE, shell=platform.system()=="Windows") )
				p.append( Popen(shlex.split("fstcompose - %s" 
											% self.fstpath, posix=not IS_WINDOWS ), stdout = subprocess.PIPE, stdin = p[-1].stdout, 
											stderr=subprocess.PIPE, shell=platform.system()=="Windows") )
				if project:
					p.append( Popen(shlex.split("fstproject --project_output", posix=not IS_WINDOWS), 
								stdout = subprocess.PIPE, stdin = p[-1].stdout, stderr=subprocess.PIPE, shell=platform.system()=="Windows") )
			else: ## build ED o SEQ, project to input
				p.append( Popen(shlex.split("fstarcsort --sort_type='ilabel' %s" 
											% self.cnvtools._input_filename(filename), posix=not IS_WINDOWS), stdout = subprocess.PIPE, 
											stderr=subprocess.PIPE, shell=platform.system()=="Windows") )
				p.append( Popen(shlex.split("fstcompose %s -" 
											% self.fstpath, posix=not IS_WINDOWS ), stdout = subprocess.PIPE, stdin = p[-1].stdout, 
											stderr=subprocess.PIPE, shell=platform.system()=="Windows") )
				if project:
					p.append( Popen(shlex.split("fstproject", posix=not IS_WINDOWS), 
									stdout = subprocess.PIPE, stdin = p[-1].stdout, stderr=subprocess.PIPE, shell=platform.system()=="Windows") )

			if minimize and project: ## can't minimize if not projecting
				p.append( Popen(shlex.split("fstdeterminize" + (" --weight=%f" % (detweight) if detweight >=0 else ""), posix=not IS_WINDOWS)
											, stdout = subprocess.PIPE, stdin = p[-1].stdout, stderr=subprocess.PIPE, shell=platform.system()=="Windows"))
				if detweight>=0:
					self.log.write("detweight=%f..." % detweight)
				p.append( Popen(shlex.split("fstminimize", posix=not IS_WINDOWS), stdout = subprocess.PIPE, stdin = p[-1].stdout, 
								stderr=subprocess.PIPE, shell=platform.system()=="Windows") )
				
			if rmweights:
				p.append( Popen(shlex.split("fstmap --map_type='rmweight'", posix=not IS_WINDOWS), stdout = subprocess.PIPE, 
								stdin = p[-1].stdout, stderr=subprocess.PIPE, shell=platform.system()=="Windows") )
				
				
			result = p[-1].communicate()
			_cleanup(p)
			if p[-1].returncode==0:
				with open(composed_filename, "wb") as fd:
					fd.write(result[0])
				self.log.writeln("done!")
			else:
				self.log.writeln("failed!")
				raise CnvToolsError(result[1])

		else:
			self.log.writeln("skipped!")
		
		return composed_filename

	def projectPDT(self, filename, detweight=-1, minimize = True, from_right=False, project=True, rmweights=False):
		""" composes the given sequence with the aligner and projects to output (unless project=false) """
		
		self.log.write("Composing " + filename + " with aligner " + self.fstpath + "...")
		if not os.path.isabs(filename):
			composed_filename = self.workingDir + "/" + filename + "_edit.bin"
		else:
			composed_filename = filename + "_edit.bin"
		
		p = []

		if not from_right: ## build SEQ o ED, project to output
			p.append( Popen(shlex.split("fstarcsort --sort_type='ilabel' %s" 
										% self.fstpath, posix=not IS_WINDOWS), stdout = subprocess.PIPE, 
										stderr=subprocess.PIPE, shell=platform.system()=="Windows") )
			p.append( Popen(shlex.split("pdtcompose --left_pdt=true --pdt_parentheses=%s %s -" 
										% (self.config.pdt_parens_file, self.cnvtools._input_filename(filename)), posix=not IS_WINDOWS ), stdout = subprocess.PIPE, stdin = p[-1].stdout, 
										stderr=subprocess.PIPE, shell=platform.system()=="Windows") )
			if project:
				p.append( Popen(shlex.split("fstproject --project_output", posix=not IS_WINDOWS), 
							stdout = subprocess.PIPE, stdin = p[-1].stdout, stderr=subprocess.PIPE, shell=platform.system()=="Windows") )

		else: ## build ED o SEQ, project to input
			p.append( Popen(shlex.split("fstarcsort --sort_type='olabel' %s" 
										% self.fstpath, posix=not IS_WINDOWS), stdout = subprocess.PIPE, 
										stderr=subprocess.PIPE, shell=platform.system()=="Windows") )
			p.append( Popen(shlex.split("pdtcompose --left_pdt=false --pdt_parentheses=%s - %s" 
										% (self.config.pdt_parens_file, self.cnvtools._input_filename(filename)), posix=not IS_WINDOWS ), stdout = subprocess.PIPE, stdin = p[-1].stdout, 
										stderr=subprocess.PIPE, shell=platform.system()=="Windows") )
			if project:
				p.append( Popen(shlex.split("fstproject", posix=not IS_WINDOWS), 
								stdout = subprocess.PIPE, stdin = p[-1].stdout, stderr=subprocess.PIPE, shell=platform.system()=="Windows") )

		if minimize and project: ## can't minimize if not projecting
			p.append( Popen(shlex.split("fstdeterminize" + (" --weight=%f" % (detweight) if detweight >=0 else ""), posix=not IS_WINDOWS), stdout = subprocess.PIPE, stdin = p[-1].stdout, stderr=subprocess.PIPE, shell=platform.system()=="Windows"))
			if detweight>=0:
				self.log.write("detweight=%f..." % detweight)
			p.append( Popen(shlex.split("fstminimize", posix=not IS_WINDOWS), stdout = subprocess.PIPE, stdin = p[-1].stdout, stderr=subprocess.PIPE, shell=platform.system()=="Windows") )
		
		if rmweights:
			p.append( Popen(shlex.split("fstmap --map_type='rmweight'", posix=not IS_WINDOWS), stdout = subprocess.PIPE, stdin = p[-1].stdout, stderr=subprocess.PIPE, shell=platform.system()=="Windows") )


		result = p[-1].communicate()
		_cleanup(p)
		if p[-1].returncode==0:
			with open(composed_filename, "wb") as fd:
				fd.write(result[0])
			self.log.writeln("done!")
		else:
			self.log.writeln("failed!")
			raise CnvToolsError(result[1])

		return composed_filename

	def align_pairwise(self, filenames):
		""" computes pairwise distances between the files in filenames and returns the result as numpy array. """
		
		distances = numpy.zeros((len(filenames), len(filenames)))
		
		self.log.write("Computing pairwise distances...")
		for i in range(0, len(filenames)):
			for j in range(0, len(filenames)):
				if self.asymmetric or j>i: ## compute distances only for half diagonal if symmetric
					f1 = self.cnvtools._input_filename(filenames[i])
					f2 = self.cnvtools._input_filename(filenames[j])
					p1 = Popen(shlex.split("%s %s %s %s" % 
										   (ALIGN_BINARY_STD, 
											self.fstpath, 
											f1, f2), posix=not IS_WINDOWS)
										   , stdout=subprocess.PIPE, stderr = subprocess.PIPE)
					result = p1.communicate()
					_cleanup(p1)
					if p1.returncode!=0:
						self.log.writeln("failed!")
						raise CnvToolsError(result[1])
					
					distance = float(result[0])
				
					distances[i,j] = distance
					if not self.asymmetric:
						distances[j,i] = distance
	
		self.log.writeln("done!")
		return distances

	def align_pairwise_pdt(self, filenames):
		""" computes pairwise distances between the files in filenames and returns the result as numpy array. 
		Assumes the sequence files are all PDTs. """
		
		distances = numpy.zeros((len(filenames), len(filenames)))
		
		self.log.write("Computing pairwise distances from PDTs...")
		for i in range(0, len(filenames)):
			for j in range(i+1, len(filenames)):
				f1 = self.cnvtools._input_filename(filenames[i])
				f2 = self.cnvtools._input_filename(filenames[j])
				p=[]
				p.append( Popen(shlex.split("fstarcsort --sort_type='olabel' %s" %  f1, posix=not IS_WINDOWS), stdout = subprocess.PIPE, stderr=subprocess.PIPE, shell=platform.system()=="Windows"))
				p.append( Popen(shlex.split("pdtcompose --pdt_parentheses=%s - %s" %  (self.config.pdt_parens_file, self.fstpath), posix=not IS_WINDOWS), stdout = subprocess.PIPE, stdin = p[-1].stdout, stderr=subprocess.PIPE, shell=platform.system()=="Windows"))
				p.append( Popen(shlex.split("fstproject --project_output", posix=not IS_WINDOWS), stdout = subprocess.PIPE, stdin = p[-1].stdout, stderr=subprocess.PIPE, shell=platform.system()=="Windows"))
				p.append( Popen(shlex.split("pdtexpand --pdt_parentheses=%s" %  self.config.pdt_parens_file, posix=not IS_WINDOWS), stdout = subprocess.PIPE, stdin = p[-1].stdout, stderr=subprocess.PIPE, shell=platform.system()=="Windows"))
				p.append( Popen(shlex.split("fstarcsort --sort_type='olabel'", posix=not IS_WINDOWS), stdout = subprocess.PIPE, stdin = p[-1].stdout, stderr=subprocess.PIPE, shell=platform.system()=="Windows"))
				p.append( Popen(shlex.split("pdtcompose --left_pdt=false --pdt_parentheses=%s - %s" %  (self.config.pdt_parens_file, f2), posix=not IS_WINDOWS), stdout = subprocess.PIPE, stdin = p[-1].stdout, stderr=subprocess.PIPE, shell=platform.system()=="Windows"))
				p.append( Popen(shlex.split("pdtshortestpath --pdt_parentheses=%s" %  self.config.pdt_parens_file, posix=not IS_WINDOWS), stdout = subprocess.PIPE, stdin = p[-1].stdout, stderr=subprocess.PIPE, shell=platform.system()=="Windows"))
				p.append( Popen(shlex.split("fstdeterminize", posix=not IS_WINDOWS), stdout = subprocess.PIPE, stdin = p[-1].stdout, stderr=subprocess.PIPE, shell=platform.system()=="Windows"))
				p.append( Popen(shlex.split("fstshortestdistance --reverse=true", posix=not IS_WINDOWS), stdout = subprocess.PIPE, stdin = p[-1].stdout, stderr=subprocess.PIPE, shell=platform.system()=="Windows"))
				result = p[-1].communicate()
				_cleanup(p)
				if p[-1].returncode!=0:
					self.log.writeln("failed!")
					raise CnvToolsError(result[1])
					
				distance = float(StringIO(result[0]).next().strip("\n").split("\t")[1])
				
				distances[i,j] = distance
				distances[j,i] = distance
	
		self.log.writeln("done!")
		return distances
	
	def find_closest_target_pdt(self, source, target):
		""" finds the closest target sequence from source using the given transducer.
	   Target is a PDT. """
		
		value = -1
		self.log.write("Computing new distances...")
		p=[]
		p.append( Popen(shlex.split("fstcompose %s %s" %  (self.cnvtools._input_filename(source), self.fstpath), posix=not IS_WINDOWS)
							, stdout = subprocess.PIPE, stderr=subprocess.PIPE, shell=platform.system()=="Windows"))
		p.append( Popen(shlex.split("pdtcompose --left_pdt=false --pdt_parentheses=%s - %s" %  (self.config.pdt_parens_file, self.cnvtools._input_filename(target)), posix=not IS_WINDOWS)
							, stdout = subprocess.PIPE, stdin = p[-1].stdout, stderr=subprocess.PIPE, shell=platform.system()=="Windows"))
		p.append( Popen(shlex.split("pdtshortestpath --pdt_parentheses=%s" %  self.config.pdt_parens_file, posix=not IS_WINDOWS)
							, stdout = subprocess.PIPE, stdin = p[-1].stdout, stderr=subprocess.PIPE, shell=platform.system()=="Windows"))
		p.append( Popen(shlex.split("fstproject --project_output", posix=not IS_WINDOWS)
							, stdout = subprocess.PIPE, stdin = p[-1].stdout, stderr=subprocess.PIPE, shell=platform.system()=="Windows"))
		p.append( Popen(shlex.split("pdtexpand --pdt_parentheses=%s" %  self.config.pdt_parens_file, posix=not IS_WINDOWS)
							, stdout = subprocess.PIPE, stdin = p[-1].stdout, stderr=subprocess.PIPE, shell=platform.system()=="Windows"))
		p.append( Popen(shlex.split("fstrmepsilon", posix=not IS_WINDOWS)
							, stdout = subprocess.PIPE, stdin = p[-1].stdout, stderr=subprocess.PIPE, shell=platform.system()=="Windows"))
		p.append( Popen(shlex.split("fstdeterminize", posix=not IS_WINDOWS)
							, stdout = subprocess.PIPE, stdin = p[-1].stdout, stderr=subprocess.PIPE, shell=platform.system()=="Windows"))
		p.append( Popen(shlex.split("fstminimize", posix=not IS_WINDOWS)
							, stdout = subprocess.PIPE, stdin = p[-1].stdout, stderr=subprocess.PIPE, shell=platform.system()=="Windows"))
		
		result = p[-1].communicate()
		_cleanup(p)
		if p[-1].returncode == 0:
			self.log.writeln("done!")
			return result[0]
		else:
			self.log.writeln("failed!")
			raise CnvToolsError(result[1])

	def align_pair(self, source, target, shortest_path = False):
		""" computes shortest distance (or shortest path if shortest_path=True) from the source file to the target file """
		
		value = -1
		self.log.write("Computing new distances...")
		p1 = Popen(shlex.split("%s %s %s %s" % 
							   (SP_BINARY if shortest_path else ALIGN_BINARY_STD, 
								self.fstpath, 
								self.cnvtools._input_filename(source), 
								self.cnvtools._input_filename(target)), posix=not IS_WINDOWS)
							   , stdout=subprocess.PIPE, stderr = subprocess.PIPE, shell = IS_WINDOWS)
		
		result = p1.communicate()
		_cleanup(p1)
		if p1.returncode == 0:
			self.log.writeln("done!")
			if shortest_path:
				return result[0]
			else:
				try:
					value = float(result[0])
				except ValueError:
					value = -1
					self.log.writeln("WARNING: No result returned when aligning %s to %s!" % (source, target))
				return value
		else:
			self.log.writeln("failed!")
			raise CnvToolsError(result[1])

	def balign_pair_full(self, source, target):
		""" computes full composition of source o aligner o target """
		
		self.log.write("Composing with aligner...")
		p=[]
		p.append(Popen(shlex.split("fstarcsort --sort_type='olabel' %s/%s.bin" % (self.workingDir, source), posix=not IS_WINDOWS), 
				   stdout=subprocess.PIPE, stderr = subprocess.PIPE, shell=platform.system()=="Windows"))
		p.append(Popen(shlex.split("fstcompose - %s" % self.fstpath, posix=not IS_WINDOWS), 
				   stdin = p[-1].stdout, stdout=subprocess.PIPE, stderr = subprocess.PIPE, shell=platform.system()=="Windows"))
		p.append(Popen(shlex.split("fstarcsort --sort_type='olabel'", posix=not IS_WINDOWS), 
				   stdin = p[-1].stdout, stdout=subprocess.PIPE, stderr = subprocess.PIPE, shell=platform.system()=="Windows"))
		p.append(Popen(shlex.split("fstcompose - %s/%s.bin" % (self.workingDir, target), posix=not IS_WINDOWS), 
				   stdin = p[-1].stdout, stdout=subprocess.PIPE, stderr = subprocess.PIPE, shell=platform.system()=="Windows"))
		
		result = p[-1].communicate()
		_cleanup(p)
		if p[-1].returncode == 0:
			self.log.writeln("done!")
			return result[0]
		else:
			self.log.writeln("failed!")
			raise CnvToolsError(result[1])


		
class CnvToolsError(CnvPhyloError):
	""" Exception class for this module """ 
	def __init__(self, message):
		super(CnvToolsError, self).__init__(message)


def _cleanup(p):
	if isinstance(p, subprocess.Popen):
		p = [p,]
	for q in p:
		if not q.stdin == None:
			q.stdin.close()
		if not q.stdout == None:
			q.stdout.close()
		if not q.stderr == None:
			q.stderr.close()
