'''
Created on 31 Mar 2011

@author: rfs
'''

import cnvphylo
import commons.tools.log
import Bio.SeqIO
import fstlib.fst
import fstlib.fst.factory
import fstlib.fst.io
import fstlib.fst.openfst
from fstlib.fst.fst import *
import os
from subprocess import Popen, CalledProcessError
import subprocess
import shlex
from cnvphylo.cnvtools import CNVAligner, CnvToolBox, COMMAND_UNION, COMMAND_INTERSECT, CnvToolsError
import Bio.Phylo
import numpy
import tempfile
import StringIO
from optparse import OptionParser
import threading
import Queue
import time
from cnvphylo import *
from cnvphylo.trees import TreeTools, TreesError
import cnvphylo.biotools 
import shutil
import collections
from fstlib.fst.factory import FSTFactory
import glob

DISTANCE_FILENAME_A = "allele_A.dist"
DISTANCE_FILENAME_B = "allele_B.dist"
DISTANCE_FILENAME_PAIRWISE = "pairwise.dist"
DISTANCE_FILENAME_PAIRWISE_DIRECT = "pairwise_direct.dist"
DISTANCE_FILENAME_PAIRWISE_SUMMED = "pairwise_summed.dist"
DISTANCE_FILENAME_ADDITIVE = "additive.dist"
DISTANCE_FILENAME_ADDITIVE_SUMMED = "additive_summed.dist"
DISTANCE_FILENAME_FINAL = "tree_final.dist"
NJ_TREE_BASENAME = "tree_nj"
FITCH_TREE_BASENAME = "tree_fitch"
DIPLOID_NAME = "diploid"
TREE_FILENAME_A = "tree_A"
TREE_FILENAME_B = "tree_B"
TREE_FILENAME_COMBINED = "tree"
TREE_FILENAME_CONSENSUS = "consensus_tree"
TREE_FILENAME_PAIRWISE_SUMMED = "pairwise_summed_tree"
TREE_FILENAME_ADDITIVE_SUMMED = "additive_summed_tree"
TREE_FILENAME_FINAL = "tree_final"
NETWORK_FILENAME_CONSENSUS = "consensus_network"
CENTER_FILENAME = "center_final"
ALLELE_A_FASTA_FILENAME = "alleleA.fasta"
ALLELE_B_FASTA_FILENAME = "alleleB.fasta"
PHASED_ALLELES_DESCRIPTION_FILENAME = "phased_desc.txt"

SYMM_EDIT_DIST_FST = os.path.abspath(FST_DIR + "/ed_symm_nls.fst")
ASYMM_EDIT_DIST_FST_LS = os.path.abspath(FST_DIR + "/ed_asymm_ls.fst")
ASYMM_EDIT_DIST_FST_NLS = os.path.abspath(FST_DIR + "/ed_asymm_nls.fst")
DUAL_SYMM_EDIT_DIST_FST = os.path.abspath(FST_DIR + "/ed_symm_dual.bin")
DUAL_ASYMM_EDIT_DIST_FST = os.path.abspath(FST_DIR + "/ed_asymm_dual.bin")
DUAL_CODEX = os.path.abspath(FST_DIR + "/codex")
CNV_SYMBOLS_FILE = os.path.abspath(FST_DIR + "/cnv.symbols")
PDT_PARENS_FILE = os.path.abspath(FST_DIR + "/pdtparens.txt")

ALPHABET = [c for c in "0123456789a"]
WORKING_SUBDIR = "fsts"

class DualTreeFactory(object):
	
	@classmethod
	def create_option_parser(cls, usage):
		
		parser = OptionParser(usage)
		
		parser.add_option("-v", "--verbose", action = "store_true", dest = "verbose",
						  default = False, help = "be verbose?")
		
		format_help = """sequence or alignment format, see http://biopython.org/wiki/SeqIO 
						  or http://biopython.org/wiki/AlignIO for available formats"""
		
		parser.add_option("-i", "--input-format", action="store", dest="input_format", default="fasta",
						  metavar="FORMAT", help=format_help)
		parser.add_option("-s", "--short", action = "store_true", dest = "short",
						  default = False, help = "don't reconstruct ancestors, tree only")
		parser.add_option("-d", "--diploid_name", action = "store", type="string", dest = "diploid_name",
						  default = DIPLOID_NAME, help = "name of the diploid file (defaults to 'diploid')")
		parser.add_option("-t", "--total", action="store_true", dest = "no_resolve_total",
						  default = False, help = "Sum alleles to total CN before distance computation, implies -s")
		parser.add_option("","--ancestor-only", action="store_true", dest="ancestor_only",
						  default=False, help = "reconstruct ancestors only (needs full directory tree from previous -s run)")
		parser.add_option("", "--enable-logo", action="store_true", dest = "enable_logo",
						  default = False, help = "try to create sequence logos (requires weblogolib)")
		parser.add_option("", "--asymmetric", action="store_true", dest = "asymmetric",
						  default = False, help = "compute asymmetric distances, implies -s")
		
		return parser

	@classmethod
	def create_option_parser_phasing(cls, usage):
		
		parser = OptionParser(usage)
		
		parser.add_option("-v", "--verbose", action = "store_true", dest = "verbose",
						  default = False, help = "be verbose?")
		
		format_help = """sequence or alignment format, see http://biopython.org/wiki/SeqIO 
						  or http://biopython.org/wiki/AlignIO for available formats"""
		
		parser.add_option("-i", "--input-format", action="store", dest="input_format", default="fasta",
						  metavar="FORMAT", help=format_help)
		parser.add_option("-d", "--diploid_name", action = "store", type="string", dest = "diploid_name",
						  default = DIPLOID_NAME, help = "name of the diploid file (defaults to 'diploid')")
		parser.add_option("-a", "--alternative", action="store_true", dest= "candidate_based", default =False,
						  help = "use alternative candidate based allele resolver")
		
		return parser
	
	@classmethod
	def create_reconstruction_algorithm(cls, theapp, options):
		algo = ReconstructionAlgorithm(theapp)
		
		## in case of asymmetric distance or total CN, imply --short
		if options.no_resolve_total or options.asymmetric:
			options.short = True

		# choose correct sequence module
		if not options.ancestor_only:
			if options.no_resolve_total:
				algo.sequenceModule = ModuleSequencesAsSumOfMajorAndMinor(algo, options.input_format)
			else:
				algo.sequenceModule = ModuleSequencesRead(algo, options.input_format)
		
			## choose correct distance module
			if options.no_resolve_total:
				algo.distanceModule = ModuleDistancesTotalCN(algo)
			else: ## resolve and compute distances
				algo.distanceModule = ModuleDistancesPerAllele(algo, options.asymmetric)

		algo.treeModule = ModuleTreeNeighbourJoining(algo)
		
		## choose correct ancestor module
		if not options.short:
			algo.ancestorModule = ModuleAncestorsPerAllele(algo, False, options.enable_logo)
		
		return algo
		
	@classmethod
	def create_phasing_algorithm(cls, theapp):
		algo = PhasingAlgorithm(theapp)
		
		algo.sequenceModule = ModuleSequencesAsPushdownAcceptors(algo, theapp.options.input_format, theapp.options.diploid_name)
		algo.phasingModule = ModulePhaseAllelesPDT(algo, theapp.options.candidate_based)

		return algo     

	
class MEDICCApp(object):
	""" Main application class for tree inference """
	
	def __init__(self, cnvfiles, outputDir, options, logger=None):
		""" constructor """
		
		if logger:
			self.log = logger
		else:
			self.log = commons.tools.log.Log(is_logging=options.verbose)
		
		if not os.path.exists(outputDir):
			self.log.writeln("Creating %s" % outputDir)
			os.mkdir(outputDir)
				 
		self.config = cnvphylo.CnvBaseConfig(cnv_symbol_file = CNV_SYMBOLS_FILE, 
									alphabet = ALPHABET,
									pdt_parens_file=PDT_PARENS_FILE)
				
		#self.cnvtools = cnvphylo.cnvtools.CnvToolBox(config, logger=logger)
		
		#self.aligner_symm = CNVAligner(config, SYMM_EDIT_DIST_FST, logger=self.log)
		#self.aligner_symm_xt = CNVAligner(config, SYMM_EDIT_DIST_FST_XT, logger=self.log)
		#self.aligner_asymm_ls = CNVAligner(config, ASYMM_EDIT_DIST_FST_LS, logger=self.log)
		#self.aligner_asymm_nls = CNVAligner(config, ASYMM_EDIT_DIST_FST_NLS, logger=self.log)
		#self.aligner_dual_symm = CNVAligner(config, DUAL_SYMM_EDIT_DIST_FST, logger=self.log)
		#self.aligner_dual_asymm = CNVAligner(config, DUAL_ASYMM_EDIT_DIST_FST, logger=self.log)
		
		self.options = options
		self.cnvfiles = cnvfiles ## 3-tuple (name, major_filename, minor_filename)
		self.outputDir = outputDir
		
		self.algorithm = DualTreeFactory.create_reconstruction_algorithm(self, self.options)

	def run(self):
		""" main application routine """
		
		self.algorithm.run()
	
class MEDICCPhaseApp(object):
	""" Main application class for allele phasing """
	
	def __init__(self, cnvfiles, outputDir, options, logger=None):
		""" constructor """
		
		if logger:
			self.log = logger
		else:
			self.log = commons.tools.log.Log(is_logging=options.verbose)
		
		if not os.path.exists(outputDir):
			self.log.writeln("Creating %s" % outputDir)
			os.mkdir(outputDir)
				 
		self.config = cnvphylo.CnvBaseConfig(cnv_symbol_file = CNV_SYMBOLS_FILE, 
									alphabet = ALPHABET,
									pdt_parens_file=PDT_PARENS_FILE)
				
		self.options = options
		self.cnvfiles = cnvfiles ## 3-tuple (name, major_filename, minor_filename)
		self.outputDir = outputDir
		
		self.algorithm = DualTreeFactory.create_phasing_algorithm(self)

	def run(self):
		""" main application routine """

		self.algorithm.run()

class PhasingAlgorithm(object):
	""" This is the phasing algorithm. """

	def __init__(self, app, sequence_module=None, phasing_module=None):
		self.app = app
		self.sequenceModule = sequence_module
		self.phasingModule = phasing_module

	def run(self):
		""" main routine """
		descText = ""
		for chr, major, minor in self.app.cnvfiles:
			workingDir = self.app.outputDir + "/" + chr
			## compile for each chr
			filenames, labels, diploid = self.sequenceModule.run(major, minor, workingDir)
			## distances for each chr
			self.phasingModule.run(filenames, labels, diploid, workingDir)
			## copy per-chromosome phased fasta files to root dir
			alleleA_target = "%s_%s" % (chr, ALLELE_A_FASTA_FILENAME)
			alleleB_target = "%s_%s" % (chr, ALLELE_B_FASTA_FILENAME)
			shutil.copy("%s/%s" % (workingDir, ALLELE_A_FASTA_FILENAME), "%s/%s" % (self.app.outputDir, alleleA_target))
			shutil.copy("%s/%s" % (workingDir, ALLELE_B_FASTA_FILENAME), "%s/%s" % (self.app.outputDir, alleleB_target))
			## create desc.txt
			descText += "%s %s %s\n" % (chr, alleleA_target, alleleB_target)

		with open("%s/%s" % (self.app.outputDir, PHASED_ALLELES_DESCRIPTION_FILENAME), "wb") as fd:
			fd.write(descText)



class ReconstructionAlgorithm(object):
	""" This is the reconstruction algorithm. It consists of several modules that can be plugged in:
	* sequence compilation module (choices are "as dual acceptors", "as sum of major and minor")
	* distance computation (and allele resolving) module 
			(choices are "individual alleles", "direct from dual", "direct from major/minor")
	* ancestor inference module (choices are "from individual alleles").
	* A module always waorks on one major/minor pair. """
		
	def __init__(self, app, sequence_module = None, distance_module = None, tree_module = None, ancestor_module = None):
		self.app = app
		self.sequenceModule = sequence_module
		self.distanceModule = distance_module
		self.treeModule = tree_module
		self.ancestorModule = ancestor_module
		self.cnvtoolbox = cnvtools.CnvToolBox(self.app.outputDir, self.app.config, self.app.log)

	def run(self):
		ReconStruct = collections.namedtuple('ReconStruct', 'chr distmat labels seqA seqB')
		results = []
		for chr, major, minor in self.app.cnvfiles:
			workingDir = self.app.outputDir + "/" + chr
			## compile for each chr
			filenames, labels = self.sequenceModule.run(major, minor, workingDir)
			## distances for each chr
			distmat, seqA, seqB = self.distanceModule.run(filenames, labels, workingDir)
			results.append(ReconStruct(chr, distmat, labels, seqA, seqB))

		## sum distances into one distance matrix
		distmat, labels = self._sum_pairwise_distances([(rs.distmat, rs.labels) for rs in results])
		with open("%s/%s" % (self.app.outputDir, DISTANCE_FILENAME_PAIRWISE_SUMMED), "w") as fd_dist:
			fd_dist.writelines( cnvphylo.biotools.to_phylip_format(distmat, labels, sort=True) )

		## build first tree
		tree = self.treeModule.run(distmat, labels, self.app.outputDir)

		## optional global additional stuff
		pass
		
		## optional additional stuff for each chr
		pass

		## build second tree
		pass

		## ancestors for each chr
		if self.ancestorModule:
			chromosome_haplotype_trees = []
			for rs in results:
				workingDir = self.app.outputDir + "/" + rs.chr
				chromosome_haplotype_trees.append(self.ancestorModule.run(tree, (rs.seqA, rs.labels), (rs.seqB, rs.labels), self.app.options.diploid_name, workingDir))

			## now combine the trees of the two alleles
			final_tree_A = TreeTools.combine_trees(*[treeA for treeA,_ in chromosome_haplotype_trees]) 
			final_tree_B = TreeTools.combine_trees(*[treeB for _,treeB in chromosome_haplotype_trees]) 
			final_tree = TreeTools.combine_trees(final_tree_A, final_tree_B)

			if final_tree:
				self.cnvtoolbox.write_tree_files(final_tree, TREE_FILENAME_FINAL, self.app.outputDir)
				## also write the additive distance matrix from this tree        
				combined_dist_mat, combined_dist_labels = TreeTools.to_distance_matrix(final_tree, sort=True)
				with open("%s/%s" % (self.app.outputDir, DISTANCE_FILENAME_FINAL), "w") as fdout:
					fdout.writelines(cnvphylo.biotools.to_phylip_format(combined_dist_mat, combined_dist_labels, sort=False))

			if final_tree_A:
				self.cnvtoolbox.write_tree_files(final_tree_A, TREE_FILENAME_FINAL + "_A", self.app.outputDir)
				## also write the additive distance matrix from this tree        
				combined_dist_mat, combined_dist_labels = TreeTools.to_distance_matrix(final_tree_A, sort=True)
				with open("%s/%s" % (self.app.outputDir, DISTANCE_FILENAME_FINAL + "_A"), "w") as fdout:
					fdout.writelines(cnvphylo.biotools.to_phylip_format(combined_dist_mat, combined_dist_labels, sort=False))

			if final_tree_B:
				self.cnvtoolbox.write_tree_files(final_tree_B, TREE_FILENAME_FINAL + "_B", self.app.outputDir)
				## also write the additive distance matrix from this tree        
				combined_dist_mat, combined_dist_labels = TreeTools.to_distance_matrix(final_tree_B, sort=True)
				with open("%s/%s" % (self.app.outputDir, DISTANCE_FILENAME_FINAL + "_B"), "w") as fdout:
					fdout.writelines(cnvphylo.biotools.to_phylip_format(combined_dist_mat, combined_dist_labels, sort=False))


	def _sum_pairwise_distances(self, distances):
		""" sums the individual distance matrices into a big one """
		
		if len(distances) == 0:
			self.log.writeln("Warning: No distances computed.")
			return
		
		distmats, labels = zip(*distances)

		labels_agree = [labels[i] == labels[i+1] for i in range(len(labels)-1)] ## if only one chromosome, all([]) is still True

		if all(labels_agree):
			total_distance = numpy.sum(distmats, 0)
		else:
			raise DualTreeError("ERROR: Matrix labels do not agree!")
		
		return (total_distance, labels[0])


class ModuleSequencesAsPushdownAcceptors(object):
	PAREN1_OPEN = "("
	PAREN2_OPEN = "["
	PAREN3_OPEN = "{"
	PAREN1_CLOSE= ")"
	PAREN2_CLOSE = "]"
	PAREN3_CLOSE = "}"
	SEPARATOR = "X"

	""" Sequence module that compiles sequences as PDTs """
	def __init__(self, parent, input_format, diploid):
		self.parent = parent

		## options
		self.input_format = input_format
		self.diploid = diploid
		
		## tools
		self.fstlib = fst.openfst.OpenFST(self.parent.app.config.alphabet, self.parent.app.config.symbol_file, self.parent.app.config.pdt_parens_file)
		self.log = self.parent.app.log


	def run(self, majorFilename, minorFilename, workingDir):    
		cnvtoolbox = cnvtools.CnvToolBox(workingDir, self.parent.app.config, self.log)

		## step 1: turn major and minor CN files into combined FSAs
		fst_name_tuples = zip(*self.__create_pdts_from_cn_files(majorFilename, minorFilename))

		if not fst_name_tuples:
			return
		
		filenames = list(fst_name_tuples[0])
		textFSTs = list(fst_name_tuples[1])

		if not os.path.exists(workingDir):
			os.mkdir(workingDir)

		for filename in filenames:
			output_path = "%s/%s.fst" % (workingDir, filename)
			if os.path.exists(output_path):
				self.log.writeln("Warning! File %s.fst already exists! Replacing..." % filename)
			with open(output_path, "w") as fd_out:
				fd_out.write(str(textFSTs[filenames.index(filename)]))

		## step 1b: identify the diploid
		try:
			diploid = cnvtoolbox.identify_diploid(self.diploid)
		except CnvToolsError as ex:
			diploid = None
			self.log.writeln("Warning: " + ex.message)        
		
		## step 2: compile the FSAs
		binaryFSTs = [self.fstlib.bcompile(f, acceptor=False) for f in textFSTs]

		for i in range(0, len(filenames)):
			with open("%s/%s.bin" % (workingDir, filenames[i]), "wb") as fd_bin:
				fd_bin.write(binaryFSTs[i])

		return (filenames, filenames, diploid)
				
	def __create_pdts_from_cn_files(self, major_cn_file, minor_cn_file):
		""" turns the major and minor cn files into a pushdown automaton in which the two sequences are separated by the
		separator char (typically "X").
		Returns a list of (filename, fstobject) tuples """
		
		textFSTs = list()
		
		with open(major_cn_file, "r") as fd_major:
			seqs_major = [record for record in Bio.SeqIO.parse(fd_major, self.input_format)]
			idx_major = [s.id for s in seqs_major]
			
		with open(minor_cn_file, "r") as fd_minor:
			seqs_minor = [record for record in Bio.SeqIO.parse(fd_minor, self.input_format)]
			idx_minor = [s.id for s in seqs_minor]

		if len(list(set(idx_major))) != len(idx_major):
			raise DualTreeError("FATAL: sequence ids not unique (in major cn file). Exiting.")

		if len(list(set(idx_minor))) != len(idx_minor):
			raise DualTreeError("FATAL: sequence ids not unique (in minor cn file). Exiting.")
		
		if sum([seqid not in idx_minor for seqid in idx_major]): ## list not identical
			raise DualTreeError("FATAL: sequence identifiers differ in major and minor cn files. Exiting.")
		
		for cur_major in seqs_major:
			cur_minor = seqs_minor[idx_minor.index(cur_major.id)]
			
			## create FST and add states as many as sequence long + 1
			myfst = fstlib.fst.fst.Fst()
			startState = myfst.add_state(is_final=False)

			seqlen = len(cur_major.seq) 

			## first pass
			currentState = startState
			for i in range(0, seqlen):
				ilabel = cur_major.seq[i]
				olabel = cur_minor.seq[i]
				
				if ilabel == olabel: ## simply add state and single transition
					newState = myfst.add_state(is_final=False)
					myfst.add_transition((currentState,  newState, ilabel, ilabel, 0))
					currentState=newState
				else: ## add seperate symbol transitions and stack transitions
					iState = myfst.add_state(is_final=False)
					oState = myfst.add_state(is_final=False)
					joinState = myfst.add_state(is_final=False)
					myfst.add_transition((currentState,  iState, ilabel, ilabel, 0))
					myfst.add_transition((currentState,  oState, olabel, olabel, 0))
					myfst.add_transition((iState,  joinState, self.PAREN1_OPEN, self.PAREN1_OPEN, 0))
					myfst.add_transition((oState,  joinState, self.PAREN2_OPEN, self.PAREN2_OPEN, 0))
					currentState = joinState

			## add separator
			sepState = myfst.add_state(is_final=False)
			myfst.add_transition((currentState,  sepState, self.SEPARATOR, self.SEPARATOR, 0))
			currentState = sepState

			## second pass
			for i in range(seqlen-1, -1, -1):
				ilabel = cur_minor.seq[i]
				olabel = cur_major.seq[i]
				
				if ilabel == olabel: ## simply add state and single transition
					newState = myfst.add_state(is_final=False)
					myfst.add_transition((currentState,  newState, ilabel, ilabel, 0))
					currentState=newState
				else: ## add seperate symbol transitions and stack transitions
					iState = myfst.add_state(is_final=False)
					oState = myfst.add_state(is_final=False)
					joinState = myfst.add_state(is_final=False)
					myfst.add_transition((currentState,  iState, ilabel, ilabel, 0))
					myfst.add_transition((currentState,  oState, olabel, olabel, 0))
					myfst.add_transition((iState,  joinState, self.PAREN1_CLOSE, self.PAREN1_CLOSE, 0))
					myfst.add_transition((oState,  joinState, self.PAREN2_CLOSE, self.PAREN2_CLOSE, 0))
					currentState = joinState


			currentState.is_final=True
			
			label = cur_major.id.replace("/", "_") ## filenames mustn't contain "/"            
			textFSTs.append((label, myfst))
				
		return textFSTs

class ModuleSequencesAsSumOfMajorAndMinor(object):
	""" Sequence module that compiles sequences as the sum of major and minor """
	
	def __init__(self, container, input_format):
		## options
		self.parent = container
		self.input_format = input_format
		
		## tools
		self.log = container.app.log

	def run(self, majorFilename, minorFilename, workingDir):
		## step 1: turn major and minor CN files into summed files

		self.cnvtoolbox = cnvtools.CnvToolBox(workingDir, self.parent.app.config, self.log)
				
		fst_name_tuples = zip(*self._sum_sequences(majorFilename, minorFilename))

		if not fst_name_tuples:
			return
		
		filenames = fst_name_tuples[0]
		textFSTs = fst_name_tuples[1]

		if not os.path.exists(workingDir):
			os.mkdir(workingDir)

		for filename in filenames:
			output_path = "%s/%s.fst" % (workingDir, filename)
			if os.path.exists(output_path):
				self.log.writeln("Warning! File %s.fst already exists! Replacing..." % filename)
			with open(output_path, "w") as fd_out:
				fd_out.write(str(textFSTs[filenames.index(filename)]))

		## step 2: compile the FSAs
		binaryFSTs = [self.cnvtoolbox.bcompile(f, acceptor=True) for f in textFSTs]

		for i in range(0, len(filenames)):
			with open("%s/%s.bin" % (workingDir, filenames[i]), "wb") as fd_bin:
				fd_bin.write(binaryFSTs[i])

		return (filenames, filenames)

	def _sum_sequences(self, major_cn_file, minor_cn_file):
		textFSTs = list()
		
		with open(major_cn_file, "r") as fd_major:
			seqs_major = [record for record in Bio.SeqIO.parse(fd_major, self.input_format)]
			idx_major = [s.id for s in seqs_major]
			
		with open(minor_cn_file, "r") as fd_minor:
			seqs_minor = [record for record in Bio.SeqIO.parse(fd_minor, self.input_format)]
			idx_minor = [s.id for s in seqs_minor]

		if len(list(set(idx_major))) != len(idx_major):
			raise DualTreeError("FATAL: sequence ids not unique (in major cn file). Exiting.")

		if len(list(set(idx_minor))) != len(idx_minor):
			raise DualTreeError("FATAL: sequence ids not unique (in minor cn file). Exiting.")
		
		if sum([seqid not in idx_minor for seqid in idx_major]): ## list not identical
			raise DualTreeError("FATAL: sequence identifiers differ in major and minor cn files. Exiting.")
		
		for cur_major in seqs_major:
			cur_minor = seqs_minor[idx_minor.index(cur_major.id)]
			
			text_sum = self.cnvtoolbox.sum_text_sequences(cur_major, cur_minor)
			
			fst_sum = FSTFactory.create_fixed_length_hmm_from_sequences([text_sum,], ALPHABET, pseudo_counts=0)
			
			label = cur_major.id.replace("/", "_") ## filenames mustn't contain "/"            
			textFSTs.append((label, fst_sum))
				
		return textFSTs
			
		
class ModuleSequencesRead(object):
	""" Sequence module that compiles sequences exactly as they are found in the description file """
	
	def __init__(self, container, input_format):
		## options
		self.parent = container
		self.input_format = input_format
		
		## tools
		self.log = container.app.log

	def run(self, majorFilename, minorFilename, workingDir):
		if not os.path.exists(workingDir):
			os.mkdir(workingDir)

		## step 1 - read sequences
		self.cnvtoolbox = cnvtools.CnvToolBox(workingDir, self.parent.app.config, self.log)
		textFSTs = self._read_sequences(majorFilename, minorFilename)
		fst_name_tuples = zip(*textFSTs)
		
		if not fst_name_tuples:
			return
		
		filenames = fst_name_tuples[0]
		textFSTs_A = fst_name_tuples[1]
		textFSTs_B = fst_name_tuples[2]

		for filename in filenames:
			## write allele A
			output_path = "%s/%s_A.fst" % (workingDir, filename)
			if os.path.exists(output_path):
				self.log.writeln("Warning! File %s.fst already exists! Replacing..." % filename)
			with open(output_path, "w") as fd_out:
				fd_out.write(str(textFSTs_A[filenames.index(filename)]))

			## write allele B
			output_path = "%s/%s_B.fst" % (workingDir, filename)
			if os.path.exists(output_path):
				self.log.writeln("Warning! File %s.fst already exists! Replacing..." % filename)
			with open(output_path, "w") as fd_out:
				fd_out.write(str(textFSTs_B[filenames.index(filename)]))

		## step 2: compile the FSAs
		binaryFSTs_A = [self.cnvtoolbox.bcompile(f, acceptor=True) for f in textFSTs_A]
		binaryFSTs_B = [self.cnvtoolbox.bcompile(f, acceptor=True) for f in textFSTs_B]

		for i in range(0, len(filenames)):
			with open("%s/%s_A.bin" % (workingDir, filenames[i]), "wb") as fd_bin:
				fd_bin.write(binaryFSTs_A[i])
			with open("%s/%s_B.bin" % (workingDir, filenames[i]), "wb") as fd_bin:
				fd_bin.write(binaryFSTs_B[i])

		return (filenames, filenames)

	def _read_sequences(self, major_cn_file, minor_cn_file):
		textFSTs = list()

		with open(major_cn_file, "r") as fd_major:
			seqs_major = [record for record in Bio.SeqIO.parse(fd_major, self.input_format)]
			idx_major = [s.id for s in seqs_major]
			
		with open(minor_cn_file, "r") as fd_minor:
			seqs_minor = [record for record in Bio.SeqIO.parse(fd_minor, self.input_format)]
			idx_minor = [s.id for s in seqs_minor]

		if len(list(set(idx_major))) != len(idx_major):
			raise DualTreeError("FATAL: sequence ids not unique (in major cn file). Exiting.")

		if len(list(set(idx_minor))) != len(idx_minor):
			raise DualTreeError("FATAL: sequence ids not unique (in minor cn file). Exiting.")
		
		if sum([seqid not in idx_minor for seqid in idx_major]): ## list not identical
			raise DualTreeError("FATAL: sequence identifiers differ in major and minor cn files. Exiting.")
		
		for cur_major in seqs_major:
			cur_minor = seqs_minor[idx_minor.index(cur_major.id)]
			
			fst_major = FSTFactory.create_fixed_length_hmm_from_sequences([cur_major,], ALPHABET, pseudo_counts=0)
			fst_minor = FSTFactory.create_fixed_length_hmm_from_sequences([cur_minor,], ALPHABET, pseudo_counts=0)
			
			label = cur_major.id.replace("/", "_") ## filenames mustn't contain "/"            
			textFSTs.append((label, fst_major, fst_minor))
				
		return textFSTs

class ModuleDistancesPerAllele(object):
	""" Computes distances as the sum of the distances of the two alleles """

	def __init__(self, parent, asymmetric):
		self.parent = parent
		self.log = self.parent.app.log
		self.asymmetric = asymmetric
		self.fstlib = fstlib.fst.openfst.OpenFST(self.parent.app.config.alphabet, self.parent.app.config.symbol_file, self.parent.app.config.pdt_parens_file)

	def run(self, files, labels, working_dir):

		if self.asymmetric:
			aligner = cnvtools.CNVAligner(working_dir, self.parent.app.config, ASYMM_EDIT_DIST_FST_NLS, logger=self.parent.app.log, asymmetric=True)
		else:
			aligner = cnvtools.CNVAligner(working_dir, self.parent.app.config, SYMM_EDIT_DIST_FST, logger=self.parent.app.log, asymmetric=False)

		nseqs = len(files)
	
		sequencesA = ["%s_A" % f for f in files]
		sequencesB = ["%s_B" % f for f in files]

		## run the algorithm
		distancesA = aligner.align_pairwise(sequencesA)
		distancesB = aligner.align_pairwise(sequencesB)    
		distances = distancesA + distancesB
		 
		with open(working_dir + "/" + DISTANCE_FILENAME_A, "w") as fd_distance:
			fd_distance.writelines(cnvphylo.biotools.to_phylip_format(distancesA, labels, sort=True))
		with open(working_dir + "/" + DISTANCE_FILENAME_B, "w") as fd_distance:
			fd_distance.writelines(cnvphylo.biotools.to_phylip_format(distancesB, labels, sort=True))
		with open(working_dir + "/" + DISTANCE_FILENAME_PAIRWISE, "w") as fd_distance:
			fd_distance.writelines(cnvphylo.biotools.to_phylip_format(distances, labels, sort=True))

		return (distances, sequencesA, sequencesB)

class ModulePhaseAllelesPDT(object):
	""" Computes distances from dual acceptors by resolving the alleles first and
	adding the distance matrices. Uses the iterative version of the solver for the
	Fermat-Weber problem based on PDTs to resolve the alleles. """

	def __init__(self, parent, candidate_based):
		self.candidate_based=candidate_based
		self.parent = parent
		self.log = self.parent.app.log

		self.fstlib = fst.openfst.OpenFST(self.parent.app.config.alphabet, self.parent.app.config.symbol_file, self.parent.app.config.pdt_parens_file)
		self.aligner = None

	def _to_hmm(self, inputfst):
		""" turns an acceptor in to a hmm and returns both the binary hmm and the 
		posterior decoding object """
				
		textual_fst = self.fstlib.bprint(inputfst)
			
		## now convert to position specific HMM
		self.log.write("Parsing input FST...")
		reader = fstlib.fst.io.OpenFstTextReader(StringIO.StringIO(textual_fst))
		fstobj = reader.create_fst()
		self.log.writeln("done!")
	
		self.log.write("Running Posterior Decoding...")
		algo = fstlib.fst.algos.PosteriorDecodingCountDP(fstobj)
		self.log.writeln("done!")
	
		myfst = fstlib.fst.factory.FSTFactory.create_fixed_length_hmm_from_counts(algo.posterior)
		return myfst

	def _find_center_from_triples(self, distmat, input_sequences):
		original_sequences = sequences = input_sequences[:]
		D = distmat.copy()

		while True:
			## now find the (at max) 10 most divergent sequences
			order = numpy.argsort(D.sum(0))[::-1]
			top3 = order[0:min(3, len(order))]
		
			## now take those candidates and intersect them
			selected_sequences = [sequences[i] for i in top3]
		
			big_intersect = self.fstlib.bmass_intersect_quick_pdt(selected_sequences, detmin=False)
			point = self.fstlib.bmap(self.fstlib.bdetmin(self.fstlib.bshortestpath_pdt(self.fstlib.bshortestpath_pdt(big_intersect)), True), map_type="rmweight")
			
			dist_to_others, point_edit = self._compute_distance_to_others(point, sequences)
			##self.log.writeln("Dist to others: %d" % dist_to_others.sum())
			
			## modify distance matrix
			D = numpy.append(numpy.append(D, dist_to_others.reshape(1,len(dist_to_others)), 0), numpy.append(dist_to_others, 0).reshape(len(dist_to_others)+1,1), 1)
			D = numpy.delete(D, top3, 0)
			D = numpy.delete(D, top3, 1)
		
			## modify sequence list
			sequences = [sequences[i] for i,_ in enumerate(sequences) if i not in top3]
			sequences.append(point_edit)
		
			if len(sequences) <3: break

		dist,_ = self._compute_distance_to_others(point, original_sequences)
		
		return (point, dist.sum())

	def run(self, files, labels, diploid, working_dir):
		""" set candidate_based to True for the previous allele resolver based on shortest path candidates between all pairs """

		self.aligner = cnvtools.CNVAligner(working_dir, self.parent.app.config, ASYMM_EDIT_DIST_FST_NLS, logger=self.parent.app.log)
		aligner_symm = cnvtools.CNVAligner(working_dir, self.parent.app.config, SYMM_EDIT_DIST_FST, logger=self.parent.app.log)
		#cnvtoolbox = cnvtools.CnvToolBox(working_dir, self.parent.app.config, self.parent.app.log)

		nseqs = len(files)

		## first project sequences (without literally projecting them)
		composed_sequences = [self.aligner.projectPDT(s, minimize=True, from_right=True, project=False) for s in files]
		
		## now for each of the sequences compute candidate assignments by aligning them to all other sequences and keeping the shortest path
		
		## compute pairwise distances and generate candidates
		candidates = [ [] for i in range(nseqs) ]
		
		distmat = numpy.zeros((nseqs, nseqs))
		for i in range(nseqs-1):
			for j in range(i+1, nseqs):
				shortestPath = self.fstlib.bdetmin( self.fstlib.bshortestpath_pdt( self.fstlib.bmass_intersect_quick_pdt([self.fstlib.binvert(composed_sequences[i]), composed_sequences[j]], detmin=False) ), rmepsilon=True)
				if self.candidate_based:
					candidates[i].append( self.fstlib.bmap( self.fstlib.bproject(shortestPath), "identity" ))
					candidates[j].append( self.fstlib.bmap( self.fstlib.bproject(shortestPath, to_output=True) , "identity"))
				val = self.fstlib.bshortestdistance( shortestPath, reverse=True )[0][1]
				self.log.writeln("Distance: %.2f" % val)
				distmat[i,j] = distmat[j,i] = val

		with open(working_dir + "/" + DISTANCE_FILENAME_PAIRWISE_DIRECT, "wb") as fd_distance:
			fd_distance.writelines(cnvphylo.biotools.to_phylip_format(distmat, labels, sort=True))

		if not self.candidate_based:
			projected_sequences = [self.fstlib.bproject(f) for f in composed_sequences]

			center, center_dist = self._find_center_from_triples(distmat, projected_sequences)
			self.log.writeln("Center found with distance %d" % center_dist)

			with open("%s/%s.bin" % (working_dir, CENTER_FILENAME), "wb") as fd_center:
				fd_center.write(center)

			realigned = [self.fstlib.bcompose_pdt(center, pda, left_pdt=False) for pda in composed_sequences]

			sps = [self.fstlib.bshortestpath_pdt(pda) for pda in realigned]
		else:
			## now take the candidates and build the unions
			unions = [self.fstlib.bmulticommand(COMMAND_UNION, fsts, return_filename = True) for fsts in candidates]

			## create position specific hmms from that
			hmms = [self.fstlib.bcompile(self._to_hmm(f), acceptor=True) for f in unions]

			## compose unions with hmms
			compositions = [self.fstlib.bmass_intersect_quick([unions[i], hmms[i]]) for i in range(len(unions))]

			## find shortest path
			sps = [self.fstlib.bshortestpath(f) for f in compositions] 

		### select the final sequences
		final_sequences = [self.fstlib.bdetmin(self.fstlib.bproject(self.fstlib.bmap(f, "rmweight"), to_output=True), rmepsilon=True) for f in sps]


		sequencesA = []
		sequencesB = []

		fastaA = []
		fastaB = []

		## now extract the two alleles from the sequence
		text_sequences = [self.fstlib.get_sequences(seq)[0][0] for seq in final_sequences]

		for index, seq in enumerate(text_sequences):
			splitstring = seq.split(ModuleSequencesAsPushdownAcceptors.SEPARATOR)
			assert(len(splitstring)==2)
			alleleA = splitstring[0]
			alleleB = splitstring[1][::-1] ## invert because of the way context-free grammars are evaluated (inside-out)
			fstA = fstlib.fst.factory.FSTFactory.create_fixed_length_hmm_from_sequences([alleleA], ALPHABET, 0)
			fstB = fstlib.fst.factory.FSTFactory.create_fixed_length_hmm_from_sequences([alleleB], ALPHABET, 0)
			filenameA = files[index] + "_A"
			filenameB = files[index] + "_B"
			with open("%s/%s.bin" % (working_dir, filenameA), "wb") as fdA:
				fdA.write(self.fstlib.bcompile(fstA, acceptor=True))
			with open("%s/%s.bin" % (working_dir, filenameB), "wb") as fdB:
				fdB.write(self.fstlib.bcompile(fstB, acceptor=True))
			sequencesA.append(filenameA)
			sequencesB.append(filenameB)
			fastaA.append(">%s" % files[index])
			fastaA.append(alleleA)
			fastaB.append(">%s" % files[index])
			fastaB.append(alleleB)


		## write fasta files
		with open("%s/%s" %(working_dir, ALLELE_A_FASTA_FILENAME), "wb") as fdA:
			fdA.write("\n".join(fastaA)+"\n")
		with open("%s/%s" %(working_dir, ALLELE_B_FASTA_FILENAME), "wb") as fdB:
			fdB.write("\n".join(fastaB)+"\n")

		return (ALLELE_A_FASTA_FILENAME, ALLELE_B_FASTA_FILENAME)


	def _compute_distance_to_others(self, candidate, projected):
		##candidate_edit = self.aligner.projectPDT(candidate, minimize=True, from_right=True)
		candidate_edit = self.fstlib.barcsort(self.fstlib.bdetmin(self.fstlib.bproject(self.fstlib.bcompose(ASYMM_EDIT_DIST_FST_NLS, candidate))), "ilabel")
		distances = numpy.zeros(len(projected))
		for i,p in enumerate(projected):
			inter = self.fstlib.bcompose_pdt(p, candidate_edit)
			sp = self.fstlib.bdetmin(self.fstlib.bshortestpath_pdt(inter),True)
			distances[i] = self.fstlib.bshortestdistance(sp, True)[0][1]
			 
		return (distances, candidate_edit)

class ModuleDistancesTotalCN(object):
	""" Computes distances over the total CN,i.e. major + minor """
	
	def __init__(self, algo):
		self.parent = algo
		self.log = self.parent.app.log
		self.fstlib = fstlib.fst.openfst.OpenFST(self.parent.app.config.alphabet, self.parent.app.config.symbol_file, self.parent.app.config.pdt_parens_file)
	
	def run(self, files, labels,  workingDir):
		
		aligner = CNVAligner(workingDir, self.parent.app.config, SYMM_EDIT_DIST_FST_XT, self.parent.app.log)

		distances = aligner.align_pairwise(files)

		return (distances, None, None)

class ModuleTreeNeighbourJoining(object):
	def __init__(self, parent):
		self.parent = parent
		self.log = self.parent.app.log

	def run(self, distmat, labels, outDir):
		""" build tree """

		cnvtoolbox = cnvtools.CnvToolBox(outDir, self.parent.app.config, self.log)
		
		## initialize tree
		neighbour = cnvphylo.trees.NeighbourJoining(distmat, labels, logger=self.log)
		cnvtoolbox.write_tree_files(neighbour.tree, NJ_TREE_BASENAME, output_dir = outDir)
		
		return neighbour.tree
		
class ModuleTreeFitchMargoliash(object):
	def __init__(self, parent, clock):
		self.parent = parent
		self.log = self.parent.app.log
		self.clock = clock

	def run(self, distmat, labels, outDir):
		""" build tree """

		cnvtoolbox = cnvtools.CnvToolBox(outDir, self.parent.app.config, self.log)
		
		## initialize tree
		fitch = cnvphylo.trees.FitchMargoliash(distmat, labels, self.clock, logger=self.log)
		cnvtoolbox.write_tree_files(fitch.tree, FITCH_TREE_BASENAME + ("_c" if self.clock else "_nc"), output_dir = outDir)
		
		return fitch.tree
		 
class ModuleAncestorsPerAllele(object):
	def __init__(self, parent, use_alternative_reconstruction, write_sequence_logo):
		self.parent = parent
		self.log = self.parent.app.log

		## options
		self.alternative_reconstruction = use_alternative_reconstruction
		self.write_sequence_logo = write_sequence_logo

		self.NodeInfo = collections.namedtuple("NodeInfo", ["approx_full", "approx_split", "exact", "estimate"])
		
		self.fstlib = fstlib.fst.openfst.OpenFST(self.parent.app.config.alphabet, self.parent.app.config.symbol_file, self.parent.app.config.pdt_parens_file)
		self.aligner_symm = None
		self.aligner_asymm = None
		self.cnvtools = None
		self.diploid = ""
		self.working_dir = ""

	def run(self, tree, sequencesA, sequencesB, diploid, workingDir):
		""" second half of the calculation, reconstruct the ancestors for both alleles """
		self.aligner_asymm = cnvtools.CNVAligner(workingDir, self.parent.app.config, ASYMM_EDIT_DIST_FST_LS, self.log)
		self.aligner_symm = cnvtools.CNVAligner(workingDir, self.parent.app.config, SYMM_EDIT_DIST_FST, self.log)
		self.cnvtools = cnvtools.CnvToolBox(workingDir, self.parent.app.config, self.log)
		self.diploid = diploid
		self.working_dir = workingDir

		if not tree:
			raise DualTreeError("No tree reconstructed! Can't infer ancestors!")
		
		self.log.writeln("######### BEGIN Ancestor reconstruction ############")
		if self.alternative_reconstruction:
			self.log.writeln("Using alternative ancestor reconstruction routine")
			treeA, int_seqsA, int_lblsA = self._reconstruct_ancestors_full_likelihood(tree, sequencesA, suffix="_A")
			treeB, int_seqsB, int_lblsB = self._reconstruct_ancestors_full_likelihood(tree, sequencesB, suffix="_B")
		else:
			treeA, int_seqsA, int_lblsA = self._reconstruct_ancestors(tree, sequencesA, suffix="_A")
			treeB, int_seqsB, int_lblsB = self._reconstruct_ancestors(tree, sequencesB, suffix="_B")
		self.cnvtools.write_tree_files(treeA, TREE_FILENAME_A, output_dir = workingDir)
		self.cnvtools.write_tree_files(treeB, TREE_FILENAME_B, output_dir = workingDir)
		
		self.log.writeln("######### END Ancestor reconstruction ############")

		return (treeA, treeB)

	def _reconstruct_ancestors(self, tree, sequences, suffix=""):
		""" main reconstruction routine """
		
		def resolve_subtrees(label1, label2):
			""" resolves the subtrees and finalizes the two sequence sets """
		
			## finalize it
			ambiguity_left, ambiguity_right, txtseq_left, txtseq_right = _finalize_pair_profile(label1, label2)

			rec_left = Bio.SeqRecord.SeqRecord(Bio.Seq.Seq(data=".", alphabet=None),
				   id=label1+suffix, name=suffix.strip("_"),
				   description=txtseq_left)

			rec_right = Bio.SeqRecord.SeqRecord(Bio.Seq.Seq(".", None),
				   id=label2+suffix, name=suffix.strip("_"),
				   description=txtseq_right)
			
			## calculate backward distances
			clade_left = label_clade_map[label1]
			clade_right = label_clade_map[label2]
					
			clade_left.confidences.append(Bio.Phylo.PhyloXML.Confidence(ambiguity_left, type="ambiguity"))
			clade_right.confidences.append(Bio.Phylo.PhyloXML.Confidence(ambiguity_right, type="ambiguity"))
					
			clade_left.sequences.append(Bio.Phylo.PhyloXML.Sequence.from_seqrecord(rec_left))
			clade_right.sequences.append(Bio.Phylo.PhyloXML.Sequence.from_seqrecord(rec_right))
									
			if len(clade_left.clades):
				backward_files_left = [lsm[clade.name] for clade in clade_left.clades]
				backward_distances_left = [self.aligner_symm.align_pair(lsm[label1], target) for target in backward_files_left]
				for idx in range(0, len(backward_distances_left)):
					clade_left.clades[idx].branch_length = backward_distances_left[idx]
					self.log.writeln("Adding branch length %.4f for branch %s" % (backward_distances_left[idx], clade_left.clades[idx].name))
			if len(clade_right.clades):
				backward_files_right = [lsm[clade.name] for clade in clade_right.clades]
				backward_distances_right = [self.aligner_symm.align_pair(lsm[label2], target) for target in backward_files_right]
				for idx in range(0, len(backward_distances_right)):
					clade_right.clades[idx].branch_length = backward_distances_right[idx]
					self.log.writeln("Adding branch length %.4f for branch %s" % (backward_distances_right[idx], clade_right.clades[idx].name))
		## end join_and_resolve_subtrees
		
		## join pair
		def join_pair(label1, label2, new_filename):
			""" joins two sequences by composition with edit distance and intersect """
			
			final_left = lsm[label1]
			final_right = lsm[label2]
			
			left_edit = self.aligner_asymm.project(final_left, minimize=True, lazy=True, from_right=True)
			right_edit = self.aligner_asymm.project(final_right, minimize=True, lazy=True, from_right=True)
			
			self.cnvtools.mass_intersect_quick([left_edit, right_edit], new_filename + suffix)
		
			lsm[new_filename] = new_filename + suffix
		## end join pair

		def _finalize_pair_profile(label_left, label_right):
			""" aligns two sequences and projects the result to input and output to 
			finalize the sequences """
	
			def _to_hmm(inputfst):
				""" turns an acceptor in to a hmm and returns both the binary hmm and the 
				posterior decoding object """
				
				textual_fst = self.cnvtools.bprint(inputfst)
			
				## now convert to position specific HMM
				self.log.write("Parsing input FST...")
				reader = fstlib.fst.io.OpenFstTextReader(StringIO.StringIO(textual_fst))
				fstobj = reader.create_fst()
				self.log.writeln("done!")
	
				self.log.write("Running Posterior Decoding...")
				algo = fstlib.fst.algos.PosteriorDecodingCountDP(fstobj)
				self.log.writeln("done!")
	
				myfst = fstlib.fst.factory.FSTFactory.create_fixed_length_hmm_from_counts(algo.posterior)
				return (myfst, algo)
				
			def _to_count_matrix(algo):
				""" turns the counts returned from a posterior decoding run into a 
				count matrix for generating a sequence logo """    
			
				## setup count matrix
				count_matrix = numpy.zeros((len(algo.posterior), len(ALPHABET)))
				for pos in range(0, len(algo.posterior)):
					for char in ALPHABET:
						try:
							count = algo.posterior[pos][char]
							#print "Position %d: character %s, %d times" % (pos, char, count)
							count_matrix[pos, ALPHABET.index(char)] = count
						except KeyError:
							pass
						
			
				ambiguity = numpy.sum(numpy.sum(count_matrix!=0, axis=1) != 1)
				
				return count_matrix, ambiguity
			
			ext = suffix + "_final"
			ambiguities = list()
			textseqs = list()
	
			file_left = lsm[label_left]
			file_right = lsm[label_right]
											
			alignment = self.cnvtools.bprune( 
						self.aligner_symm.balign_pair_full(file_left, file_right), 
						weight=0)
	
			## left sequence first
			def _seq_iterator():
				yield (self.cnvtools.bproject(alignment), label_left) 
				yield (self.cnvtools.bproject(alignment, to_output=True), label_right)

			for projected, label in _seq_iterator(): ## for both left and right sequence 
				mapped = self.cnvtools.bmap(projected, map_type="rmweight")
				detmin = self.cnvtools.bdetmin(mapped)
				
				hmm, algo = _to_hmm(detmin)
				binhmm = self.cnvtools.bcompile(hmm, acceptor=True)
				
				filename = label + ext
				
				## save the hmm
				with open("%s/%s.bin" % (self.working_dir, filename), "wb") as fd: 
					fd.write(binhmm)

				## now weight the ambiguous sequences in detmin by their position specific probabilities
				with tempfile.NamedTemporaryFile(delete=False) as fd_temp:
					fd_temp.write(detmin)

				detmin_weighted = self.cnvtools.bmass_intersect_quick([filename, fd_temp.name])
				os.unlink(fd_temp.name)
				
				## and select our final sequence based on these probabilities
				estimate = self.cnvtools.bmap(
							self.cnvtools.bshortestpath(detmin_weighted)
							, map_type="rmweight")
				
				## overwrite the file (previous the hmm) with this new estimate 
				with open("%s/%s.bin" % (self.working_dir, filename), "wb") as fd: 
					fd.write(estimate)
					
				txtestimate = self.cnvtools.bprint(estimate)
				dfs = fstlib.fst.algos.PathDepthFirstSearchNew(
						fstlib.fst.io.OpenFstTextReader(StringIO.StringIO(txtestimate)).create_fst())
				
				txtseq = "".join([p.symbol_from for p in dfs.get_paths().next()])
				textseqs.append(txtseq)
				
				lsm[label] = filename
				
				countmat, ambiguity = _to_count_matrix(algo)
				ambiguities.append(ambiguity)
				
				if self.write_sequence_logo:
					with open(self.working_dir + "/" + label + ext + ".pdf", "wb") as fd_a:
						self.cnvtools.write_sequence_logo(countmat, ALPHABET, fd_a, name=label+suffix)
	
			return (ambiguities[0], ambiguities[1], textseqs[0], textseqs[1]) 

		""""""
		## main reconstruction routine starts here
		input_tree = Bio.Phylo.BaseTree.copy.deepcopy(tree)
		tmpsearch = [c for c in input_tree.find_clades(name=self.diploid)]
		if len(tmpsearch) == 0: ## no diploid, make our own
			self.log.writeln("ERROR: no diploid root found!")
			sys.exit(1)
		else:
			diploid = tmpsearch[0]
			root_path = input_tree.get_path(diploid)[::-1]
			if len(root_path)>1:
				new_root = root_path[1]
				self.log.writeln("Rooting with " + new_root.name)
				input_tree.root_with_outgroup(new_root)
			else:
				## if the root path is of length one, the node immediately
				## before the diploid is already the root => do nothing
				pass
			
			input_tree.root.clades.remove(diploid)            
		
		## setup label sequence map
		lsm = dict(zip(*sequences[::-1])) ## label-sequence-map
				
		## make sure diploid is first
		remaining_labels = list(sequences[1]) ## mutable copy 
		remaining_labels.remove(self.diploid)
		remaining_labels = [self.diploid, ] + remaining_labels
		nseqs = len(remaining_labels)
		
		internal_nodes = input_tree.get_nonterminals(order="level")[::-1]
		self.log.writeln("Internal node order: " + " -> ".join([n.name for n in internal_nodes]))
		
		tree = Bio.Phylo.PhyloXML.Phylogeny()
		label_clade_map = dict()
		for s in lsm:
			label_clade_map[s] = Bio.Phylo.PhyloXML.Clade(name=s)
		
		internal_count = 0

		
		while len(remaining_labels)>2:
			## find pair
			label_left = internal_nodes[internal_count].clades[0].name
			label_right = internal_nodes[internal_count].clades[1].name
	
			#(final_a, final_b) = resolve_subtrees(label_a, label_b)
			resolve_subtrees(label_left, label_right)
			
			joined_name = internal_nodes[internal_count].name
			join_pair(label_left, label_right, joined_name)
			internal_count += 1
			label_clade_map[joined_name] = Bio.Phylo.PhyloXML.Clade(name=joined_name, 
																   clades = [label_clade_map[label_left], 
																			 label_clade_map[label_right]])
		
			## remove pair from sequence list and distance matrix
			remaining_labels.remove(label_left)
			remaining_labels.remove(label_right)
			
			## add new sequence to sequence list and distance matrix
			if internal_count < (nseqs-2):
				remaining_labels.append(joined_name)
			
			## rinse and repeat
			self.log.writeln(remaining_labels)

		## final pair
		label_a = joined_name
		label_b = remaining_labels[0]
		assert label_b == self.diploid
		
		resolve_subtrees(label_a, label_b)
		
		last_distance = self.aligner_symm.align_pair(lsm[label_a], lsm[label_b])
		
		label_clade_map[label_a].branch_length = 0
		label_clade_map[label_b].branch_length = last_distance
		label_clade_map[label_a].clades.append(label_clade_map[label_b])
		tree.root = label_clade_map[label_a]
	
		internal_labels = [c.name for c in internal_nodes]
		internal_sequences = [lsm[l] for l in internal_labels]
		return (tree, internal_sequences, internal_labels)

	def _reconstruct_ancestors_full_likelihood(self, tree, sequences, suffix=""):
		""" main reconstruction routine 
		two steps, first walk through the tree to get likelihood (=total edit cost), 
		than backtrack to get ancestors """

		input_tree = Bio.Phylo.BaseTree.copy.deepcopy(tree)
		tmpsearch = [c for c in input_tree.find_clades(name=self.diploid)]
		if len(tmpsearch) == 0:
			self.log.writeln("ERROR: no diploid root found!")
			sys.exit(1)
		else:
			diploid = tmpsearch[0]
			root_path = input_tree.get_path(diploid)[::-1]
			if len(root_path)>1:
				new_root = root_path[1]
				self.log.writeln("Rooting with " + new_root.name)
				input_tree.root_with_outgroup(new_root)
			else:
				## if the root path is of length one, the node immediately
				## before the diploid is already the root => do nothing
				pass
			
			output_tree = Bio.Phylo.BaseTree.copy.deepcopy(input_tree)
			input_tree.root.clades.remove(diploid)            
		
		
					
		## setup label sequence map
		#label2seq = dict(zip(*sequences[::-1])) ## label-sequence-map
		label2seq = dict()
		for i in range(0, len(sequences[1])):
			label2seq[sequences[1][i]] = self.NodeInfo(approx_full=None, approx_split=None, 
													   exact=sequences[0][i], estimate=sequences[0][i])
				
		## make sure diploid is first
		internal_nodes = input_tree.get_nonterminals(order="level")[::-1]
		self.log.writeln("Internal node order: " + " -> ".join([n.name for n in internal_nodes]))

		## iterate through tree and build internal node distributions
		for intnode in internal_nodes:
			label_left = intnode.clades[0].name
			label_right = intnode.clades[1].name
		
			def _project(seq):
				""" composes with edit distance and projects to output,
				if seq object is iterable does this for each item and takes 
				the union of the output """
				
				result = None
				if seq.approx_split: ## list of approximation acceptors found
					composed = [self.aligner_asymm.project(s, from_right=True, minimize=False) for s in seq.approx_split]
					composed = [self.cnvtools.bprune(s, weight=6) for s in composed]
					if len(composed) > 1:
						result = self.cnvtools.bmulticommand(cnvphylo.cnvtools.COMMAND_UNION, 
																 composed, 
																 return_filename=True,
																 detmin=False)
					else:
						result = composed[0]
						
				else: ## e.g. terminal nodes
					result = self.aligner_asymm.project(seq.exact, from_right=True)
#                    result = self.app.cnvtools.bprune(
#                            self.app.aligner_asymm.project(seq.exact, from_right=True, minimize=False), weight=7)
				return result
					
			left_composed = _project(label2seq[label_left])
			right_composed = _project(label2seq[label_right])
			 
			intersect = intnode.name + suffix + "_full"
			self.cnvtools.mass_intersect_quick((left_composed, right_composed), intersect, detmin=False)
			
														
			## Strategy 1: approximate the (exact) intersect by "sampling over the posterior"
			def _sample_over_posterior():
				nrepeats=1
				npaths_per_repeat=10
				self.log.write("Sampling over posterior...")
				self.log.is_logging=False
				approxes = [None] * nrepeats
				for i in range(0, nrepeats):
					approxes[i] = self.cnvtools.bdetmin(
					  self.cnvtools.brandgenweighted(
						self.cnvtools.bproject( 
						  self.aligner_asymm.balign_pair_full(label2seq[self.diploid].exact, intersect),
						to_output=True),
					  npath=npaths_per_repeat)
					)
				#approx = self.app.cnvtools.bmulticommand(cnvphylo.cnvtools.COMMAND_UNION, approxes, return_filename=True)
				self.log.is_logging=True                
				self.log.writeln("done!")
				return approxes

			## Strategy 2: approximate the (exact) intersect by n-shortest paths
			def _nshortest_to_diploid():
				npaths_per_repeat=1000
				nrepeats=1
				self.log.write("Finding n-shortest paths to diploid...")
				self.log.is_logging=False
				approxes = [None] * nrepeats
				for i in range(0, nrepeats):
					approxes[i] = self.cnvtools.bdetmin(
					  self.cnvtools.bshortestpath(
						self.cnvtools.bproject( 
						  self.aligner_asymm.balign_pair_full(label2seq[self.diploid].exact, intersect),
						to_output=True),
					  nshortest=npaths_per_repeat),
					rmepsilon=True)
				self.log.is_logging=True                
				self.log.writeln("done!")
				return approxes

			## Strategy 3: approximate the (exact) intersect by pruning
			def _prune_intersect():
				prune_weight=6
				self.log.write("Pruning intersect...")
				self.log.is_logging=False
				approxes = [None]
				approxes[0] = self.cnvtools.bprune(intersect, weight=prune_weight)
				self.log.is_logging=True                
				self.log.writeln("done!")
				return approxes

			#approx_tmp = _sample_over_posterior()
			#approx_tmp = _nshortest_to_diploid()
			approx_tmp = _prune_intersect()
			
			approx_split_filenames = ["%s_approx_%.2d" % (intersect, i) for i in range(0, len(approx_tmp))]
			for fn in approx_split_filenames:
				with open("%s/%s.bin" % (self.working_dir, fn), "w") as fd_approx:
					fd_approx.write(approx_tmp[approx_split_filenames.index(fn)])
				
				#shutil.move(approx_tmp, "%s/%s.bin" % (self.app.working_dir, approx_filename))
			
			approx_full_filename = "%s_approx_full" % intersect            
			if len(approx_tmp) > 1:
				approx_full_tmp = self.cnvtools.bmulticommand(
								cnvphylo.cnvtools.COMMAND_UNION, approx_split_filenames, return_filename=True)
				shutil.move(approx_full_tmp, "%s/%s.bin" % (self.working_dir, approx_full_filename))
			else:
				with open("%s/%s.bin" % (self.working_dir, approx_full_filename), "w") as fd:
					fd.write(approx_tmp[0])
				
			label2seq[intnode.name] = self.NodeInfo(approx_full = approx_full_filename, 
													approx_split = approx_split_filenames, 
													exact = intersect, estimate=None)            

#            label2seq[intnode.name] = self.NodeInfo(approx_full = None, 
#                                                    approx_split = None, 
#                                                    exact = intersect, 
#                                                    estimate=None)            
			
		## now backtrack, bc we don't have a P(root) we use P(root | diploid) instead 
		internal_nodes = output_tree.get_nonterminals(order="level")
		self.log.writeln("Internal node order: " + " -> ".join([n.name for n in internal_nodes]))

		previous = output_tree.find_clades(name=self.diploid).next()
		for intnode in internal_nodes:
			if not output_tree.root == intnode:
				path = output_tree.get_path(intnode)
				if len(path) == 1:
					previous = output_tree.root
				else:
					previous = path[-2]

			## now align the previously found ancestor with the acceptor holding the distribution
			sp = self.aligner_asymm.align_pair(label2seq[previous.name].estimate, label2seq[intnode.name].exact, shortest_path=True)
			estimate = self.cnvtools.bmap(self.cnvtools.bproject(sp, to_output=True), 
											  map_type="rmweight")

			estimate_name = "%s%s_final"  % (intnode.name, suffix)
			with open("%s/%s.bin" % (self.working_dir, estimate_name), "w") as fd: 
				fd.write(estimate)
			label2seq[intnode.name] = label2seq[intnode.name]._replace(estimate = estimate_name)
			
			distance = self.aligner_symm.align_pair(label2seq[previous.name].estimate, estimate_name)
			#previous.branch_length = distance
			intnode.branch_length = distance
			
			## determine branch lengths of the external branches
			#external_nodes = [c for c in intnode.clades if c.is_terminal() and not c.name == DIPLOID_NAME]
			external_nodes = [c for c in intnode.clades if c.is_terminal()]
			for extnode in external_nodes:
				distance = self.aligner_symm.align_pair(label2seq[intnode.name].estimate, label2seq[extnode.name].estimate)
				extnode.branch_length = distance
					
		internal_labels = [c.name for c in internal_nodes]
		internal_sequences = [label2seq[l].estimate for l in internal_labels]
		return (output_tree, internal_sequences, internal_labels)
	
	
class TaskInfo(object):
	def __init__(self, label, final_tree=None, distance_matrix_pairwise=None, output_dir="", exception = None):
		self.label = label
		self.final_tree = final_tree
		self.distance_matrix_pairwise = distance_matrix_pairwise
		self.output_dir = output_dir
		self.exception = exception
		
class NoException(Exception):
	pass
	
class DualTreeError(CnvPhyloError):
	""" Exception class for this module """ 
	def __init__(self, message):
		super(DualTreeError, self).__init__(message)
	
