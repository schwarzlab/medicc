﻿""" init module """

import csv
import sys
import os
import os.path

IS_WINDOWS = os.name=="nt"
PROJECT_HOME = os.path.split(os.path.realpath(__file__))[0]
FST_DIR = os.path.abspath(PROJECT_HOME + "/../minimum_event_distance")

class CnvBaseConfig(object):
    def __init__(self, cnv_symbol_file=None, alphabet=None, pdt_parens_file = None):
        self.symbol_file = cnv_symbol_file
        self.pdt_parens_file = pdt_parens_file
        if alphabet!=None:
            self.alphabet = alphabet
        else:
            self._parse_symbol_file()            

    def _parse_symbol_file(self):
        self.alphabet = None
        
        if self.symbol_file:
            reader = csv.reader(open(self.symbol_file,"r"), delimiter=" ")
            self.alphabet = [line[0] for line in reader if len(line)>0]

    
class CnvPhyloError(Exception):
    """ Exception class for this package. All Exceptions in submodules should inherit that """ 
    def __init__(self, message):
        super(CnvPhyloError, self).__init__(message)


from . import *