
import scipy
import csv
import numpy

def to_phylip_format(distances, labels, sort=False):
    """ converts a given numpy array (distance matrix) with a given list of strings (row labels)
    into a list of strings (one per line) that is a textual representation of the
    distance matrix. If given a list of distance matrices it also expects a list of list of strings
    for the taxon names (one list of taxon names for each matrix)."""    
    lines=list()
    if isinstance(distances, numpy.ndarray):
        lines=_to_phylip_format(distances, labels, sort)
    elif isinstance(distances, list):
        for (dm, tn) in zip(*(distances,labels)):
            lines+=_to_phylip_format(dm,  tn, sort)

    return(lines)
        

def _to_phylip_format(distances, labels, sort=False):
    if sort:
        sortidx = sorted(range(0,len(labels)), key=lambda i:labels[i])
    else:
        sortidx = range(0, distances.shape[0])
        
    lines = []
    lines.append("%d\n" % len(labels))
    #for i in range(0, distances.shape[0]):
    for i in sortidx:
        line = "%-9s " % labels[i]
        line += " ".join(["%f" % val for val in distances[i, sortidx]])
        line += "\n"
        lines.append(line)
    return lines    


def parse_phylip_format(fd):
    """ Trys to construct a numpy array from a file handle for a phylip distance matrix.
   Deals with multiple distance matrices per file. Does not accept any comments or empty lines. 
   
   Returns a list of numpy arrays (the distances) and a list of lists of strings (one list of taxon 
   names per distance matrix) """
    allDistances = list()
    allTaxa = list()
    csvReader = csv.reader(fd, delimiter=" ")

    try:
        while True:
            row = next(csvReader)
            nTaxa = int(row[0])
            taxa = list()
            rows = list()
            for i in range(0,nTaxa):
                row = next(csvReader)
                taxa.append(row[0])
                rows.append([float(r) for r in row[1:] if r != ""])
            allDistances.append(numpy.array(rows))
            allTaxa.append(taxa)
    except StopIteration:
        pass
    return((allDistances, allTaxa))

