#!env python

'''
Created on 22 Sep 2011

@author: schwar01
'''
import sys
import Bio.Phylo
#import commons.tools.log
import optparse
import cnvphylo

#LOG = commons.tools.log.Log()


def main():
    usage = "USAGE: %prog <tree_file> <sequence_id>"
    parser = optparse.OptionParser(usage=usage)
    
    parser.add_option("-i", "--input-format", action="store", dest="input_format", default="phyloxml",
                      metavar="FORMAT", help="input format (def: phyloxml")
    parser.add_option("-a", "--allele_a", action="store_true", dest="allele_a", default=False,
                      help="get allele A")
    parser.add_option("-b", "--allele_b", action="store_true", dest="allele_b", default=False,
                      help="get allele B")
    parser.add_option("-t", "--total", action="store_true", dest="total", default=False,
                      help="sum A and B to total CN")
    parser.add_option("-n", "--number_of_sequences", action="store", dest="nseqs", default=-1, type="int",
                      help="number of sequences to report, defaults to all (-1)")
    parser.add_option("-l", "--no-label", action="store_true", dest="no_label", default=False,
                      help="don't print label")
    parser.add_option("-s", "--separator", action="store_true", dest="separator", default=False,
                      help="keep chromosome separator")
    

    (options, args) = parser.parse_args()
    
    if len(args) != 2:
        parser.error("incorrect number of arguments")
    
    strSourceFile = args[0]
    sequenceID = args[1]
    
    tree = Bio.Phylo.read(strSourceFile, options.input_format)
    
    countPrints = 0
    countClades = 0
    for clade in tree.find_clades(name=sequenceID):
        seqA = ""
        seqB = ""
        idA = ""
        idB = ""
        for seqobj in clade.sequences:
            symb = seqobj.symbol
            id = seqobj.accession.value
            if not options.separator: 
                seq = seqobj.name.replace("X", "")
            else:
                seq = seqobj.name
            if symb == "A":
                seqA = seq
                idA = id
            elif symb == "B":
                seqB = seq
                idB = id

            do_print = (symb == "A" and options.allele_a) or (symb == "B" and options.allele_b)
            if do_print and (countClades < options.nseqs or options.nseqs < 0): 
                if not options.no_label:
                    print ">%s" % id
                print seq
                countPrints += 1
        if options.total:
            t = [str(int(seqA[i]) + int(seqB[i])) if seqA[i] != "X" else "X" for i in range(len(seqA)) ]
            totseq = "".join(t)
            if (countClades < options.nseqs or options.nseqs < 0): 
                if not options.no_label:
                    print ">%s_plus_%s" % (idA, idB)
                print totseq
                countPrints += 1

        countClades += 1            

    print >> sys.stderr, "%d clades found" % countClades
# ======================================================================    

if __name__ == '__main__':
    sys.exit(main())
