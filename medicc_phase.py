#!env python

'''
Created on 04/03/2014

@author: rfs

Phasing alleles for MEDICC, desc file as in MEDICC:


label1 path_to_major_cn_file1 path_to_minor_cn_file1
label2 path_to_major_cn_file2 path_to_minor_cn_file2
[...]
labelN path_to_major_cn_fileN path_to_minor_cn_fileN

Paths can be either absolute or relative to the folder the description file is contained in.
Empty lines are ignore at the beginning and end of the file only.
'''

import sys
import os
sys.path.append(sys.path[0] + os.sep + "lib" + os.sep + "fstframework")

from optparse import OptionParser
from commons.tools import log
import numpy
from Bio import Phylo
import Bio.SeqIO
#import glob
import cnvphylo.cnvtools
import cnvphylo.trees
import subprocess
from subprocess import Popen 
import shlex
from cnvphylo.dualtree import DualTreeFactory, DualTreeError, MEDICCApp, MEDICCPhaseApp

LOG = log.Log()

def main():
    """ the main method """

    usage = "USAGE: %prog <description_file> <output_dir>"    
    parser = DualTreeFactory.create_option_parser_phasing(usage)

    (options, args) = parser.parse_args()
    
    if len(args) != 2:
        parser.error("incorrect number of arguments")
    
    LOG.is_logging = options.verbose
    
    desc_file = args[0]
    output_dir = args[1]
    
    if not os.path.exists(desc_file):
        LOG.writeln("Description file does not exist!")
        sys.exit(1)

    if not os.path.isfile(desc_file):
        LOG.writeln("Description path is not a file!")
        sys.exit(1)

    if not os.path.exists(output_dir):
        os.mkdir(output_dir)
    
    
    with open(desc_file, "r") as fd_desc:
        path = os.path.abspath(os.path.dirname(desc_file))
        lines = fd_desc.read().strip().splitlines()
        
        cnvdata = list()
        for line in lines:
            label, major_cn, minor_cn = line.split()
            if not os.path.isabs(major_cn):
                major_cn = path + "/" + major_cn
            if not os.path.isabs(minor_cn):
                minor_cn = path + "/" + minor_cn
            cnvdata.append((label, major_cn, minor_cn))

    ##options.verbose=False
    app = cnvphylo.dualtree.MEDICCPhaseApp(cnvdata, output_dir, options)
    app.run()



if __name__ == '__main__':
    retval = main()
    LOG.writeln("finished!")
    sys.exit(retval)
   

