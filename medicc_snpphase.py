﻿#!env python

'''
Created on 04/03/2014

@author: rfs & tbkw

Phasing alleles for MEDICC, desc file as in MEDICC:

label1 path_to_major_cn_file1 path_to_minor_cn_file1
label2 path_to_major_cn_file2 path_to_minor_cn_file2
[...]
labelN path_to_major_cn_fileN path_to_minor_cn_fileN

Paths can be either absolute or relative to the folder the description file is contained in.
Empty lines are ignore at the beginning and end of the file only.
'''

import sys
import os
sys.path.append(sys.path[0] + os.sep + "lib" + os.sep + "fstframework")
import logging
from optparse import OptionParser
import numpy
import cPickle as pickle

import cnvphylo
import cnvphylo.phasing


def main():
    """ the main method """
    usage = "USAGE: %prog <logr_baf_file> <ascat_segments> <output_dir>"    
    
    parser = OptionParser(usage)

    parser.add_option("-d", "--delimiter", action = "store", dest = "delim",
                        default = "\t", help = "Delimiter for the files, default='\t'")
    parser.add_option("-v", dest="verbose", default=0, action="count",
                        help="increment output verbosity; may be specified multiple times")
    parser.add_option("-r", "--dpi", action = "store", dest = "dpi", type="int",
                        default = 800, help = "DPI for phasing images")
    parser.add_option("-n", "--no-plot", dest="no_plot", action="store_true", default=False,
                        help="don't plot the phasing results")
    parser.add_option("-m", "--no-diploid", dest="no_diploid", action="store_true", default=False,
                        help="don't add virtual diploid")
    parser.add_option("-b", "--basename", action = "store", dest = "basename", type="str",
                        default = None, help = "Basename for the segment files, defaults to the name of the output dir")
    parser.add_option("-q", "--sequencing", dest="sequencing", action = "store_true", default=False, 
                          help="Will change the way SNPs are assigned to distributions by using their read depths.")
    parser.add_option("-s", "--sigma-file", dest="sigmafile", type = "str", action = "store", default=None, 
                          help="Specifies a file of sigmas to use in assigning snps to distributions according to their read depths.")
    parser.add_option("-g", "--germline-snps", dest="germline_snps", action = "store_true", default=False, 
                          help="Whether to use column indicating potentially deleterious snps in logr_baf_file")
    parser.add_option("-p", "--purity-file", dest="purity_file", type = "str", action = "store", default=None, 
                          help="Gives a file containing all the purities related to the samples. If given the purities will determine what is reference")

    (options, args) = parser.parse_args()

    ####Write test of purity rank
    print(args)
    if len(args) != 3:
        parser.error("incorrect number of arguments")

    # For each -v passed on the commandline, a lower log.level will be enabled.
    # log.ERROR by default, log.INFO with -vv, etc.
    #logging.basicConfig(level=max(logging.ERROR - (options.verbose * 10), 1))
    #logging.info('Started')
    #raw_input("Started")
    #raw_input(options.sequencing)
    
    logr_file = args[0]

    segment_file = args[1]

    output_dir = args[2]

    
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)


    
    if options.basename is None:
        options.basename = os.path.basename(os.path.normpath(output_dir))




    try:    
        logging.info("Reading data...")



        reader = cnvphylo.phasing.SNPDataReader(logr_file, segment_file,delimiter=options.delim, 
                                                                        usingSequencingData=options.sequencing,
                                                                        depth_sigmas_file=options.sigmafile,
                                                                        germline_deleterious=options.germline_snps)

        baf_data = reader.read()
        logging.info("Finished reading data...")

    except PhasingError as ex:
        LOG.writeln(ex)
        return(1)

    #logging.info("Phasing data...")
    phaser = cnvphylo.phasing.BAFPhasing(baf_data,usingSequencingData=options.sequencing,
                                                    germline_deleterious=options.germline_snps,
                                                    output_dir=output_dir,
                                                    purity_file=options.purity_file,
                                                    delimiter=options.delim)
    phasing_results = phaser.phase()
    #phaser.consensus_phase()

    #This is where we will temporarily put the code to output the swapped segs, eventually put in a function.
    ##So now we need to decide what we values we've going to print out...
    #Note this is how the pval stars are determined:
    #note pvals are exactly the same as pvalues
    #pvalues_txt[pvalues>=0] = '****'
    #pvalues_txt[pvalues>=0.0001] = '***'
    #pvalues_txt[pvalues>=0.001] = '**'
    #pvalues_txt[pvalues>=0.01] = '*'
    #pvalues_txt[pvalues>0.05] = ''

    index_list = []
    #raw_input(numpy.where(phasing_results.pvals <0.001))
    ###So here is where we want to pull out the samples

    indices = numpy.where(phasing_results.pvals <0.001)[0]
    samples = numpy.where(phasing_results.pvals <0.001)[1]
    #indices = numpy.unique(indices)
    #raw_input(indices)
    for x in range(0, len(indices)):
        int(indices[x])
        index_list.append(indices[x])

    swapped_segments = phasing_results.joint_segments.iloc[index_list]
    #So now we need to write out the sample index as well
    swapped_segments['sample'] = samples


    ##Output segments...
    ##These will be going to the directories as follows:
    ##Directory for 
    ##Top level:snp_band_classification band_switching
    # snp_band_categories_dir = output_dir + "/" + "snp_band_categories"
    # if options.germline_snps:
    #     if not os.path.exists(snp_band_categories_dir):
    #         os.mkdir(snp_band_categories_dir)


    band_switch_dir = output_dir + "/" + "msai_results"
    fasta_output_dir= band_switch_dir + "/" + "medicc_fasta"
    snp_band_categories_dir = band_switch_dir
    ##then: all_chrom_images, band_switch_chrom_images
    all_chrom_images_dir = band_switch_dir + "/" + "all_chrom_images"
    band_switch_chrom_images_dir = band_switch_dir + "/" + "msai_chrom_images"
    if not os.path.exists(band_switch_dir):
        os.mkdir(band_switch_dir)
    if not os.path.exists(all_chrom_images_dir):
        os.mkdir(all_chrom_images_dir)
    if not os.path.exists(band_switch_chrom_images_dir):
        os.mkdir(band_switch_chrom_images_dir)
    if not os.path.exists(fasta_output_dir):
        os.mkdir(fasta_output_dir)

    #patient = os.path.basename(output_dir)
    ##segments and swapped segments should go into the top level
    swapped_segments.to_csv(band_switch_dir + "/" + options.basename + "_msai_segs.txt", sep='\t')
    #Problem now of recovering the original segments that these were part of.

    #Get chromosomes which exhibit the flipping so that they can be printed
    chroms_to_print = phasing_results.joint_segments.iloc[index_list][["chr"]]
    chroms_to_print = chroms_to_print["chr"].tolist()

    #logging.info("Writing results...")   
    writer = cnvphylo.phasing.BAFPhasingWriter(baf_data, phasing_results, germline_deleterious=options.germline_snps, baf_file=logr_file)
    writer.write_segments_FASTA(fasta_output_dir, options.basename, add_diploid=not options.no_diploid)
    writer.write_chr_ref_sample(band_switch_dir, options.basename + "_ref_segs.txt")
    writer.write_segments_csv(band_switch_dir, options.basename + "_pyclone_format_segments.txt")
    if options.germline_snps:
        writer.write_ubiq_loh_preserved_del_snps(snp_band_categories_dir, options.basename, phasing_results, options.delim)

    logging.info("Plotting results...")
    if not options.no_plot:
        plotter = cnvphylo.phasing.BAFPhasingPlot(baf_data, phasing_results, usingSequencingData=options.sequencing, germline_deleterious=options.germline_snps)
        for chr in baf_data.chrs:
                logging.info("Chromosome " + str(chr))
                plotter.plot(chr, os.path.join(all_chrom_images_dir, "phasing_chr" + str(chr) + ".png"), dpi=options.dpi)
        for chr in baf_data.chrs:
            if chr in chroms_to_print:
                logging.info("Chromosome " + str(chr))
                plotter.plot(chr, os.path.join(band_switch_chrom_images_dir, "phasing_chr_" + str(chr) + ".png"), dpi=options.dpi)
                # logging.info("Chromosome " + str(chr))
                # plotter.figure_plot(chr, os.path.join(band_switch_chrom_images_dir, "phasing_chr_figure" + str(chr) + ".png"), dpi=1600)#options.dpi



    return 0


if __name__ == '__main__':
    retval = main()
    if retval == 0:
        logging.info("finished!")
    else:
        logging.info("error!")
    sys.exit(retval)
   

                                                                                                                                               