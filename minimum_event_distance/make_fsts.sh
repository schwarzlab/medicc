#!/bin/bash

## make xt fst
## create only symbol table

alpha="0123456789a"
#alpha="0123"

alphalen=${#alpha}
repdel=$(($alphalen - 1))
repamp=$(($alphalen - 2))
echo "Alphabet: $alpha"
echo "Number of amplification steps $repamp"
echo "Number of deletion steps $repdel"

echo -n 'Creating symbol tables...'
../create_symbol_tables.py ./ -n 1024 -x X -a "$alpha" -p pdtparens.txt -s cnv.symbols
echo 'done.'

## no length scoring
echo -n 'Creating one step amp and del FSTs...'
../create_cnv_fst.py -c del -a "$alpha" -x X ed_del_step1_nls.txt
../create_cnv_fst.py -c amp -a "$alpha" -x X ed_amp_step1_nls.txt 

fstcompile --isymbols=cnv.symbols --osymbols=cnv.symbols --keep_isymbols --keep_osymbols ed_amp_step1_nls.txt ed_amp_step1_nls.fst
fstcompile --isymbols=cnv.symbols --osymbols=cnv.symbols --keep_isymbols --keep_osymbols ed_del_step1_nls.txt ed_del_step1_nls.fst
echo 'done.'

echo -n 'Expanding amp FST'
../fstlib/fst_multicmd.py -c fstcompose -r "$repamp" ed_amp_step1_nls.fst -ms > ed_amp_full_nls.fst
echo 'done'

echo -n 'Expanding del FST'
../fstlib/fst_multicmd.py -c fstcompose -r "$repdel" ed_del_step1_nls.fst -ms > ed_del_full_nls.fst
echo 'done.'

echo -n 'Combining amp and del FST...'
fstarcsort --sort_type=olabel ed_del_full_nls.fst | fstcompose - ed_amp_full_nls.fst > ed_asymm_nls_tmp.fst
echo 'done.'

echo -n 'Minimizing combined FST...'
../fstlib/fstencode_and_minimize.sh ed_asymm_nls_tmp.fst > ed_asymm_nls_tmp2.fst
echo 'done.'

echo -n 'Adding Zeros to combined FST...'
fstprint ed_asymm_nls_tmp2.fst | ../fstlib/fst_add_zeros.py | fstcompile --isymbols=cnv.symbols --osymbols=cnv.symbols --keep_isymbols --keep_osymbols > ed_asymm_nls.fst
echo 'done.'

echo -n 'Building symmetric FST...'
fstinvert ed_asymm_nls.fst | fstarcsort --sort_type="olabel" | fstcompose - ed_asymm_nls.fst > ed_symm_nls_tmp.fst
#fstinvert ed_asymm_nls.fst | fstunion - ed_asymm_nls.fst | fstrmepsilon > ed_symm_nls.fst
echo 'done.'

echo -n 'Minimizing symmetric FST...'
../fstlib/fstencode_and_minimize.sh ed_symm_nls_tmp.fst > ed_symm_nls.fst
echo 'done.'

## length scoring
echo -n 'Creating one step amp and del FSTs...'
../create_cnv_fst.py -c del -a "$alpha" -x X -e 0.1 ed_del_step1_ls.txt
../create_cnv_fst.py -c amp -a "$alpha" -x X -e 0.1 ed_amp_step1_ls.txt 

fstcompile --isymbols=cnv.symbols --osymbols=cnv.symbols --keep_isymbols --keep_osymbols ed_amp_step1_ls.txt ed_amp_step1_ls.fst
fstcompile --isymbols=cnv.symbols --osymbols=cnv.symbols --keep_isymbols --keep_osymbols ed_del_step1_ls.txt ed_del_step1_ls.fst
echo 'done.'

echo -n 'Expanding amp FST'
../fstlib/fst_multicmd.py -c fstcompose -r "$repamp" ed_amp_step1_ls.fst -ms > ed_amp_full_ls.fst
echo 'done'

echo -n 'Expanding del FST'
../fstlib/fst_multicmd.py -c fstcompose -r "$repdel" ed_del_step1_ls.fst -ms > ed_del_full_ls.fst
echo 'done.'

echo -n 'Combining amp and del FST...'
fstarcsort --sort_type=olabel ed_del_full_ls.fst | fstcompose - ed_amp_full_ls.fst > ed_asymm_ls_tmp.fst
echo 'done.'

echo -n 'Minimizing combined FST...'
../fstlib/fstencode_and_minimize.sh ed_asymm_ls_tmp.fst > ed_asymm_ls_tmp2.fst
echo 'done.'

echo -n 'Adding Zeros to combined FST...'
fstprint ed_asymm_ls_tmp2.fst | ../fstlib/fst_add_zeros.py | fstcompile --isymbols=cnv.symbols --osymbols=cnv.symbols --keep_isymbols --keep_osymbols > ed_asymm_ls.fst
echo 'done.'


#rm ed_amp_step1_nls.txt ed_amp_step1_nls.fst ed_del_step1_nls.txt ed_del_step1_nls.fst ed_amp_full_nls.fst ed_del_full_nls.fst ed_asymm_nls_tmp.fst
#rm ed_amp_step1_ls.txt ed_amp_step1_ls.fst ed_del_step1_ls.txt ed_del_step1_ls.fst ed_amp_full_ls.fst ed_del_full_ls.fst ed_asymm_ls_tmp.fst
