\name{medicc.test.for.star.topology}
\alias{medicc.test.for.star.topology}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
  Test for star topology
}
\description{
  This function tests for the null hypothesis that a given MEDICC distance matrix represents
  a star topology.
}
\usage{
medicc.test.for.star.topology(D)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{D}{
    The MEDICC distance matrix.
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
  The p-value of the test.
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
Roland F Schwarz <rfs32@cam.ac.uk>
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
