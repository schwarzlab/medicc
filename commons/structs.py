'''
Created on 7 May 2011

@author: rfs
'''

class NamedList(object):
    """ a list with names """
    
    def __init__(self, items, names=None):
        self.items=items
        
        if names:
            self.names=names
        else:
            if self.items:
                self.names = [""] * len(self.items)


