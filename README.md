# MEDICC - Minimum Event Distance for Intra-tumour Copy number Comparisons #
*UPDATED version 14/03/2017*

## This is the old version of MEDICC. We strongly recommend using MEDICC2: https://bitbucket.org/schwarzlab/medicc2 ##



## Download: ##

* Clone the MEDICC repository using "git clone https://bitbucket.org/schwarzlab/medicc.git"
* The FST framework is now part of the main repository, no need to download it separately anymore

## Other requirements: ##

* Unix style operating system or Cygwin on Windows
* Python 2.7x with current Biopython, Numpy/Scipy, weblogolib 3.3 if logos are required for ancestral reconstruction
* OpenFST 1.6.2 (http://www.openfst.org) compiled and in your PATH. You must ./configure OpenFST with at least the --enable-pdt and --enable-far flags. Use this exact version of OpenFST, in particular the new OpenFST versions do not seem to compile on cygwin anymore.
* Optional: Weblogolib 3.3 to create weblogos for ancestors, has to be manually enabled with --enable-logo

## Installation: ##

* Navigate to the cExtensions subfolder, compile using "make" under Cygwin or Unix

## Running & Testing: ##

Phasing and tree inference have been separated in this release.

* Executable "medicc_phase.py" takes two arguments (+options) [currently untested]:
	1) Description file indicating the position of the major and minor CNV sequences
	2) Output directory to write the phased copy number profiles

* Executable "medicc.py" takes two arguments (+options):
	1) Description file indicating the position of the phased major and minor CNV sequences
	2) Output directory to write the reconstructed tree etc.

* To check the installation run from the medicc folder: ./medicc.py --help

* If that succeeds, run the test examples using:

./medicc_phase.py examples/example1/desc.txt examples/example1.out -v
./medicc.py examples/example1.out/desc_phased.txt examples/example1.out -v

## Troubleshooting: ##

* **OpenFST:** The OpenFST make files install parts of the OpenFST shared libraries in a "fst" subfolder under the chosen install prefix, by default /usr/local/lib/fst 
You need to make sure this folder is part of both the runtime link search path (e.g. add it to ldconfig) as well as the compile time link search path.
For the latter there is a variable in the cExtensions makefile that sets the -L flag that is passed to the linker. It is by default set to /usr/local/lib/fst.
If you install the OpenFST libraries to a custom location the cExtensions makefile needs to be edited to match that.
* **Weblogolib:** Make sure you use weblogolib 3.3 for now, not the new 3.4 version in which function calls have changed without it being mentioned in the changelog.

## Contact: ##
Email questions, feature requests and bug reports to **Roland Schwarz, roland.schwarz@mdc-berlin.de**.

## Please cite: ##

PLoS Comput Biol. 2014 Apr 17;10(4):e1003535. doi: 10.1371/journal.pcbi.1003535.  
**Phylogenetic quantification of intra-tumour heterogeneity.**  
Schwarz RF, Trinh A, Sipos B, Brenton JD, Goldman N, Markowetz F.

PLoS Med. 2015 Feb 24;12(2):e1001789. doi: 10.1371/journal.pmed.1001789.  
**Spatial and temporal heterogeneity in high-grade serous ovarian cancer: a phylogenetic analysis.**  
Schwarz RF, Ng CK, Cooke SL, Newman S, Temple J, Piskorz AM, Gale D, Sayal K, Murtaza M, Baldwin PJ, Rosenfeld N, Earl HM, Sala E, Jimenez-Linan M, Parkinson CA, Markowetz F, Brenton JD.

March 2017, Roland Schwarz

