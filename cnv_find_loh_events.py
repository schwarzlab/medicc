#!/usr/bin/python

'''
Created on Oct 24, 2011

@author: rfs
'''

import cnvphylo
import sys
from optparse import OptionParser
from commons.tools import log
import os
import numpy
import Bio
import Bio.Phylo
import Bio.Phylo.PhyloXMLIO
import Bio.SeqIO
import Bio.SeqRecord
import Bio.Seq
import Bio.Alphabet
from cnvphylo import cnvtools
import fst.factory

LOG = log.Log()

def find_loh_events(parent, child, depth=0):
    """ Detects LOH events in the direction 'parent' -> 'child', i.e. events
    where one of the alleles is reduced to zero.
    'parent' and 'child' are clade objects from Bio.Phylo. """
    depth += 1 ## recursion depth
    
    parent_seq_A = parent.sequences[0].name ## still saved in 'name' object due to xml restrictions
    parent_seq_B = parent.sequences[1].name ## still saved in 'name' object due to xml restrictions

    child_seq_A = child.sequences[0].name ## still saved in 'name' object due to xml restrictions
    child_seq_B = child.sequences[1].name ## still saved in 'name' object due to xml restrictions
    
    loh_events = list()
    
    assert(len(parent_seq_A) == len(parent_seq_B) == len(child_seq_A) == len(child_seq_B))
    
    def find_events(parent_seq, child_seq, allele):
        """ helper fun that does the actual event detection """
        
        parent_seq = parent_seq.replace('X','')
        child_seq = child_seq.replace('X','')
        
        ## add end conditions to make sure the last event terminates
        parent_seq += 'Z'
        child_seq += 'Z'
        
        length_counter = 0
        start = -1      
        for i in range(0, len(parent_seq)): ## all sequences are of same length
            if child_seq[i] == '0' and parent_seq[i] != '0':
                if start == -1:
                    start = i   ## remember start 
                length_counter += 1
            else:
                if length_counter != 0: ## event ends here
                    loh_events.append((start+1, start+length_counter, depth, allele))
                    length_counter = 0 ## reset counter
                    start = -1 
            
    find_events(parent_seq_A, child_seq_A, 'A')
    find_events(parent_seq_B, child_seq_B, 'B')
    
    ## now recurse
    if len(child.clades)==2:
        loh_events += find_loh_events(child, child.clades[0], depth)
        loh_events += find_loh_events(child, child.clades[1], depth)
    elif len(child.clades)==1:
        print >> sys.stderr, "Error: Child with only one clade!"
        sys.exit(1)
    
    return loh_events        
            
def print_loh_events(loh_events):
    """ prints the list in a R compatible format """
    
    print "start end level allele"
    for start, end, level, allele in loh_events:
        print "%d %d %d %s" % (start, end, level, allele)
    
def main():
    """ the main method """
    
    usage = "USAGE: %prog <tree>"
    
    parser = OptionParser(usage)

    parser.add_option("-v", "--verbose", action = "store_true", dest = "verbose",
                      default = False, help = "be verbose?")
    parser.add_option("-n", "--diploid-name", action = "store", dest = "diploid_name",
                      default = "diploid", help = "name of the diploid")
    
    
    
    (options, args) = parser.parse_args()
   
    if len(args) != 1:
        parser.error("incorrect number of arguments")

    LOG.is_logging = options.verbose
    
    input_file = args[0]

    if not os.path.exists(input_file):
        print >> sys.stderr, "Input file does not exist!"
        sys.exit(1)
    

    phylogenies = Bio.Phylo.PhyloXMLIO.read(input_file)
    for t in phylogenies:
        try:
            clades = [c for c in t.find_clades(name=options.diploid_name)]
            if len(clades)==0:
                clades = [c for c in t.find_clades(name=".*" + options.diploid_name + ".*")]
                if len(clades)==0:
                    clades = [c for c in t.find_clades(name=".*diploid.*")]
                    if len(clades)==0:
                        print >> sys.stderr, ("Diploid '%s' not found!" % options.diploid_name)
                        sys.exit(1)
            
            diploid = clades[0]
            print >> sys.stderr, ("Diploid is '%s'" % diploid.name)
            t.root_with_outgroup(diploid)
            
        except ValueError:
            print >> sys.stderr, ("Diploid '%s' not found!" % diploid.name)
            sys.exit(1)
            
        ## now all trees are rooted with outgroup
        ## recursion start is the diploid
        diploid_index = [c.name for c in t.root.clades].index(diploid.name)
        loh_events = list()
        loh_events += find_loh_events(t.root.clades[diploid_index], t.root.clades[0])
        loh_events += find_loh_events(t.root.clades[diploid_index], t.root.clades[1])

        print_loh_events(loh_events)
        
if __name__ == '__main__':
    sys.exit(main())
   

  