# from Bio import Phylo
import os
import glob
import pandas as pd
from ete3 import Tree
from ete3 import Phyloxml
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--choice", "-c", choices = ['l', 'a'], default='a', help="write CNPs", type=str)
parser.add_argument("--input", "-i", type=str, required=True, help="input file in xml format")
parser.add_argument("--output", "-o", type=str, required=True, help="output file")

args = parser.parse_args()

project_tree = Phyloxml()

names = []
allele1 = []
allele2 = []

treeFile = args.input

project_tree.build_from_file(treeFile)
t = project_tree.get_phylogeny()[0]
lca = t.get_common_ancestor(t.get_leaves())

if args.choice == "a":
    for node in t.traverse('levelorder'):
        names.append(node.name)
        allele1.append(node.phyloxml_clade.sequence[0].name)
        allele2.append(node.phyloxml_clade.sequence[1].name)

        tree_df = pd.DataFrame({'leaf': names, 'allele1': allele1, 'allele2': allele2})
        tree_df = tree_df[['leaf', 'allele1', 'allele2']]

        tree_df.replace(to_replace=lca.name, value=str(lca.name+'*'), inplace=True)

        tree_df.to_csv(args.output, sep='\t')

elif args.choice == "l":
    allele1.append(lca.phyloxml_clade.sequence[0].name)
    allele2.append(lca.phyloxml_clade.sequence[1].name)
    lca_df = pd.DataFrame({'leaf': lca.name, 'allele1': allele1, 'allele2': allele2})
    lca_df.to_csv(args.output, sep="\t")
