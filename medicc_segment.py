﻿#!env python

'''
Created on 04/03/2014

@author: rfs

Phasing alleles for MEDICC, desc file as in MEDICC:


label1 path_to_major_cn_file1 path_to_minor_cn_file1
label2 path_to_major_cn_file2 path_to_minor_cn_file2
[...]
labelN path_to_major_cn_fileN path_to_minor_cn_fileN

Paths can be either absolute or relative to the folder the description file is contained in.
Empty lines are ignore at the beginning and end of the file only.
'''

import sys
import os
sys.path.append(sys.path[0] + os.sep + "lib" + os.sep + "fstframework")
import logging
from optparse import OptionParser
import numpy

import cnvphylo
import cnvphylo.phasing
import cnvphylo.ascat

def main():
    """ the main method """
    usage = "USAGE: %prog <logr_baf_file> <output_dir>"    
    
    parser = OptionParser(usage)

    parser.add_option("-d", "--delimiter", action = "store", dest = "delim",
                        default = "\t", help = "Delimiter for the files, default='\t'")
    parser.add_option("-v", dest="verbose", default=0, action="count",
                        help="increment output verbosity; may be specified multiple times")
    parser.add_option("-g", "--germline-file", action="store", dest="germline_file",
                      default = None, type="str", help = "Optional germline file for segmentation")

        

    (options, args) = parser.parse_args()

    if len(args) != 2:
        parser.error("incorrect number of arguments")

    # For each -v passed on the commandline, a lower log.level will be enabled.
    # log.ERROR by default, log.INFO with -vv, etc.
    logging.basicConfig(level=max(logging.ERROR - (options.verbose * 10), 1))
    logging.info('Started')
    
    logr_file = args[0]
    output_dir = args[1]
    
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)
    
    try:    
        logging.info("Reading data...")
        reader = cnvphylo.phasing.SNPDataReader(logr_file, None, delimiter=options.delim)
        snp_data = reader.read()

        if options.germline_file is not None:
            germline_reader = cnvphylo.phasing.SNPDataReader(options.germline_file, None, options.delim)
            germline_data = germline_reader.read()
    except PhasingError as ex:
        logging.error(ex)
        return(1)

    ascat = cnvphylo.ascat.ASCAT(snp_data)
    ascat.predict_germline_genotypes()

    return 0


if __name__ == '__main__':
    retval = main()
    if retval == 0:
        logging.info("finished!")
    else:
        logging.info("error!")
    sys.exit(retval)
   

