#!ENV python
""" Create kernel fsts for CNV project (separate signal and noise parts) """

import sys
from optparse import OptionParser
from fstlib.fst import fst
from fstlib.fst.factory import FSTFactory

 

usage = "USAGE: %prog [outfile]"

parser = OptionParser(usage)

parser.add_option("-c", "--choice", action = "store", dest = "choice", default = "c", 
				  help = "which fst to create: asymmetric 1-step CNV FST with length scoring (l), asymmetric 1-step CNV FST without length scoring (m), \
					asymmetric 1-step amplification FST (amp), asymmetric 1-step deletion FST (del)")
parser.add_option("-a", "--alphabet", action = "store", dest = "alphabet", default = "", 
				  help = "alphabet to use if not 0:f")
parser.add_option("-x", "--separator", action="store", dest = "separator", default = None, help = "add separator")
parser.add_option("-z", "--zeros", action="store_true", dest = "zeros", default = False, help = "add zero transitions")
parser.add_option("-e", "--extend-cost", action="store", dest = "extend_cost", default = 0, type='float', help = "extend cost")

(options, args) = parser.parse_args()

if len(args)>1:
	parser.error('Invalid number of arguments')

if options.alphabet == "":
	alphabet = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f']
else:
	alphabet = [s for s in options.alphabet] 

if options.choice == "l":
	fst_signal = FSTFactory.create_cnv_fst_asymm_lengthscoring(alphabet, separator= options.separator, add_zeros=options.zeros)
elif options.choice == "m":
	fst_signal = FSTFactory.create_cnv_fst_asymm_no_lengthscoring(alphabet, separator=options.separator, add_zeros=options.zeros)
elif options.choice == 'amp':
	fst_signal = FSTFactory.create_fst_1step_amp(alphabet, options.extend_cost, options.separator)
elif options.choice == 'del':
	fst_signal = FSTFactory.create_fst_1step_del(alphabet, options.extend_cost, options.separator)

else:
	FSTError('Invalid choice %s' % option.choice)

if len(args)==1:
	with open(args[0], 'wb') as fd:
		fd.write(str(fst_signal))
else:
	sys.stdout.write(str(fst_signal))

