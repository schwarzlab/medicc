#!env python
""" Creates .fst files from a FASTA file, representing the contained sequences as acceptors.
If several sequences have the same name, a probabilistic automaton is created. Pseudocounts can
be added if desired. Make sure the sequences having the same name are also of identical length. """

from Bio import SeqIO
import sys
import os
from fst.fst import *
from fst.factory import FSTFactory
from optparse import OptionParser
import subprocess
from subprocess import CalledProcessError
from subprocess import Popen

##TMPPATH = "/tmp"

usage = "USAGE: %prog <FASTA file> <output dir>"

parser = OptionParser(usage)

parser.add_option("-r", "--real-semiring", action = "store_true", dest = "use_real_semiring", default = False,
                  help = "use real semiring (default log)?")

parser.add_option("-p", "--pseudo-counts", action = "store", type = "int", dest = "pseudo_counts", default = 0,
                  help = "number of pseudo counts to add (default 0)")

##parser.add_option("-u", "--union", action = "store", dest = "union", default = False,
##                  help = "combine all sequences into one unified FST with the given filename")

(options, args) = parser.parse_args()

if len(args) != 2:
    parser.error("incorrect number of arguments")
    sys.exit(1)
    
if not(os.path.exists(args[0])):
    print >> sys.stderr, "File does not exist."
    sys.exit(1)
    
if not(os.path.exists(args[1])):
    os.mkdir(args[1])

if options.use_real_semiring:
    semiring = Fst.Semiring.REAL
else:
    semiring = Fst.Semiring.LOG

with open(args[0]) as handle:
    filenames = {}
    data = [record for record in SeqIO.parse(handle, "fasta")]
    alphabet = list(set("".join([str(s.seq) for s in data])))
    ids = set([seq.id for seq in data])
    for id in ids:
        print >> sys.stderr, "Processing " + id
        sys.stderr.flush()

        name = id.replace("/","_")
        name = name.replace("|","_")
        name = name.replace("[","_")
        name = name.replace("]","_")
        name = name.replace(" ","_")
        print >> sys.stderr, "Corrected name: " + name
        sys.stderr.flush()

        seqs = [seq for seq in data if seq.id == id]
        
        #if len(seqs) > 1:
        fsa = str(FSTFactory.create_fixed_length_hmm_from_sequences(seqs, alphabet, 
                    pseudo_counts = options.pseudo_counts, semiring = semiring, to_lower = False))
        #else:
        #    fsa = str(FSTFactory.create_fsa_from_sequence(seq, semiring, True))
        
        fullpath = "%s/%s.fst" % (args[1], name)
        if os.path.exists(fullpath):
            print >> sys.stderr, "Warning! File %s.fst already exists! Replacing..." % name
        
        print "%s.fst" % name
        with open(fullpath,"w") as fd_out:
            fd_out.write(fsa)
            filenames[name] = fullpath ## save for later use















