#!/usr/bin/python
"""Create semi-global alignment transducer from scoring matrix 
and affine gap cost model"""

from Bio.SubsMat import MatrixInfo
import sys
from optparse import OptionParser
from fst.fst import *
from fst.factory import FSTFactory


#DNAEXP = {('A', 'A'): 2.8512358587881401, ('A', 'T'): 3.0396830090087774, ('T', 'T'): 1.8974875643790847, ('T', 'A'): 3.0396830090087774, ('C', 'A'): 3.7190999967568206, ('C', 'T'): 3.2662887680514583, ('G', 'G'): 2.8028092648826655, ('T', 'C'): 3.2662887680514583, ('G', 'A'): 3.6155593178159804, ('G', 'T'): 2.8398505365630147, ('A', 'G'): 3.6155593178159804, ('C', 'G'): 3.3569853295222702, ('C', 'C'): 2.4181654845880867, ('T', 'G'): 2.8398505365630147, ('G', 'C'): 3.3569853295222702, ('A', 'C'): 3.7190999967568206}
DNAEXP = {('A', 'A'): 2.3797654605525818, ('A', 'T'): 3.1532202470020141, ('T', 'T'): 2.3500179409534674, ('T', 'A'): 3.1532202470020141, ('C', 'A'): 3.1664024667929485, ('C', 'T'): 3.1572330934783825, ('G', 'G'): 2.392298397615908, ('T', 'C'): 3.1572330934783825, ('G', 'A'): 3.1828086852453756, ('G', 'T'): 3.1610199086227939, ('A', 'G'): 3.1828086852453756, ('C', 'G'): 3.1714878096208929, ('C', 'C'): 2.4101012294965733, ('T', 'G'): 3.1610199086227939, ('G', 'C'): 3.1714878096208929, ('A', 'C'): 3.1664024667929485}

usage = "USAGE: %prog output_file"

parser = OptionParser(usage)

parser.add_option("-o","--gap-open", action="store", type="float", dest="gap_open", default="10", metavar="FLOAT", 
                  help="gap open cost (must be > 0)")
parser.add_option("-e","--gap-extend", action="store", type="float", dest="gap_extend", default="1", metavar="FLOAT",
                  help="gap extend cost (must be > 0)")
parser.add_option("-s","--substitution-matrix", action="store", type="string", dest="substmat", default="EDNAFULL", 
                  help = "substitution matrix to use; must be one of %s" % ", ".join(MatrixInfo.available_matrices))
parser.add_option("-m","--multiplier", action = "store", type = "int", dest = "mult", default = 1, 
                  help = "multiplier for substitution matrix scores")
parser.add_option("-c", "--choice", action = "store", dest = "choice", default = "a", 
                  help = "which fst to create, choice of alignment (a) or edit-distance (d)")

(options, args) = parser.parse_args()

if len(args) != 1:
    parser.error("incorrect number of arguments")

# determine scoring matrix
scoring_matrix = None
needs_inversion = False

if options.substmat.lower() == "ednafull":
    scoring_matrix = FSTFactory.EDNAFULL
    needs_inversion = True
elif options.substmat.lower() == "dnaexp":
    scoring_matrix = DNAEXP
else:
    try:    
        scoring_matrix = getattr(MatrixInfo, options.substmat.lower())
        needs_inversion = True
    except AttributeError:
        parser.error("ERROR: Unknown substitution matrix.")

# modify substitution matrix
for key in scoring_matrix:
    scoring_matrix[key] *= options.mult

# determine gap costs
gap_open = options.gap_open
gap_extend = options.gap_extend

if options.choice == "d": ## edit distance
    # extract alphabet
    alphabet = set([k[0].lower() for k in scoring_matrix.keys()])
    fst = FSTFactory.create_edit_distance_fst(alphabet)
else:
    fst = FSTFactory.create_three_state_global_alignment_fst(scoring_matrix, gap_open, gap_extend, needs_inversion)

try:
    with open(args[0],"w") as fd:
        fd.write(str(fst))
except IOError:
    print "ERROR: Couldn't write FST file %s. Exiting." % sys.argv[4]
    sys.exit(1)

try:
    
    with open(args[0]+".symbols","w") as fd:
        fd.write(fst.create_symbol_table())
except IOError:
    print "ERROR: Couldn't write FST file. Exiting."
    sys.exit(1)

    