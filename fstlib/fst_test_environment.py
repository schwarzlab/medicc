#!/usr/bin/python

from fst  import *
from fst.factory import FSTFactory

from Bio.SubsMat import MatrixInfo

import fst.io
import fst.algos

import cProfile
import pstats

import sys


import Bio.Phylo
import cnvphylo
import cnvphylo.trees
from cnvphylo.trees import *

f1 = "/home/rfs/Documents/Projects/CRI/project-cnv-distance/resolve_CN_test/test3/tree_combined.xml"
f2 = "/home/rfs/Documents/Projects/CRI/project-cnv-distance/resolve_CN_test/test4/tree_combined.xml"

tree1 = Bio.Phylo.read(f1,"phyloxml")
tree2 = Bio.Phylo.read(f2,"phyloxml")

##ss1 = SplitSystemFactory.splitsystem_from_tree(tree1)
##ss2 = SplitSystemFactory.splitsystem_from_tree(tree2)

#print ss1
#print "---"
#print ss2
#print "---"

#table = SplitSystemTools.table([ss1, ss2])
#for t in table:
#    print "%s: %f" % (str(t), table[t])

#print "---"

#c= cnvphylo.trees.ConsensusTree([tree1, tree2])
#t = c.run()

#Bio.Phylo.draw_ascii(t)

#Bio.Phylo.write(t, "/tmp/test.xml", "phyloxml")



t1 = Bio.Phylo.PhyloXML.Phylogeny()
t1.root = Bio.Phylo.PhyloXML.Clade(name="root")
t1.root.clades.append(Bio.Phylo.PhyloXML.Clade(name="C"))

i3 = Bio.Phylo.PhyloXML.Clade(name="int3")
i3.clades.append(Bio.Phylo.PhyloXML.Clade(name="A"))
i3.clades.append(Bio.Phylo.PhyloXML.Clade(name="B"))
t1.root.clades.append(i3)

i1 = Bio.Phylo.PhyloXML.Clade(name="int2")
i1.clades.append(Bio.Phylo.PhyloXML.Clade(name="F"))
t1.root.clades.append(i1)

i2 = Bio.Phylo.PhyloXML.Clade(name="int2")
i2.clades.append(Bio.Phylo.PhyloXML.Clade(name="E"))
i2.clades.append(Bio.Phylo.PhyloXML.Clade(name="D"))
i1.clades.append(i2)

###

t2 = Bio.Phylo.PhyloXML.Phylogeny()
t2.root = Bio.Phylo.PhyloXML.Clade(name="root")
t2.root.clades.append(Bio.Phylo.PhyloXML.Clade(name="B"))

j3 = Bio.Phylo.PhyloXML.Clade(name="int3")
j3.clades.append(Bio.Phylo.PhyloXML.Clade(name="A"))
j3.clades.append(Bio.Phylo.PhyloXML.Clade(name="C"))
t2.root.clades.append(j3)

j1 = Bio.Phylo.PhyloXML.Clade(name="int2")
j1.clades.append(Bio.Phylo.PhyloXML.Clade(name="D"))
t2.root.clades.append(j1)

j2 = Bio.Phylo.PhyloXML.Clade(name="int2")
j2.clades.append(Bio.Phylo.PhyloXML.Clade(name="E"))
j2.clades.append(Bio.Phylo.PhyloXML.Clade(name="F"))
j1.clades.append(j2)


Bio.Phylo.draw_ascii(t1)
Bio.Phylo.draw_ascii(t2)



c= cnvphylo.trees.ConsensusTree([t1, t2])
t = c.run()

Bio.Phylo.draw_ascii(t)
Bio.Phylo.write(t, "/tmp/test.xml", "phyloxml")