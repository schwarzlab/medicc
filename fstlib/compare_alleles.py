#!/usr/bin/python

from Bio import SeqIO
import sys
import os
from fst.fst import *
from fst.factory import FSTFactory
from optparse import OptionParser
import subprocess
from subprocess import CalledProcessError
from subprocess import Popen
import numpy
##TMPPATH = "/tmp"

def main():
    usage = "USAGE: %prog <dataA> <dataB> <resultsA> <resultsB>"

    parser = OptionParser(usage)

    #parser.add_option("-r", "--real-semiring", action = "store_true", dest = "use_real_semiring", default = False,
    #                  help = "use real semiring (default log)?")

    #parser.add_option("-p", "--pseudo-counts", action = "store", type = "int", dest = "pseudo_counts", default = 0,
    #                  help = "number of pseudo counts to add (default 0)")

    ##parser.add_option("-u", "--union", action = "store", dest = "union", default = False,
    ##                  help = "combine all sequences into one unified FST with the given filename")

    (options, args) = parser.parse_args()

    if len(args) != 4:
        parser.error("incorrect number of arguments")
        sys.exit(1)
    
    for i in range(4):
        if not(os.path.exists(args[i])):
            print >> sys.stderr, "File %i does not exist!" %i
            sys.exit(1)

    compare_alleles(args[0], args[1], args[2], args[3])

def compare_alleles(trueA, trueB, estA, estB):
    with open(trueA, "rb") as fd:
        dataA = [record for record in SeqIO.parse(fd, "fasta")]
    with open(trueB, "rb") as fd:
        dataB = [record for record in SeqIO.parse(fd, "fasta")]
    with open(estA, "rb") as fd:
        resultA = [record for record in SeqIO.parse(fd, "fasta")]
    with open(estB, "rb") as fd:
        resultB = [record for record in SeqIO.parse(fd, "fasta")]

    scores = numpy.zeros(len(dataA))
    for i in range(len(dataA)):
        assert(dataA[i].name == dataB[i].name == resultA[i].name == resultB[i].name)
        inA = numpy.array(list(str(dataA[i].seq)))
        inB = numpy.array(list(str(dataB[i].seq)))
        outA = numpy.array(list(str(resultA[i].seq)))
        outB = numpy.array(list(str(resultB[i].seq)))

        aa=sum(inA == outA)
        ab=sum(inA == outB)
        ba=sum(inB == outA)
        bb=sum(inB == outB)
        total = len(inA)
        scores[i] = max(aa, ab)/float(total)

    print ", ".join([str(s) for s in scores])
    print "%.4f" % numpy.mean(scores)

if __name__ == '__main__':
    retval = main()
    sys.exit(retval)


