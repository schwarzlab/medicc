#!/usr/bin/python
""" Pipeline for comparing sequence comparison methods """

import sys
import subprocess
from subprocess import CalledProcessError
from subprocess import Popen
import os
import csv
import numpy
import numpy.linalg
from optparse import OptionParser

N_REPS          = 5 # number of sequence replicates
##STEPS = range(1, 11) # steps in the evolutionary distance to work on
STEPS = [7,]

EXE             = 0 # dummy constant, ignore
OUTPUT          = 1 # dummy constant, ignore 
MAKES_TREES     = 2 # dummy constant, ignore 

## method definitions: dictionary (hash for our perl fans) with names of the method
## as key, value is itself another dictionary with keys EXE and DICT.
## EXE contains the path to the executable
## DIST contains a tuple () of distance filenames created by the method
## All method binaries must expect exactly 2 arguments: sourcefile and outputdir
METHODS         = {##"muscle":{EXE:"./muscle.sh", OUTPUT:("muscle.dist",),MAKES_TREES:False},
##                   "muscle_boot":{EXE:"./muscle_boot.sh", OUTPUT:("muscle_boot.tre",),MAKES_TREES:True},
                   ##"fstscore":{EXE:"./transducer.py", OUTPUT:("kern.dist","kernnorm.dist"),MAKES_TREES:False},
                   "fstscore_mult":{EXE:"./transducer_mult.sh", OUTPUT:("kern.dist","kernnorm.dist"),MAKES_TREES:False},
                   ##"fstscore_simple":{EXE:"./transducer_simple.sh", OUTPUT:("kern.dist","kernnorm.dist"),MAKES_TREES:False},
                   ##"fstscore_simple2":{EXE:"./transducer_simple2.sh", OUTPUT:("kern.dist","kernnorm.dist"),MAKES_TREES:False},
                   ##"fstscore_trained":{EXE:"./transducer_trained.sh", OUTPUT:("kern.dist","kernnorm.dist"),MAKES_TREES:False},
##                   "fstscore_exp":{EXE:"./transducer_exp.sh", OUTPUT:("kern.dist","kernnorm.dist", "kernproj.dist"),MAKES_TREES:False},
                   "stretcher":{EXE: "./perform_alignment_stretcher.sh", OUTPUT:("kern.dist","kernnorm.dist","kernexp.dist"),MAKES_TREES:False},
##                   "needle":{EXE: "./perform_alignment_needle.sh", OUTPUT:("kern.dist","kernnorm.dist","kernexp.dist"),MAKES_TREES:False}
##                   "levenshtein":{EXE: "./levenshtein.sh", OUTPUT:("levenshtein.dist",),MAKES_TREES:False},
}

## path to binaries for tree computation and distance between trees
BIONJ_BIN       = "bionj"
QDIST_BIN       = "./qdistance.sh"
RFDIST_BIN      = "./rfdistance.sh"

## path to the file containing the true tree
TRUE_TREE_FILE  ="thetree.tre" ## relative to sourcedir

usage = "USAGE: %prog sourcedir outdir"
parser = OptionParser(usage=usage)

parser.add_option("-r","--use-rf-dist", action="store_true", dest="use_rf_dist", default=False,
                  help="use the Robinson Fould Distance instead of the Quartet distance")
parser.add_option("-b","--basename", action = "store", dest = "basename", default = "18tree_ind",
                  help = "Basename of the sequence files (filenames are: [basename][step]_[rep].[ext])")
parser.add_option("-x","--extension", action = "store", dest = "extension", default = "fas",
                  help = "Extension of the sequence files (filenames are: [basename][step]_[rep].[ext])")

(options, args) = parser.parse_args()

BASENAME        = options.basename
FILE_EXT        = options.extension

if len(args)!=2:
    parser.error("incorrect number of arguments")

source_dir = args[0]
output_dir = args[1]

if not(os.path.exists(source_dir)):
    print >> sys.stderr, "ERROR: source path does not exist"
    sys.exit(1)
    
if not(os.path.exists(output_dir)):
    os.mkdir(output_dir)
    
for method in METHODS.keys():      
    for rep in range(1,N_REPS+1):
        for step in STEPS:
            source_file = "%s%d_%d.%s" % (BASENAME, step, rep, FILE_EXT)
            target_dir = "%s%d_%d_%s" % (BASENAME, step, rep, method)
 
            ## call methods to compute distance matrices
            exe = METHODS[method][EXE]
            
            try:
                pass
                subprocess.check_call([exe, 
                                     "%s/%s" % (source_dir, source_file),
                                       "%s/%s" % (output_dir, target_dir),
                                       ])
            except CalledProcessError as e:
                sys.exit(e.returncode)

## compute trees from distance matrices if the MAKES_TREES flag is not set    
for method in METHODS.keys():
    for dist_file in METHODS[method][OUTPUT]:
        result_mat = numpy.zeros((N_REPS, len(STEPS)))      
        for rep in range(1,N_REPS+1):
            for step in STEPS:
                target_dir = "%s%d_%d_%s" % (BASENAME, step, rep, method)
                tree_file = "%s.tre" % dist_file

                distance = -1
                try:
                    if not(METHODS[method][MAKES_TREES]):
                        subprocess.check_call([BIONJ_BIN, 
                                               "%s/%s/%s" % (output_dir, target_dir, dist_file),
                                               "%s/%s/%s" % (output_dir, target_dir, tree_file)])
                    else:
                        tree_file = dist_file
                except CalledProcessError as e:
                    print >> sys.stderr, "Warning: couldn't compute tree for %s/%s/%s" % (output_dir, target_dir, tree_file)
                    pass
                else:   
                    ## compute distances to original tree
                    dist_bin = None
                    if options.use_rf_dist:
                        dist_bin = RFDIST_BIN
                    else:
                        dist_bin = QDIST_BIN
                        
                    p1 = subprocess.Popen([dist_bin, 
                                           "%s/%s" % (source_dir, TRUE_TREE_FILE),
                                           "%s/%s/%s" % (output_dir, target_dir, tree_file)], 
                                           stdout=subprocess.PIPE)
                    dist_result = p1.communicate()
                    if p1.returncode == 0:
                        distance = dist_result[0]
                    else:
                        print >> sys.stderr, "Warning: couldn't compute distance for tree %s/%s/%s" % (output_dir, target_dir, tree_file)
                result_mat[rep-1, STEPS.index(step)] = distance
                
        csv_file = "%s_%s_%s.csv" % (BASENAME, method, dist_file)
        try:
            with open("%s/%s" % (output_dir, csv_file),"w") as fd:
                writer = csv.writer(fd, delimiter='\t')
                for i in range(N_REPS):
                    row = result_mat[i,].tolist()                
                    writer.writerow(row)
        except IOError as e:
            sys.exit(e.returncode)
        
    
            