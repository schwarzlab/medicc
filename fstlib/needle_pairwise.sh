#!/bin/bash

FILENAME=$1
WORKDIR="./"

NUMOFSEQS=`grep -c "^>" $WORKDIR$FILENAME`

echo "Number of sequences: $NUMOFSEQS";

for i in `seq 1 "$NUMOFSEQS"`
do
    let "SEQTOWORK=(i-1)*2+1"

    # Das ist der Header der Sequenz. Ich will aber nur die
    # Sequenz... Dazu einfach noch eins addieren

    let "SEQTOWORK=SEQTOWORK+1"

    SEQ=`sed -n -e "$SEQTOWORK"p $WORKDIR$FILENAME`

    DATA=`needle -asequence ASIS:$SEQ -bsequence $WORKDIR$FILENAME -gapopen 10 -gapextend 0.5 \
          -datafile EDNAFULL -aformat3 markx3 -auto -stdout | \
          sed -n -e '/# Score: /{s/# Score: //g;p;}' | \
          tr "\n" "," | sed 's/,$//g'`

    echo "DATA=$DATA"
done

