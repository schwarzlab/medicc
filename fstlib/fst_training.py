#!/usr/bin/python
""" fst training script. expects names of alignment files used for training """


import os
from optparse import OptionParser
from Bio import SeqIO
from Bio import AlignIO
from Bio.SubsMat import MatrixInfo
import math
from numpy import *
import sys

from fst.fst import *
from fst.factory import FSTFactory
from fst.training import CountBasedTrainingStrategy

#from scipy.io import write_array
#from scipy.io import read_array

def determine_state_sequence(seq1, seq2, states):
    """ Determines the state sequence for an affine gap cost alignment transducer.
    The starting state is always assumed to be zero.The state sequence then contains
    the state the FST is in after (!) consuming the two symbols at the same position
    in the two sequences. """
    
    assert len(seq1) == len(seq2)
    sequence = []
    current_state = states[0]
    for i in range(0, len(seq1)):
        if seq1[i] == Fst.GAP and seq2[i] == Fst.GAP:
            current_state = None ## illegal state, should be caught in main routine
        elif seq1[i] == Fst.GAP:
            current_state = states[2] ## enter insert state
        elif seq2[i] == Fst.GAP:
            current_state = states[1] ## enter delete state
        else:
            current_state = states[0] ## stay in match state
        sequence.append(current_state)
    
    return sequence

PSEUDO_COUNT = 1
usage = "USAGE: %prog [filelist]"
parser = OptionParser(usage=usage)

format_help = """sequence or alignment format, see http://biopython.org/wiki/SeqIO 
                  or http://biopython.org/wiki/AlignIO for available formats"""
parser.add_option("-f", "--format", action="store", dest="format", default="fasta",
                  metavar="FORMAT", help=format_help)
parser.add_option("-p", "--protein-alignment", action="store_true", dest="is_protein", default=False,
                  help="alignment is a protein alignment (default nucleotides)")
parser.add_option("-n", "--no-normalize", action="store_false", dest="normalize", default = True,
                  help="alignment is a protein alignment (default nucleotides)")

(options, args) = parser.parse_args()

## setup alphabet and fst skeleton
alphabet = set()
myfst = None
if options.is_protein:
    alphabet = set([x[0].lower() for x in MatrixInfo.blosum62.keys()])
    myfst = FSTFactory.create_three_state_global_alignment_fst(MatrixInfo.blosum62, 0, 0, False)
else:
    alphabet = set(["a", "c", "g", "t"])
    myfst = FSTFactory.create_three_state_global_alignment_fst(FSTFactory.EDNAFULL, 0, 0, False)


alignments = []

if len(args) == 0:
    alignments.extend(AlignIO.parse(sys.stdin, options.format))
else:
    input_files = args
    ## try reading the files
    for file in input_files:
        with open(file,"r") as fd:
            alignments.extend(AlignIO.parse(fd, options.format))

## construct training data set
training_data = []        
for alignment in alignments:
    length = alignment.get_alignment_length()
    for i in range(0,len(alignment)):
        for j in range(0, len(alignment)):
            seq1 = alignment[i].seq
            seq2 = alignment[j].seq
            state_sequence = determine_state_sequence(seq1, seq2, myfst.states)
            training_data.append((str(seq1).lower(), str(seq2).lower(), state_sequence))

## train fst
myfst.train(CountBasedTrainingStrategy(normalize = options.normalize), training_data)

## output
print str(myfst)

