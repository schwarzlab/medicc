from __init__ import *
import collections
import logging
import subprocess
from subprocess import Popen
import shlex
import platform
from openfst import OpenFSTBase
import re


MM = collections.namedtuple("MM", ["ABSOLUTE","KATZ", "KNESER_NEY", "PRESMOOTHED", "UNSMOOTHED", "WITTEN_BELL"])
MAKE_METHOD = MM("absolute","katz","kneser_ney","presmoothed","unsmoothed","witten_bell")

PM = collections.namedtuple("PM", ["COUNT_PRUNE","RELATIVE_ENTROPY","SEYMORE"])
PRUNE_METHOD = PM("count_prune", "relative_entropy", "seymore")

class OpenGRM(OpenFSTBase):
    """ Wrapper around the opengrm-ngram library """


    def __init__(self):
        """ constructor """
        super(OpenGRM, self).__init__()


    def ngramsymbols(self, text, oov_symbol="[unk]", epsilon_symbol="[epsilon]"):
        """ Reads symbol table from a text corpus. 
        Each sentence in the text is on its own line, words are separated by spaces. """
        
        logging.info("Extracting symbol table...")
        p = Popen(shlex.split("ngramsymbols --OOV_symbol='%s' --epsilon_symbol='%s'" % (oov_symbol, epsilon_symbol), posix=not IS_WINDOWS), 
                  stdin=subprocess.PIPE, stdout = subprocess.PIPE, stderr = subprocess.PIPE, close_fds=platform.system()!="Windows", shell=platform.system()=="Windows")
        result = p.communicate(input=text)
        _cleanup(p)

        if p.returncode == 0:
            logging.info("done!")
            return result[0]
        else:
            logging.info("failed!")
            raise OpenGRMError(result[1])
    
    def ngramcount(self, fsa, order=3, epsilon_as_backoff=False):
        """ Counts ngrams in FST archive and returns an unnormalized language model """
        
        logging.info("Counting %d-grams..." % order)
        p = Popen(shlex.split("ngramcount --order=%d --epsilon_as_backoff=%d" % (order, int(epsilon_as_backoff)), posix=not IS_WINDOWS), 
                  stdin=subprocess.PIPE, stdout = subprocess.PIPE, stderr = subprocess.PIPE, close_fds=platform.system()!="Windows", shell=platform.system()=="Windows")
        result = p.communicate(input=fsa)
        _cleanup(p)

        if p.returncode == 0:
            logging.info("done!")
            return result[0]
        else:
            logging.info("failed!")
            raise OpenGRMError(result[1])

    def ngrammake(self, fst, backoff=False, backoff_label=0, bins=-1, check_consistency=False, discount_D=-1, method=MAKE_METHOD.WITTEN_BELL, witten_bell_k=1):
        """ Turns an unnormalized language model into a probabilistic, also applies smoothing if desired """
        
        if method not in MAKE_METHOD:
            method = MAKE_METHOD.WITTEN_BELL
            logging.warning("Unknown method, using '%s' instead." % method)

        logging.info("Normalizing and smoothing model...")
        p = Popen(shlex.split("ngrammake --backoff=%d --backoff_label=%d --bins=%d --check_consistency=%d --discount_D=%d --method=%s --witten_bell_k=%d" % 
                              (int(backoff), backoff_label, bins, int(check_consistency), discount_D, method, witten_bell_k), 
                              posix=not IS_WINDOWS), 
                  stdin=subprocess.PIPE, stdout = subprocess.PIPE, stderr = subprocess.PIPE, close_fds=platform.system()!="Windows", shell=platform.system()=="Windows")
        result = p.communicate(input=fst)
        _cleanup(p)

        if p.returncode == 0:
            logging.info("done!")
            return result[0]
        else:
            logging.info("failed!")
            raise OpenGRMError(result[1])

    def ngramrandgen(self, fsa, max_length=2147483647, max_sents=1, remove_epsilon=False, remove_total_weight=False, weighted=False):
        """ Generates random sentences from a language model """
        
        logging.info("Generating random sequences...")
        p = Popen(shlex.split("ngramrandgen --max_length=%d --max_sents=%d --remove_epsilon=%d --remove_total_weight=%d --weighted=%d" % 
                              (max_length, max_sents, int(remove_epsilon), int(remove_total_weight), int(weighted)), posix=not IS_WINDOWS), 
                  stdin=subprocess.PIPE, stdout = subprocess.PIPE, stderr = subprocess.PIPE, close_fds=platform.system()!="Windows", shell=platform.system()=="Windows")
        result = p.communicate(input=fsa)
        _cleanup(p)

        if p.returncode == 0:
            logging.info("done!")
            return result[0]
        else:
            logging.info("failed!")
            raise OpenGRMError(result[1])
         
    def ngramshrink(self, fsa, backoff_label=0, check_consistency=False, count_pattern="", method=PRUNE_METHOD.SEYMORE, shrink_opt=0, theta=0, total_unigram_count=-1):
        """ Prunes a ngram model """

        if method not in PRUNE_METHOD:
            method = PRUNE_METHOD.SEYMORE
            logging.warning("Unknown method, using '%s' instead." % method)
        
        logging.info("Pruning ngram model...")
        command = "ngramshrink --backoff_label=%d --check_consistency=%d --count_pattern='%s' --method='%s' --shrink_opt=%d --theta=%f --total_unigram_count=%d" % (backoff_label, int(check_consistency), count_pattern, method, shrink_opt, theta, total_unigram_count)
        p = Popen(shlex.split(command), 
                  stdin=subprocess.PIPE, stdout = subprocess.PIPE, stderr = subprocess.PIPE, close_fds=platform.system()!="Windows", shell=platform.system()=="Windows")
        result = p.communicate(input=fsa)
        _cleanup(p)

        if p.returncode == 0:
            logging.info("done!")
            return result[0]
        else:
            logging.info("failed!")
            raise OpenGRMError(result[1])

    def ngraminfo(self, model):
        """ Provides info about language model """
        
        logging.info("Getting info...")
        p = Popen("ngraminfo", stdin=subprocess.PIPE, stdout = subprocess.PIPE, stderr = subprocess.PIPE, close_fds=platform.system()!="Windows", shell=platform.system()=="Windows")
        result = p.communicate(input=model)
        _cleanup(p)

        if p.returncode == 0:
            logging.info("done!")
            return result[0]
        else:
            logging.info("failed!")
            raise OpenGRMError(result[1])

    def ngramprint(self, model, to_arpa=False, integers=False):
        """ Prints the language model """
        
        logging.info("Printing...")
        p = Popen("ngramprint" + (" --ARPA" if to_arpa else "") + (" --integers=1" if integers else ""), stdin=subprocess.PIPE, stdout = subprocess.PIPE, stderr = subprocess.PIPE, close_fds=platform.system()!="Windows", shell=platform.system()=="Windows")
        result = p.communicate(input=model)
        _cleanup(p)

        if p.returncode == 0:
            logging.info("done!")
            return result[0]
        else:
            logging.info("failed!")
            raise OpenGRMError(result[1])

    def ngramperplexity(self, model, textcorpus, return_raw=False):
        """ Computes the perplexity of the textcorpus against the model.
       The textcorpus must be a FAR. """
        
        logging.info("Perplexity...")
        p = Popen("ngramperplexity %s -" % self._input_filename(model), stdin=subprocess.PIPE, stdout = subprocess.PIPE, stderr = subprocess.PIPE, close_fds=platform.system()!="Windows", shell=platform.system()=="Windows")
        result = p.communicate(input=self._input_binary(textcorpus))
        _cleanup(p)

        if p.returncode == 0:
           logging.info("done!")
           if return_raw:
               return result[0]
           else:
            pattern = re.compile("(?P<sentences>\w+?)\s*sentences,\s*(?P<words>\w+?)\s*words,\s*(?P<oov>\w+?)\s*OOVs.*logprob\(base\s*10\)\=\s*(?P<logprob>.*?);\s*perplexity\s*=\s*(?P<perplexity>[+\-0-9.e]*)", re.DOTALL);
            match = pattern.match(result[0])

            PerplexityResult = collections.namedtuple("PerplexityResult", ("nsentences", "nwords", "oov", "logprob", "perplexity"))
            nsent = int(match.group("sentences"))
            nwords = int(match.group("words"))
            oov = int(match.group("oov"))
            logprob = float(match.group("logprob"))
            perplex = float(match.group("perplexity"))

            return PerplexityResult(nsent, nwords, oov, logprob, perplex)
        else:
            logging.info("failed!")
            raise OpenGRMError(result[1])


class OpenGRMError(Exception):
    def __init__(self, *args):
        return super(OpenGRMError, self).__init__(*args)


def _cleanup(p):
    if isinstance(p, subprocess.Popen):
        p = [p,]
    for q in p:
        if not q.stdin == None:
            q.stdin.close()
        if not q.stdout == None:
            q.stdout.close()
        if not q.stderr == None:
            q.stderr.close()


