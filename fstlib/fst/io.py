'''
Created on 17 May 2010

@author: rfs

Module for FST input and output from OpenFST text files

'''

from fst import *
import re


class AbstractFstReader(object):
    """ abstract superclass of all readers
    members:
        str filename
    """
    
    def __init__(self, fd):
        """ minimum constructor with fd """
        assert(fd != "" and fd != None)
        self.fd = fd
        self.initial_state = -1
        self.transitions = []
        self.final_states = {}
        
    def read(self):
        """ virtual read method """
        pass
    
    def create_fst(self):
        """ virtual method for creating the final fst """
        pass    
    
class OpenFstTextReader(AbstractFstReader):
    """ Parses a OpenFST text file """
    
    def __init__(self, fd):
        """ call superclass constructor """
        super(OpenFstTextReader, self).__init__(fd)
        self._state_map = {}
        self.read()
    
    def read(self):
        """ reads the textfile and extracts the states and transitions.
        Also remaps the state numbers. """

        for l in self.fd:
            
            line = l.strip("\n")
            line = re.split("\s", line)

            assert(len(line) > 0 and 
                   len(line) < 6 and 
                   len(line) != 3)

            if len(line) > 3:
                
                state_from = self._map_state_id( line[0] )
                if self.initial_state == -1: ## first line shows the start state
                    self.initial_state = state_from
                state_to = self._map_state_id( line[1] )
                symbol_from = line[2]
                symbol_to = line[3]
                weight = 0.0
                if len(line) == 5:
                    weight = float(line[4])
                    
                self.transitions.append((state_from, state_to, symbol_from, symbol_to, weight))
                
            if len(line) < 3:   ## final state line
                state = self._map_state_id( line[0] )
                weight = 0.0
                if len(line) == 2:
                    weight = float(line[1])
                self.final_states[state] = weight
                
    def create_fst(self):
        """ creates the fst itself """
        myfst = Fst()
        
        myfst.add_states(self.get_state_count())
        for state in myfst.states:
            try:
                state.weight = self.final_states[state.id]
                state.is_final = True
            except KeyError:
                state.is_final = False
            
        myfst.add_transitions(self.transitions)
        myfst._update_alphabet()
        
        return myfst
            
        
    def _map_state_id(self, sid):
        """ does the mapping from the ids in the text file to the internal ids """
        val = -1
        try:
            val = self._state_map[ int(sid) ]
        except KeyError:
            val = len(self._state_map)
            self._state_map[ int(sid) ] = val
            #print "new state mapped: %d -> %d" % ( int(sid), val)
        finally:
            return val
            
    def get_state_count(self):
        return len(self._state_map)
    

