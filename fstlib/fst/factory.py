'''
Created on 21 Jan 2010

@author: rfs
'''

from fst import *
import numpy
import collections

class FSTFactory:
	""" Creates special transducers for special occasions ;) """
	
	EDNAFULL = {('a','a'):5,
				('t','t'):5,
				('g','g'):5,
				('c','c'):5,
				('a','t'):-4,
				('a','g'):-4,
				('a','c'):-4,
				('c','a'):-4,
				('c','g'):-4,
				('c','t'):-4,
				('g','a'):-4,
				('g','c'):-4,
				('g','t'):-4,
				('t','a'):-4,
				('t','g'):-4,
				('t','c'):-4}

	@classmethod
	def _adjust_scoring_matrix(cls, scoring_matrix, invert = True):
		"""the BioPython scoring matrices are not mirrored, i.e. if A->T is defined
		T->A isn't. This function fixes that. It additionally also multiplies all scores
		by -1 if 'invert = True' and turns all into lower case letters."""
		#unique_keys = set(map(lambda x:tuple(sorted(x)),scoring_matrix.keys()))
		new_scoring_matrix={}
		
		for key in scoring_matrix.keys():
			new_key = tuple([k.lower() for k in key])
			new_scoring_matrix[new_key] = scoring_matrix[key] * (-1 if invert else 1)
			new_scoring_matrix[new_key[::-1]] = new_scoring_matrix[new_key]
	
		return new_scoring_matrix
	
	@classmethod
	def create_global_alignment_fst(cls, scoring_matrix, gap_open, gap_extend):
		"""creates a transducer for global alignment with
		specified gap open, extend and scoring costs"""
	
		scoring_matrix = FSTFactory._adjust_scoring_matrix(scoring_matrix)
		
		alphabet = set([i[0] for i in scoring_matrix.keys()])
		
		myfst = Fst()
		myfst.semiring = Fst.Semiring.LOG
		myfst.add_states(2)
		
		# starting state is match state
		myfst.add_transitions([(0, 0, s,t, scoring_matrix[(s,t)]) for s in alphabet for t in alphabet])
		
		# create gap open transitions to gap state
		myfst.add_transitions([(0, 1, s, Fst.GAP, gap_open) for s in alphabet])    
		myfst.add_transitions([(0, 1, Fst.GAP, s, gap_open) for s in alphabet]) 
		
		# create self transitions to gap state (gap extend)
		myfst.add_transitions([(1, 1, s, Fst.GAP, gap_extend) for s in alphabet])    
		myfst.add_transitions([(1, 1, Fst.GAP, s, gap_extend) for s in alphabet]) 
		
		# create match transitions back to match state
		myfst.add_transitions([(1, 0, s,t, scoring_matrix[(s,t)]) for s in alphabet for t in alphabet])

		return myfst

	@classmethod
	def create_three_state_global_alignment_fst(cls, scoring_matrix, gap_open, gap_extend, invert_matrix = False):
		"""creates a transducer for global alignment with
		specified gap open, extend and scoring costs. This FST 
		has 3 states and distinguishes between inserts and deletes """

		scoring_matrix = FSTFactory._adjust_scoring_matrix(scoring_matrix, invert_matrix)
		
		alphabet = set([i[0] for i in scoring_matrix.keys()])
		
		myfst = Fst()
		myfst.semiring = Fst.Semiring.LOG
		myfst.add_states(3)

		# starting state is match state
		# create match transitions to self
		myfst.add_transitions([(0, 0, s,t, scoring_matrix[(s,t)]) for s in alphabet for t in alphabet])        

		# create gap open transitions to gap state [delete]
		myfst.add_transitions([(0, 1, s, Fst.GAP, gap_open) for s in alphabet])        
		
		# create gap open transitions to gap state [insert]
		myfst.add_transitions([(0, 2, Fst.GAP, s, gap_open) for s in alphabet])
		
		# create self transitions to gap states (gap extend) [delete]
		myfst.add_transitions([(1, 1, s, Fst.GAP, gap_extend) for s in alphabet])        
		
		# create self transitions to gap states (gap extend) [insert]
		myfst.add_transitions([(2, 2, Fst.GAP, s, gap_extend) for s in alphabet])        

		# create match transitions back to match state
		myfst.add_transitions([(2, 0, s,t, scoring_matrix[(s,t)]) for s in alphabet for t in alphabet])
		myfst.add_transitions([(1, 0, s,t, scoring_matrix[(s,t)]) for s in alphabet for t in alphabet])
		
		# create delete to delete transitions
		myfst.add_transitions([(1, 2, Fst.GAP, s, gap_open) for s in alphabet])
		myfst.add_transitions([(2, 1, s, Fst.GAP, gap_open) for s in alphabet])
		
		return myfst

	@classmethod
	def create_three_state_semilocal_alignment_fst(cls, scoring_matrix, gap_open, gap_extend, invert_matrix = False):
		"""creates a transducer for semilocal alignment with
		specified gap open, extend and scoring costs. This FST 
		has 5 states and distinguishes between inserts and deletes.
		Semilocal here means the fst is assymettric, allowing for free insertions but
		not deletions on both ends of the string. """

		scoring_matrix = FSTFactory._adjust_scoring_matrix(scoring_matrix, invert_matrix)
		
		alphabet = set([i[0] for i in scoring_matrix.keys()])
		
		myfst = Fst()
		myfst.semiring = Fst.Semiring.LOG
		myfst.add_states(5, False)
		myfst.states[1].is_final=True
		myfst.states[4].is_final=True


		# starting state is free insert state
		myfst.add_transitions([(0, 0, Fst.GAP, s, 0) for s in alphabet])        
		myfst.add_transitions([(0, 1, s, t, scoring_matrix[(s,t)]) for s in alphabet for t in alphabet])        

		# create match transitions to self
		myfst.add_transitions([(1, 1, s,t, scoring_matrix[(s,t)]) for s in alphabet for t in alphabet])        

		# create gap open transitions to gap state [delete]
		myfst.add_transitions([(1, 2, s, Fst.GAP, gap_open) for s in alphabet])        
		
		# create gap open transitions to gap state [insert]
		myfst.add_transitions([(1, 3, Fst.GAP, s, gap_open) for s in alphabet])
		
		# create self transitions to gap states (gap extend) [delete]
		myfst.add_transitions([(2, 2, s, Fst.GAP, gap_extend) for s in alphabet])        
		
		# create self transitions to gap states (gap extend) [insert]
		myfst.add_transitions([(3, 3, Fst.GAP, s, gap_extend) for s in alphabet])        

		# create match transitions back to match state
		myfst.add_transitions([(3, 1, s,t, scoring_matrix[(s,t)]) for s in alphabet for t in alphabet])
		myfst.add_transitions([(2, 1, s,t, scoring_matrix[(s,t)]) for s in alphabet for t in alphabet])
		
		# create delete to delete transitions
		myfst.add_transitions([(2, 3, Fst.GAP, s, gap_open) for s in alphabet])
		myfst.add_transitions([(3, 2, s, Fst.GAP, gap_open) for s in alphabet])

		# end state is free insert state
		myfst.add_transitions([(4, 4, Fst.GAP, s, 0) for s in alphabet])        
		myfst.add_transitions([(1, 4, s, t, scoring_matrix[(s,t)]) for s in alphabet for t in alphabet])        
		
		return myfst
	
	@classmethod
	def create_gappy_bigram_fst(cls, alphabet, l=2):
		
		myfst = Fst()
		myfst.semiring = Fst.Semiring.LOG
		myfst.add_states(3)
		
		# state 0
		myfst.add_transitions([(0, 0, s, Fst.GAP, 0) for s in alphabet])
		myfst.add_transitions([(0, 1, s,t, 0) for s in alphabet for t in alphabet])
		
		# state 1
		myfst.add_transitions([(1, 1, s, Fst.GAP, -math.log(l)) for s in alphabet])
		myfst.add_transitions([(1, 2, s,t, 0) for s in alphabet for t in alphabet])
		
		# state 2
		myfst.add_transitions([(0, 0, s, Fst.GAP, 0) for s in alphabet])
		
		return myfst
	
	@classmethod
	def create_ngram_fst(cls, alphabet, n, gapSymbol="0"):
		myfst = Fst()
		myfst.semiring = Fst.Semiring.LOG
		myfst.add_states(n+1, is_final=False)
		myfst.states[n].is_final = True

		# state 0
		myfst.add_transitions([(0, 0, s, gapSymbol, 0) for s in alphabet])
		
		for i in range(n):
			myfst.add_transitions([(i, i+1, s,s, 0) for s in alphabet])

		# final state
		myfst.add_transitions([(n, n, s, gapSymbol, 0) for s in alphabet])

		return myfst


	@classmethod
	def create_edit_distance_fst(cls, alphabet):
		
		myfst = Fst()
		myfst.semiring = Fst.Semiring.LOG
		myfst.add_states(1)
		
		# state 0
		myfst.add_transitions([(0, 0, s,t, 1) for s in alphabet for t in alphabet if s != t])
		myfst.add_transitions([(0, 0, s,t, 0) for s in alphabet for t in alphabet if s == t])
		myfst.add_transitions([(0, 0, s, Fst.GAP, 1) for s in alphabet])
		myfst.add_transitions([(0, 0, Fst.GAP, s, 1) for s in alphabet])
		
		return myfst

	@classmethod
	def create_fixed_length_hmm_from_sequences(cls, seqs, alphabet, pseudo_counts = 1, semiring = Fst.Semiring.LOG, to_lower = False, count_matrix = None):
		""" Creates an HMM (a probabilistic FSA) from a list of sequences of identical length. 
		Pseudocounts are added to the individual positions if desired.
		
		seqs can be simple strings or BioPython Sequence objects. 
		
		If a count matrix is given (sequence length times alphabet size) it is filled with the counts. """

		max_length = max([len(x) for x in seqs])
		if count_matrix != None:
			if count_matrix.shape != (len(alphabet), max_length):
				raise FSTFactoryError("Count matrix of wrong size, is (%d,%d), should be (%d,%d)." %(count_matrix.shape[0], count_matrix.shape[1], len(alphabet), max_length))
		else:
			count_matrix = numpy.zeros((len(alphabet), max_length))

		count_matrix += pseudo_counts

		for seq in seqs:
			indices = numpy.array([alphabet.index(x) for x in seq])
			count_matrix[indices, numpy.array(range(len(seq)))] += 1

		myfst = cls.create_fixed_length_hmm_from_count_matrix(count_matrix, alphabet, semiring, to_lower)
		return(myfst)

	@classmethod
	def create_fixed_length_hmm_from_counts(cls, count_list, semiring = Fst.Semiring.LOG, to_lower = False):
		""" Creates an HMM (a probabilistic FSA) from a list of dictionaries of counts of letters 
		(a list of character-count mappings, one list entry for each position in the sequence) """ 

		myfst = Fst(is_acceptor = True)
		myfst.semiring = semiring
		curr_state = myfst.add_state(is_final = False)
		
		seq_length = len(count_list)
		
		for i in range(0, seq_length):
			next_state = myfst.add_state(is_final = False)
			counts = count_list[i]
			for s in counts:
				prob = float(counts[s]) / sum(counts.values())
				if prob > 0:
					k = - math.log(prob) if semiring == Fst.Semiring.LOG else prob
					transition = AcceptorTransition(curr_state, next_state, s.lower() if to_lower else s, k)
					myfst.add_transition(transition)
			curr_state = next_state
		curr_state.is_final = True
		
		return myfst
	
	@classmethod
	def create_fixed_length_hmm_from_count_matrix(cls, count_matrix, alphabet, semiring = Fst.Semiring.LOG, to_lower = False, normalize=True):
		""" Creates an HMM (a probabilistic FSA) from a matrix of counts """

		myfst = Fst(is_acceptor = True)
		myfst.semiring = semiring
		curr_state = myfst.add_state(is_final = False)
		
		alphabet_size, seq_length = count_matrix.shape
		count_sum = count_matrix.sum(0)
				
		for i in range(0, seq_length):
			next_state = myfst.add_state(is_final = False)
			which = numpy.nonzero(count_matrix[:,i]>0)[0]
			for j in which:
				s = alphabet[j]
				prob = count_matrix[j,i]
				if normalize:
					prob = float(prob) / count_sum[i]
				if prob > 0:
					prob = - math.log(prob) if semiring == Fst.Semiring.LOG else prob
					transition = AcceptorTransition(curr_state, next_state, s.lower() if to_lower else s, prob)
					myfst.add_transition(transition)
			curr_state = next_state
		curr_state.is_final = True
		
		return myfst

	@classmethod
	def create_fst_1step_amp(cls, alphabet, extend_cost=0, separator=None):
		myfst = Fst(alphabet)
		myfst.semiring = Fst.Semiring.TROPICAL ## should be tropical niy.
		myfst.add_states(2)
		myfst.states[0].is_final = True
		myfst.states[1].is_final = True
		
		myfst.add_transitions([(0, 0, s, s, 0) for s in alphabet]) ## match
		if separator is not None: myfst.add_transition((0, 0, separator, separator, 0)) ## separator

		myfst.add_transitions([(0, 1, s, t, 1) for s in alphabet for t in alphabet if cls._hexdist_signed(t, s) == 1 if s!='0']) ## event open transitions
		myfst.add_transitions([(1, 1, s, t, extend_cost) for s in alphabet for t in alphabet if cls._hexdist_signed(t, s) == 1 if s!='0']) ## event extend transitions
		#myfst.add_transition((1,1,'0','0', 0))

		myfst.add_transitions([(1, 0, s, s, 0) for s in alphabet]) ## match return
		if separator is not None: myfst.add_transition((1, 0, separator, separator, 0)) ## separator
				
		return myfst

	@classmethod
	def create_fst_1step_del(cls, alphabet, extend_cost=0, separator=None):
		myfst = Fst(alphabet)
		myfst.semiring = Fst.Semiring.TROPICAL ## should be tropical niy.
		myfst.add_states(2)
		myfst.states[0].is_final = True
		myfst.states[1].is_final = True
		
		myfst.add_transitions([(0, 0, s, s, 0) for s in alphabet]) ## match
		if separator is not None: myfst.add_transition((0, 0, separator, separator, 0)) ## separator

		myfst.add_transitions([(0, 1, s, t, 1) for s in alphabet for t in alphabet if cls._hexdist_signed(t, s) == -1]) ## event open transitions
		myfst.add_transitions([(1, 1, s, t, extend_cost) for s in alphabet for t in alphabet if cls._hexdist_signed(t, s) == -1]) ## event extend transitions
		#myfst.add_transition((1,1,'0','0', 0))

		myfst.add_transitions([(1, 0, s, s, 0) for s in alphabet]) ## match return
		if separator is not None: myfst.add_transition((1, 0, separator, separator, 0)) ## separator				

		return myfst
		
	@classmethod
	def create_cnv_fst_symmetric_no_lengthscoring(cls, alphabet):
		myfst = Fst()
		myfst.semiring = Fst.Semiring.LOG ## should be tropical niy.
		myfst.add_states(3)
		
		myfst.add_transitions([(0, 0, s, s, 0) for s in alphabet]) ## match transitions
		
		## del
		myfst.add_transitions([(0, 1, s, t, 1) for s in alphabet for t in alphabet if cls._hexdist_signed(s, t) == 1]) ## event open transitions
		myfst.add_transitions([(1, 1, s, t, 0) for s in alphabet for t in alphabet if cls._hexdist_signed(s, t) == 1]) ## event extend transitions
		myfst.add_transitions([(1, 0, s, s, 0) for s in alphabet]) ## match return transitions

		## amp
		myfst.add_transitions([(0, 2, s, t, 1) for s in alphabet for t in alphabet if cls._hexdist_signed(s, t) == -1]) ## event open transitions
		myfst.add_transitions([(2, 2, s, t, 0) for s in alphabet for t in alphabet if cls._hexdist_signed(s, t) == -1]) ## event extend transitions
		myfst.add_transitions([(2, 0, s, s, 0) for s in alphabet]) ## match return transitions
		
		## cross
		myfst.add_transitions([(1, 2, s, t, 1) for s in alphabet for t in alphabet if cls._hexdist_signed(s, t) == -1]) ## event open transitions
		myfst.add_transitions([(2, 1, s, t, 1) for s in alphabet for t in alphabet if cls._hexdist_signed(s, t) == 1]) ## event open transitions                
		
		return myfst

	@classmethod
	def create_edit_distance_cnv_fst_3state_test(cls, alphabet):
		myfst = Fst()
		myfst.semiring = Fst.Semiring.LOG ## should be tropical niy.
		myfst.add_states(3)
		
		myfst.add_transitions([(0, 0, s, s, -math.log(0.8)) for s in alphabet]) ## match transitions
		
		## del
		myfst.add_transitions([(0, 1, s, t, -math.log(0.15)) for s in alphabet for t in alphabet if cls._hexdist_signed(s, t) == 1]) ## event open transitions
		myfst.add_transitions([(1, 1, s, t, -math.log(0.8)) for s in alphabet for t in alphabet if cls._hexdist_signed(s, t) == 1]) ## event extend transitions
		myfst.add_transitions([(1, 0, s, s, -math.log(0.2)) for s in alphabet]) ## match return transitions

		## amp
		myfst.add_transitions([(0, 2, s, t, -math.log(0.35)) for s in alphabet for t in alphabet if cls._hexdist_signed(s, t) == -1]) ## event open transitions
		myfst.add_transitions([(2, 2, s, t, -math.log(0.8)) for s in alphabet for t in alphabet if cls._hexdist_signed(s, t) == -1]) ## event extend transitions
		myfst.add_transitions([(2, 0, s, s, -math.log(0.2)) for s in alphabet]) ## match return transitions
		
		## cross
		myfst.add_transitions([(1, 2, s, t, -math.log(0.35)) for s in alphabet for t in alphabet if cls._hexdist_signed(s, t) == -1]) ## event open transitions
		myfst.add_transitions([(2, 1, s, t, -math.log(0.15)) for s in alphabet for t in alphabet if cls._hexdist_signed(s, t) == 1]) ## event open transitions                
		
		return myfst

	@classmethod
	def create_edit_distance_cnv_fst_3state_test_x(cls, alphabet):
		myfst = Fst()
		myfst.semiring = Fst.Semiring.LOG ## should be tropical niy.
		myfst.add_states(3)
		
		myfst.add_transitions([(0, 0, s, s, -math.log(0.8)) for s in alphabet]) ## match transitions
		
		## del
		myfst.add_transitions([(0, 1, s, t, -math.log(0.15)) for s in alphabet for t in alphabet if cls._hexdist_signed(s, t) == 1]) ## event open transitions
		myfst.add_transitions([(1, 1, s, t, -math.log(0.8)) for s in alphabet for t in alphabet if cls._hexdist_signed(s, t) == 1]) ## event extend transitions
		myfst.add_transitions([(1, 0, s, s, -math.log(0.2)) for s in alphabet]) ## match return transitions

		## amp
		myfst.add_transitions([(0, 2, s, t, -math.log(0.35)) for s in alphabet for t in alphabet if cls._hexdist_signed(s, t) == -1]) ## event open transitions
		myfst.add_transitions([(2, 2, s, t, -math.log(0.8)) for s in alphabet for t in alphabet if cls._hexdist_signed(s, t) == -1]) ## event extend transitions
		myfst.add_transitions([(2, 0, s, s, -math.log(0.2)) for s in alphabet]) ## match return transitions
		
		## cross
		myfst.add_transitions([(1, 2, s, t, -math.log(0.35)) for s in alphabet for t in alphabet if cls._hexdist_signed(s, t) == -1]) ## event open transitions
		myfst.add_transitions([(2, 1, s, t, -math.log(0.15)) for s in alphabet for t in alphabet if cls._hexdist_signed(s, t) == 1]) ## event open transitions                
		
		return myfst

	@classmethod
	def create_cnv_fst_asymm_lengthscoring(cls, alphabet, separator=None, add_zeros=False):
		myfst = Fst()
		myfst.semiring = Fst.Semiring.LOG ## should be tropical niy.
		myfst.add_states(3)
		
		myfst.add_transitions([(0, 0, s, s, 0) for s in alphabet]) ## match transitions
		if separator != None: myfst.add_transition((0, 0, separator, separator, 0)) ## separator
		
		## deletions
		myfst.add_transitions([(0, 1, s, t, 1) for s in alphabet for t in alphabet if cls._hexdist_signed(s, t) == 1]) ## event open transitions
		myfst.add_transitions([(1, 1, s, t, 0.1) for s in alphabet for t in alphabet if cls._hexdist_signed(s, t) == 1]) ## event extend transitions
		if '0' in alphabet and add_zeros: ## duplicating '0' doesn't do anything but it is still possible!
			myfst.add_transition((1, 1, '0', '0', 0.1))
		
		myfst.add_transitions([(1, 0, s, s, 0) for s in alphabet]) ## match return transitions
		if separator != None: myfst.add_transition((1, 0, separator, separator, 0)) ## separator

		## amplifications
		myfst.add_transitions([(0, 2, s, t, 1) for s in alphabet for t in alphabet if cls._hexdist_signed(s, t) == -1  and s != '0']) ## event open transitions
		myfst.add_transitions([(2, 2, s, t, 0.1) for s in alphabet for t in alphabet if cls._hexdist_signed(s, t) == -1  and s != '0']) ## event extend transitions
		if '0' in alphabet and add_zeros: ## duplicating '0' doesn't do anything but it is still possible!
			myfst.add_transition((2, 2, '0', '0', 0.1))
		
		myfst.add_transitions([(2, 0, s, s, 0) for s in alphabet]) ## match return transitions
		if separator != None: myfst.add_transition((2, 0, separator, separator, 0)) ## separator

		## cross
		myfst.add_transitions([(1, 2, s, t, 1) for s in alphabet for t in alphabet if cls._hexdist_signed(s, t) == -1 and s!='0']) ## event open transitions
		myfst.add_transitions([(2, 1, s, t, 1) for s in alphabet for t in alphabet if cls._hexdist_signed(s, t) == 1]) ## event open transitions                
		
		return myfst

	@classmethod
	def create_cnv_fst_asymm_lengthscoring_full(cls, alphabet, separator=None):
		""" Creates an asymmetric edit-distance-like fst that scores lengths and has asymmetric weights for
		amplifications and deletions. The result is already complete and the fst does not need to be composed
		with itself. """

		## compute range/number of states
		nstates = 1 + ( (len(alphabet)-1) * 2 )
		del_open = 1.2
		del_xtend = 0.2
		amp_open = 1
		amp_xtend = 0.1
		
		myfst = Fst()
		myfst.semiring = Fst.Semiring.LOG ## should be tropical niy.
		myfst.add_states(nstates)
		
		myfst.add_transitions([(0, 0, s, s, 0) for s in alphabet]) ## match transitions
		if separator != None: myfst.add_transition((0, 0, separator, separator, 0)) ## separator
		
		for char_idx in range(1,len(alphabet)-1):
			#dist = cls._hexdist(alphabet[char_idx], alphabet[char_idx+1])
			del_state = char_idx * 2 ## even numbers are delete states
			amp_state = del_state -1 ## odd numbers are amp states
			for state_from in range(0, nstates):
				dist = cls._hexdist(alphabet[char_idx], alphabet[char_idx+1])
		
		## deletions
		myfst.add_transitions([(0, 1, s, t, 1) for s in alphabet for t in alphabet if cls._hexdist_signed(s, t) == 1]) ## event open transitions
		myfst.add_transitions([(1, 1, s, t, 0.1) for s in alphabet for t in alphabet if cls._hexdist_signed(s, t) == 1]) ## event extend transitions
		myfst.add_transitions([(1, 0, s, s, 0) for s in alphabet]) ## match return transitions
		if separator != None: myfst.add_transition((1, 0, separator, separator, 0)) ## separator

		## amplifications
		myfst.add_transitions([(0, 2, s, t, 1) for s in alphabet for t in alphabet if cls._hexdist_signed(s, t) == -1  and s != '0']) ## event open transitions
		myfst.add_transitions([(2, 2, s, t, 0.1) for s in alphabet for t in alphabet if cls._hexdist_signed(s, t) == -1  and s != '0']) ## event extend transitions
		myfst.add_transitions([(2, 0, s, s, 0) for s in alphabet]) ## match return transitions
		if separator != None: myfst.add_transition((2, 0, separator, separator, 0)) ## separator

		## cross
		myfst.add_transitions([(1, 2, s, t, 1) for s in alphabet for t in alphabet if cls._hexdist_signed(s, t) == -1 and s!='0']) ## event open transitions
		myfst.add_transitions([(2, 1, s, t, 1) for s in alphabet for t in alphabet if cls._hexdist_signed(s, t) == 1]) ## event open transitions                
		
		return myfst

	@classmethod
	def create_symbol_table(cls, alphabet):
		myfst = Fst(alphabet)
		return myfst.create_symbol_table()

	@classmethod
	def create_parentheses_tables(cls, nparens=4):
		nparens = max(4, nparens)
		## create basic (hardcoded) parentheses
		basic_parens = ["( 100",") 101","[ 102","] 103","{ 104","} 105","< 106","> 107"]

		parens = basic_parens[:]

		for i in range(nparens-4):
			idx = 108 + i*2
			parens.append("%s %d" % ("("+hex(i+2)[1:], idx))
			parens.append("%s %d" % (")"+hex(i+2)[1:], idx+1))

		pairs = ["%d %d" % (100 + i*2, 101 + i*2) for i in range(nparens)]

		return ("\n".join(parens) + "\n", "\n".join(pairs) + "\n")

	@classmethod
	def create_cnv_fst_asymm_no_lengthscoring(cls, alphabet, separator=None, add_zeros=False, deletions_only=False):
		myfst = Fst()
		myfst.semiring = Fst.Semiring.LOG ## should be tropical niy.
		myfst.add_states(2 if deletions_only else 3)
		
		myfst.add_transitions([(0, 0, s, s, 0) for s in alphabet]) ## match transitions
		if separator != None: myfst.add_transition((0, 0, separator, separator, 0)) ## separator
		
		## deletions
		myfst.add_transitions([(0, 1, s, t, 1) for s in alphabet for t in alphabet if cls._hexdist_signed(s, t) == 1]) ## event open transitions
		myfst.add_transitions([(1, 1, s, t, 0) for s in alphabet for t in alphabet if cls._hexdist_signed(s, t) == 1]) ## event extend transitions
		if '0' in alphabet and add_zeros: ## deleting '0' doesn't do anything but it is still possible!
			myfst.add_transition((1, 1, '0', '0', 0))
			pass

		#myfst.add_transitions([(1, 0, s, s, 0) for s in alphabet]) ## match return transitions
		myfst.add_transitions([(1, 0, s, s, 0) for s in alphabet if s != '0']) ## match return transitions
		## the transitions with 0:0 are I think not needed, as you can always stay in a delete/amp state and then use the cross
		## transitions below to get to the other side
		
		if separator != None: myfst.add_transition((1, 0, separator, separator, 0)) ## separator

		## amplifications
		if not deletions_only:
			myfst.add_transitions([(0, 2, s, t, 1) for s in alphabet for t in alphabet if cls._hexdist_signed(s, t) == -1  and s != '0']) ## event open transitions
			myfst.add_transitions([(2, 2, s, t, 0) for s in alphabet for t in alphabet if cls._hexdist_signed(s, t) == -1  and s != '0']) ## event extend transitions
			if '0' in alphabet and add_zeros: ## duplicating '0' doesn't do anything but it is still possible!
				myfst.add_transition((2, 2, '0', '0', 0))
			#    pass

			#myfst.add_transitions([(2, 0, s, s, 0) for s in alphabet]) ## match return transitions
			myfst.add_transitions([(2, 0, s, s, 0) for s in alphabet if s != '0']) ## match return transitions
		
			if separator != None: myfst.add_transition((2, 0, separator, separator, 0)) ## separator

			## cross
			myfst.add_transitions([(1, 2, s, t, 1) for s in alphabet for t in alphabet if cls._hexdist_signed(s, t) == -1  and s!='0']) ## event open transitions
			myfst.add_transitions([(2, 1, s, t, 1) for s in alphabet for t in alphabet if cls._hexdist_signed(s, t) == 1]) ## event open transitions                
		
		return myfst

	@classmethod
	def create_cnv_fst_asymm_loh_only(cls, alphabet, separator=None):
		myfst = Fst()
		myfst.semiring = Fst.Semiring.LOG ## should be tropical niy.
		myfst.add_states(2)
		
		if separator in alphabet:
			alphabet = [a for a in alphabet if a != separator]
			
		myfst.add_transitions([(0, 0, s, t, 0) for s in alphabet for t in alphabet if s!='0' and t!='0']) ## match transitions
		if separator != None: myfst.add_transition((0, 0, separator, separator, 0)) ## separator
		
		## LOH
		if '0' in alphabet and '1' in alphabet:
			myfst.add_transition((0, 1, '1', '0', 1)) ## LOH event open
			myfst.add_transition((1, 1, '1', '0', 0)) ## LOH event extend transitions

		## return
		myfst.add_transitions([(1, 0, s, t, 0) for s in alphabet for t in alphabet if s!='0' and t!='0']) ## match transitions
		
		return myfst

	@classmethod
	def create_cnv_fst_asymm_nls_duplications(cls, alphabet, separator=None, add_zeros=False):
		myfst = Fst()
		myfst.semiring = Fst.Semiring.LOG ## should be tropical niy.
		myfst.add_states(3)
		
		myfst.add_transitions([(0, 0, s, s, 0) for s in alphabet]) ## match transitions
		if separator != None: myfst.add_transition((0, 0, separator, separator, 0)) ## separator
		
		## deletions
		myfst.add_transitions([(0, 1, s, t, 1) for s in alphabet for t in alphabet if cls._hexdist_signed(s, t) == 1]) ## event open transitions
		myfst.add_transitions([(1, 1, s, t, 0) for s in alphabet for t in alphabet if cls._hexdist_signed(s, t) == 1]) ## event extend transitions
		if '0' in alphabet and add_zeros: ## deleting '0' doesn't do anything but it is still possible!
			myfst.add_transition((1, 1, '0', '0', 0))

		myfst.add_transitions([(1, 0, s, s, 0) for s in alphabet if s != '0']) ## match return transitions

		if separator != None: myfst.add_transition((1, 0, separator, separator, 0)) ## separator

		## duplications
		myfst.add_transitions([(0, 2, s, t, 1) for s in alphabet for t in alphabet if cls._hex_is_double(s, t) and cls._hexdist(s, t) != 1]) ## event open transitions
		myfst.add_transitions([(2, 2, s, t, 0) for s in alphabet for t in alphabet if cls._hex_is_double(s, t) and cls._hexdist(s, t) != 1]) ## event extend transitions
		if '0' in alphabet and add_zeros: ## deleting '0' doesn't do anything but it is still possible!
			myfst.add_transition((2, 2, '0', '0', 0))
		
		myfst.add_transitions([(2, 0, s, s, 0) for s in alphabet if s != '0']) ## match return transitions
		
		if separator != None: myfst.add_transition((2, 0, separator, separator, 0)) ## separator

		## cross
		myfst.add_transitions([(1, 2, s, t, 1) for s in alphabet for t in alphabet if cls._hex_is_double(s, t) and cls._hexdist(s, t) != 1]) ## event open transitions
		myfst.add_transitions([(2, 1, s, t, 1) for s in alphabet for t in alphabet if cls._hexdist_signed(s, t) == 1]) ## event open transitions                
		
		return myfst

	@classmethod
	def create_one_event_cnv_fst_4state(cls, alphabet):
		myfst = Fst()
		myfst.semiring = Fst.Semiring.LOG ## should be tropical niy.
		myfst.add_states(4)
		myfst.states[0].is_final = False
		myfst.states[1].is_final = True
		myfst.states[2].is_final = True
		myfst.states[3].is_final = True
		
		myfst.add_transitions([(0, 0, s, s, 0) for s in alphabet]) ## eat the start of the sequence (match)
		myfst.add_transitions([(0, 1, s, t, 0) for s in alphabet for t in alphabet if cls._hexdist_signed(s, t) == 1]) ## event open transitions
		myfst.add_transitions([(0, 2, s, t, 0) for s in alphabet for t in alphabet if cls._hexdist_signed(s, t) == -1]) ## event open transitions
		myfst.add_transitions([(1, 1, s, t, 0) for s in alphabet for t in alphabet if cls._hexdist_signed(s, t) == 1]) ## event extend transitions
		myfst.add_transitions([(2, 2, s, t, 0) for s in alphabet for t in alphabet if cls._hexdist_signed(s, t) == -1]) ## event extend transitions

		myfst.add_transitions([(1, 3, s, s, 0) for s in alphabet]) ## eat the end of the sequence (match)
		myfst.add_transitions([(2, 3, s, s, 0) for s in alphabet]) ## eat the end of the sequence (match)
		myfst.add_transitions([(3, 3, s, s, 0) for s in alphabet]) ## eat the end of the sequence (match)
				
		return myfst

	@classmethod
	def create_simple_cnv_fst(cls, alphabet):
		""" creates a one-step copy number variation FST for the cnv distance project  """

		PROB_AMP = 0.1
		PROB_DEL = 0.08
		PROB_AMPX = 0.8
		PROB_DELX = 0.8
		
		myfst = Fst()
		myfst.semiring = Fst.Semiring.LOG
		myfst.add_states(3)
		
		myfst.add_transitions([(0, 0, s, s, 0) for s in alphabet])
		## duplication states
		myfst.add_transitions([(0, 1, s, t, -math.log(PROB_AMP) * cls._hexdist(s, t)) 
							   for s in alphabet for t in alphabet if s < t and cls._hexdist(s, t) == 1])
		myfst.add_transitions([(1, 1, s, t, -math.log(PROB_AMPX) * cls._hexdist(s, t)) 
							   for s in alphabet for t in alphabet if s < t and cls._hexdist(s, t) == 1])

		myfst.add_transitions([(1, 0, s, s, 0) for s in alphabet])

		## deletion states
		myfst.add_transitions([(0, 2, s, t, -math.log(PROB_DEL) * cls._hexdist(s, t)) 
							   for s in alphabet for t in alphabet if s > t and cls._hexdist(s, t) == 1])
		myfst.add_transitions([(2, 2, s, t, -math.log(PROB_DELX) * cls._hexdist(s, t)) 
							   for s in alphabet for t in alphabet if s > t and cls._hexdist(s, t) == 1])

		myfst.add_transitions([(2, 0, s, s, 0) for s in alphabet])
		
		## now fill in missing probabilities
		for state in myfst.states:
			for a in alphabet:
				tweights = math.fsum([math.exp(-t.weight) for t in state.get_outgoing_transitions() if t.symbol_from == a]) - 1
				trans = [u for u in state.get_outgoing_transitions() if u.symbol_from == a and u.weight == 0]
				for t in trans:
					t.weight = -math.log( (1 - tweights) / len(trans) )

		return myfst
		
	@classmethod
	def _hexdist(cls, x, y):
		return abs(int(x, 16) - int(y, 16))

	@classmethod
	def _hexdist_signed(cls, x, y):
		return int(x, 16) - int(y, 16)
	
	@classmethod
	def _hex_is_double(cls, x, y):
		if x=="0" or y=="0": return(False)
		return (int(x, 16) / float(int(y, 16)) == 2)

	@classmethod
	def _hex_is_half(cls, x, y):
		if x=="0" or y=="0": return(False)
		return (int(x, 16) / float(int(y, 16)) == 0.5)

	@classmethod
	def _hex_division(cls, x, y):
		if x=="0" or y=="0": return(False)
		return (int(x, 16) / float(int(y, 16)))
	
	class FSTFactoryError(Exception):
		def __init__(self, msg):
			self.message = msg

		def __str__(self):
			return self.message