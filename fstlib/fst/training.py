'''
Created on 21 Jan 2010

@author: rfs
'''

from fst import *
import sys
 
class CountBasedTrainingStrategy(object):
    '''
    Training strategy for fsts using simple counts and given state sequences
    '''

    def __init__(self, pseudo_counts = 1, normalize = True):
        '''
        Constructor
        '''
        self.pseudo_counts = pseudo_counts
        self.normalize = normalize
        
    def _train(self, fst, data):
        """ Trains the fst (learns the parameters).
        
        parameters:
            Fst fst
            [(seq1, seq2, state_chain)]  data
        
        """
        
        ## count it
        transition_count = {}
        
        for seq1, seq2, state_chain in data:
            assert len(seq1) == len(seq2)
            length = len(seq1)
            start_state = fst.states[0]
            current_state = start_state
            for s in state_chain:
                if isinstance(s, int):
                    s = fst.states[s]
            
            for k in range(0, length):
                if seq1[k] == Fst.GAP and seq2[k] == Fst.GAP: ## ignore double gap columns
                    continue
                key = (current_state, state_chain[k], seq1[k], seq2[k])
                try:
                    current_value = transition_count[key]
                except KeyError:
                    current_value = 0
                finally:
                    transition_count[key] = current_value + 1
                    #print key, current_value
                current_state = state_chain[k]   
                            
        #for  a,b,c,d in transition_count.keys():
        #    print a.id, b.id, c, d, transition_count[(a,b,c,d)]

        ## modify the fst
        ## convert to REAL if necessary
        old_semiring = fst.semiring
        if fst.semiring != Fst.Semiring.REAL:
            FSTTools.convert_to_semiring(fst, Fst.Semiring.REAL)

        ## initialize transitions, i.e. set the weights to 0 + pseudo counts
        for t in fst.transitions:
            t.weight = 0 + self.pseudo_counts
         
        ## now add the counts
        for t in fst.transitions:
            value = transition_count.pop((t.state_from, t.state_to, t.symbol_from, t.symbol_to), 0)
            t.weight += value

        if len(transition_count) > 0:
            print >> sys.stderr, "---------------------------------------------------------------------------"
            print >> sys.stderr, "Warning: the following transitions were found in the training data set,"
            print >> sys.stderr, "but were not contained in the original fst:"
            for  a,b,c,d in transition_count.keys():
                print >> sys.stderr, a.id, b.id, c, d, transition_count[(a,b,c,d)]
            print >> sys.stderr, "----------------------------------------------------------------------------"
            
        ## normalize the transducer
        if self.normalize:
            FSTTools.normalize(fst)
        
        ## convert to log
        if fst.semiring != old_semiring:
            FSTTools.convert_to_semiring(fst, old_semiring)

        
        