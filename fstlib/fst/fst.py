""" Python class for finite-state transducers over the real or log semiring"""

import math
import sys

class AbstractTransition(object):
    """ Abstract class as base class for all types of transitions in a transducer.
    
    Member variables are:
        State state_from
        State state_to
        float weight
    """
    def __init__(self, state_from, state_to, weight):
        assert isinstance(state_from, State)
        assert isinstance(state_to, State)
        
        self.state_from = state_from
        self.state_to = state_to
        self.weight = weight

    def __str__(self):
        raise NotImplementedError("sub-classes must implement this method.")

    
class AcceptorTransition(AbstractTransition):
    """ Class to represent an acceptor transition in a transducer.
    
    Added member variables are:
        str symbol
    """
    
    def __init__(self, state_from, state_to, symbol, weight):
        super(AcceptorTransition, self).__init__(state_from, state_to, weight)
        self.symbol = symbol

    def __str__(self):
        return "%d %d %s %f" % (self.state_from.id, self.state_to.id, self.symbol, self.weight)

class Transition(AbstractTransition):
    """ Class to represent a full transition in a transducer.
    
    Additional member:
        str symbol_from
        str symbol_to
    """
    
    def __init__(self, state_from, state_to, symbol_from, symbol_to, weight):
        super(Transition, self).__init__(state_from, state_to, weight)
        
        self.symbol_from = symbol_from
        self.symbol_to = symbol_to

    def __str__(self):
        return "%d %d %s %s %f" % (self.state_from.id, self.state_to.id, self.symbol_from, self.symbol_to, self.weight)
    

class State(object):
    """ Class that represents a state in a transducer.
    
    Member variables:
        int id
        float weight
    
    Methods:
        [] get_transitions
        [] get_connected_states
    """
        
    def __init__(self, id, weight, is_final):
        self.id = id
        self.weight = weight
        self.fst = None
        self.is_final = is_final
        self._outgoing_transitions = None
        self._incoming_transitions = None
        
    def __str__(self):
        return "%d %f" % (self.id, self.weight)
    
    def _register_with_fst(self, fst):
        self.fst = fst
        
    def get_outgoing_transitions(self, force_rebuild = False):
        if self.fst == None:
            return []
        if self._outgoing_transitions == None or force_rebuild:
            self._outgoing_transitions = [trans for trans in self.fst.transitions if trans.state_from == self] 
        return self._outgoing_transitions
    
    def get_incoming_transitions(self, force_rebuild = False):
        if self.fst == None:
            return []
        if self._incoming_transitions == None or force_rebuild:
            self._incoming_transitions = [trans for trans in self.fst.transitions if trans.state_to == self] 
        return self._incoming_transitions
    
    def get_connected_states(self):
        if self.fst == None:
            return []
        return set([trans.state_to for trans in self.get_outgoing_transitions()])
        
class Fst(object):
    """ Class that describes a finite-state transducer.
    
    Member variables:
        [] transitions
        [] states
        set alphabet
    """
    class Semiring:
        REAL = 0
        LOG = 1
        TROPICAL = 1
    
    GAP = "-"
    def __init__(self, alphabet = None, states = None, transitions = None, semiring = Semiring.LOG, is_acceptor = False):
        self.transitions = transitions if not transitions == None else []
        self.states = states if not states == None else []
        self.alphabet = alphabet if not alphabet == None else set()
        self.semiring = semiring
        self.is_acceptor = is_acceptor
        
    def __str__(self):
        retstr = "\n".join([str(trans) for trans in self.transitions])
        retstr += "\n"
        retstr += "\n".join([str(state) for state in self.states if state.is_final])
        retstr += "\n"
        return retstr 

    def __len__(self):
        return len(self.transitions)
    
    ## state functions
    def add_state(self, weight = None, is_final = True):
        if weight == None:
            if self.semiring == Fst.Semiring.REAL:
                weight = 1
            else:
                weight = 0
        if self.semiring == Fst.Semiring.REAL:
            assert weight > 0
        state = State(len(self.states), weight, is_final)
        state._register_with_fst(self)
        self.states.append(state)
        return state

    def add_states(self, number, is_final=True):
        return [self.add_state(is_final=is_final) for i in range(0,number)]
            
    def add_states_with_weights(self, *weights):
        print "adding by weights"
        for w in weights:
            self.add_state(w)


    
    ## transition functions
    def add_transition(self, transition):
        """ Adds a transition to the transducer.
        
        A transition is either a 'Transition' object, or a 5-tuple
        (state_from, state_to, symbol_from, symbol_to, weight)
        from which a 'Transition' object is created."""
        
        if not self.is_acceptor:
            if isinstance(transition, Transition):
                transition = transition
            elif isinstance(transition, AcceptorTransition):
                raise TypeError("Wrong transition type, should be 'Transition'")
            else:
                (state_from, state_to, symbol_from, symbol_to, weight) = transition
                if isinstance(state_from, int):
                    state_from = self.states[state_from]
                if isinstance(state_to, int):
                    state_to = self.states[state_to]
                transition = Transition(state_from, state_to, symbol_from, symbol_to, weight)
        else:
            if isinstance(transition, AcceptorTransition):
                transition = transition 
            elif isinstance(transition, Transition):
                raise TypeError("Wrong transition type, should be 'AcceptorTransition'")
            else:
                (state_from, state_to, symbol, weight) = transition
                if isinstance(state_from, int):
                    state_from = self.states[state_from]
                if isinstance(state_to, int):
                    state_to = self.states[state_to]
                transition = AcceptorTransition(state_from, state_to, symbol, symbol_to, weight)

        if self.semiring == Fst.Semiring.REAL:
            assert transition.weight > 0    
        self.transitions.append(transition)
    
    def add_transitions(self, transitions):
        for t in transitions:
            self.add_transition(t)
    
    def clear_transitions(self, update_alphabet = False):
        """ Removes all transitions from the fst."""
        self.transitions = []
        if update_alphabet:
            self._update_alphabet()

    def set_initial_state(self, state):
        """ sets the initial state by moving one transition from that state as the 
        first transition in the fst """
        if isinstance(state, State):
            stateId = state.id
        else:
            stateId = state
        states_from = [t.state_from.id for t in self.transitions]
        try:
            i = states_from.index(stateId)
        except ValueError:
            print >> sys.stderr, "No transitions from state %d yet" % stateId
        else:
            transition = self.transitions.pop(i)
            self.transitions.insert(0, transition)
        
    def get_initial_state(self):
        """ returns the first state in the first transition in the fst """
        if len(self.transitions)==0:
            return -1
        else:
            return self.transitions[0].state_from

    ## other methods
    def _update_alphabet(self):
        for t in self.transitions:
            if t.symbol_from != Fst.GAP:
                self.alphabet.add(t.symbol_from)
            if t.symbol_to != Fst.GAP:    
                self.alphabet.add(t.symbol_to)
        
    def create_symbol_table(self):
        if len(self.alphabet) == 0:
            self._update_alphabet()
        
        alpha_sorted = list(self.alphabet)
        alpha_sorted.sort()

        symbol_table = ""
        i = 0
        symbol_table += "%s %d\n" % (Fst.GAP, 0)
        for s in alpha_sorted:
            i += 1
            symbol_table += "%s %d\n" % (s, i)

        return symbol_table
    
    def train(self, training_strategy, training_data):
        """ Estimates parameters/trains the fst from data."""
        training_strategy._train(self, training_data)
  
class Path(list):
    """ simple extension of list class that includes a final weight """
    __attributes = ["finalWeight"]

    def __init__(self, *args, **kwargs):
        super(Path, self).__init__(*args)
        self.finalWeight = kwargs.get("finalWeight", 0)
        valid = [p in self.__attributes for p in kwargs.keys()]
        if not all(valid):
            raise FSTError("Unknown attribute in [" + ",".join(kwargs.keys()) + "]. Allowed arguments are: [" + ",".join(self.__attributes) + "].")

class FSTTools:
    """Collection of fst tools."""
    
    @classmethod
    def map_weights_to_symbols(cls, fst, remove_weights = True, map_to_output_symbols = True):
        """ Modifies the FST such that the output symbols are replaced by the weights. 
        Only works with integer weights """
        for trans in fst.transitions:
            if map_to_output_symbols:
                trans.symbol_to = "%d" % trans.weight
            else:
                trans.symbol_from = "%d" % trans.weight
            if remove_weights:
                trans.weight = 0
        
    @classmethod
    def normalize(cls, transducer, include_state_weights = False):
        """Normalizes fst so that outgoing transition weights + the state weight sum to 1.
        The state weight is only taken into account when the appropriate flag is set and
        the state is a final state. """
        
        semiring = transducer.semiring
        
        ## convert to real if necessary
        cls.convert_to_semiring(transducer, Fst.Semiring.REAL)
        
        for state in transducer.states:
            weight_sum = math.fsum([t.weight for t in state.get_outgoing_transitions()]) 
            if include_state_weights and state.is_final:
                weight_sum += state.weight
            if weight_sum > 0:
                for t in state.get_outgoing_transitions():
                    t.weight = t.weight / weight_sum
                if include_state_weights and state.is_final:
                    state.weight = state.weight / weight_sum
            else:
                if not state.is_final:
                    print >> sys.stderr, "WARNING: Non-final state %s has total weightsum of zero!" % state
        
        ## convert back
        cls.convert_to_semiring(transducer, semiring)

    @classmethod
    def normalize_alphabet(cls, transducer, verbose = False):
        """Normalizes fst so that outgoing transition weights of the same input symbol sum to 1"""
        
        semiring = transducer.semiring
        
        ## convert to real if necessary
        cls.convert_to_semiring(transducer, Fst.Semiring.REAL)
        
        for state in transducer.states:
            for a in transducer.alphabet:
                weights = [t.weight for t in state.get_outgoing_transitions() if t.symbol_from == a]
                weight_sum = math.fsum(weights)
                if verbose:
                    if weight_sum != 0:
                        print >> sys.stderr, "label %s at state %s has sum %f (%f)" % (a, state, -math.log(weight_sum), weight_sum)
                    else: 
                        print >> sys.stderr, "label %s at state %s has sum Infinity (0)" % (a, state)
                if weight_sum > 0:
                    for t in [u for u in state.get_outgoing_transitions() if u.symbol_from == a]:
                        t.weight = t.weight / weight_sum
                else:
                    if not state.is_final:
                        print >> sys.stderr, "WARNING: Non-final state %s with symbol %s has total weightsum of zero!" % (state, a)
        
        ## convert back
        cls.convert_to_semiring(transducer, semiring)
    
    @classmethod
    def convert_to_semiring(cls, transducer, semiring):
        if semiring == Fst.Semiring.LOG:
            cls._convert_to_log(transducer)
        elif semiring == Fst.Semiring.REAL:
            cls._convert_to_real(transducer)
        else:
            raise NotImplementedError("unknown semiring %s" % str(semiring))
        
    @classmethod
    def _convert_to_log(cls, transducer):
        if transducer.semiring == Fst.Semiring.LOG:
            return
        
        for trans in transducer.transitions:
            trans.weight = -math.log(trans.weight)
            
        for state in [s for s in transducer.states if s.is_final == True]:
            state.weight = -math.log(state.weight)
            
        transducer.semiring = Fst.Semiring.LOG
        
    @classmethod
    def _convert_to_real(cls, transducer):
        if transducer.semiring == Fst.Semiring.REAL:
            return
        
        for trans in transducer.transitions:
            trans.weight = math.exp(-trans.weight)
            
        for state in [s for s in transducer.states if s.is_final == True]:
            state.weight = math.exp(-state.weight)
            
        transducer.semiring = Fst.Semiring.REAL
    
    @classmethod
    def multiply_weights(cls, fst, weight):
        """ multiplies all weights by a factor """
        for t in fst.transitions:
            t.weight *= weight
            
        for s in fst.states:
            if s.is_final:
                s.weight *= weight
        
    @classmethod
    def add_to_weights(cls, fst, weight):
        """ adds a constant to all weights """
        for t in fst.transitions:
            t.weight += weight

        for s in fst.states:
            if s.is_final:
                s.weight += weight

class FSTError(Exception):
    def __init__(self, message="<unknown error>"):
        self.message = message

    def __str__(self):
        return str(self.message)
          