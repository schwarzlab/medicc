#!/usr/bin/env python
import logging
from openfst import OpenFST
from openfst import OpenFSTUtilities
from fst import *
from factory import FSTFactory
from io import OpenFstTextReader
import os
import tempfile


class JointShortestPath(object):
    """Computes the joint shortest path between n fsts via a common reference point (LCA).
    
    Constructor arguments:
    fst: the model fst 
    fsas: list of acceptors between which to compute the shortest path"""
    
    def __init__(self, fst, fsas, openfst):
        self.fst = fst          ## the model fst, symmetric or in the direction towards the ancestor
        self.fsas = fsas        ## the input fsas between which to compute the shortest path
        self.final_fsas = []    ## the output fsas
        self.lca = None         ## the output lca
        self.openfst = openfst
        self.run()

    def run(self):
        """ main execution method """
        initial_potentials = [ self.openfst.bcompose(f, self.fst) for f in self.fsas ]

        msgs_fwd = []
        ## forward
        for i in range(0, len(self.fsas)-1):
            ## marginalise
            marg = self.openfst.bproject(initial_potentials[i], True)
            marg = self.openfst.bdetmin(marg, False, True, False)
            if i>0:
                marg = self.openfst.barcsort(marg, "ilabel")
                msg = self.openfst.bintersect(msgs_fwd[i-1], marg, "match")
                msg = self.openfst.bdeterminize(msg)
                msg = self.openfst.brmepsilon(msg)
            else:
                msg = marg

            msgs_fwd.append(msg)

        ## backward
        msgs_bwd = [None] * len(msgs_fwd)
        for i in range(len(self.fsas)-1, 0, -1):
            ## marginalise
            marg = self.openfst.bproject(initial_potentials[i], True)
            marg = self.openfst.bdetmin(marg, False, True, False)
            if i < len(self.fsas)-1:
                marg = self.openfst.barcsort(marg, "ilabel")
                msg = self.openfst.bintersect(msgs_bwd[i], marg, "match")
                msg = self.openfst.bdeterminize(msg)
                msg = self.openfst.brmepsilon(msg)
            else:
                msg = marg

            msgs_bwd[i-1] = msg

        ## compute beliefs
        beliefs = [None] * len(self.fsas)
        for i in range(0, len(self.fsas)):
            if i==0:
                belief = msgs_bwd[i]
            elif i==len(self.fsas)-1:
                belief = msgs_fwd[i-1]
            else:
                belief = self.openfst.bintersect(self.openfst.barcsort(msgs_fwd[i-1]) , msgs_bwd[i], "match")
               
            belief = self.openfst.bcompose(self.openfst.barcsort(initial_potentials[i]), belief)
            beliefs[i] = belief

        ## compute final sequences
        self.lca = self.openfst.bdetmin(self.openfst.bshortestpath(self.openfst.bproject(self.openfst.bmap(beliefs[0], "to_standard"), True)), False, True, False)
        self.final_fsas = [ self.openfst.bdetmin(self.openfst.bshortestpath(self.openfst.bproject(self.openfst.bmap(b, "to_standard"), False)), False, True, False) for b in beliefs ]

    def compute_scores_to_lca(self):
        """ computes the alignment score from each final fsa to the lca """
        lca = self.openfst.bmap(self.openfst.bmap(self.lca, "rmweight"), "to_standard")
        model = self.openfst.bmap(self.fst, "to_standard")
        scores = [ self.openfst.align(model, self.openfst.bmap(self.openfst.bmap(f, "rmweight"), "to_standard"), lca) for f in self.final_fsas ]
        return scores

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    logging.info("test")

    alphabet = ["a", "c", "t", "g"]
    myfst = Fst(alphabet)
    match_state = myfst.add_state(0, True)
    [ myfst.add_transition(Transition(match_state, match_state, s, t, -2.5)) for s in alphabet for t in alphabet if s==t]
    [ myfst.add_transition(Transition(match_state, match_state, s, t, 6.5)) for s in alphabet for t in alphabet if s!=t]
    indel_state = myfst.add_state(0, True)
    [ myfst.add_transition(Transition(match_state, indel_state, s, "-", 16)) for s in alphabet ]
    [ myfst.add_transition(Transition(indel_state, indel_state, s, "-", 4)) for s in alphabet ]
    [ myfst.add_transition(Transition(indel_state, match_state, s, t, -2.5)) for s in alphabet for t in alphabet if s==t]
    [ myfst.add_transition(Transition(indel_state, match_state, s, t, 6.5)) for s in alphabet for t in alphabet if s!=t]


    seq1 = FSTFactory.create_fixed_length_hmm_from_sequences(["ata"], alphabet, 0) ## 10.5
    seq2 = FSTFactory.create_fixed_length_hmm_from_sequences(["aac"], alphabet, 0) ## 10.5
    seq3 = FSTFactory.create_fixed_length_hmm_from_sequences(["atc"], alphabet, 0) ## 1.5
    seq4 = FSTFactory.create_fixed_length_hmm_from_sequences(["taa"], alphabet, 0) ## 10.5
    seq5 = FSTFactory.create_fixed_length_hmm_from_sequences(["ttc"], alphabet, 0) ## -7.5
    seq6 = FSTFactory.create_fixed_length_hmm_from_sequences(["tac"], alphabet, 0) ## 1.5
    seq7 = FSTFactory.create_fixed_length_hmm_from_sequences(["ttc"], alphabet, 0) ## -7.5
    
    symbol_table = myfst.create_symbol_table()
    symbol_file = tempfile.gettempdir() + "/symbols.txt"
    with open(symbol_file, "wb") as fd:
        fd.write(symbol_table)

    openfst = OpenFST(alphabet, symbol_file)

    myfst = openfst.bcompile(myfst, False, "log")
    fsa1 = openfst.bcompile(seq1, True, "log")
    fsa2 = openfst.bcompile(seq2, True, "log")
    fsa3 = openfst.bcompile(seq3, True, "log")
    fsa4 = openfst.bcompile(seq4, True, "log")
    fsa5 = openfst.bcompile(seq5, True, "log")
    fsa6 = openfst.bcompile(seq6, True, "log")
    fsa7 = openfst.bcompile(seq7, True, "log")

    algo = JointShortestPath(myfst, [fsa1, fsa2, fsa3, fsa4, fsa5, fsa6, fsa7], openfst )
    scores = algo.compute_scores_to_lca()

    print(OpenFSTUtilities.openfst_to_string(openfst.bprint(algo.lca)))
    for i in range(0, len(algo.final_fsas)):
        print(OpenFSTUtilities.openfst_to_string(openfst.bprint(algo.final_fsas[i])))




        






