#!ENV python

""" converts text fsts from log to real and vice versa (i.e. applies -log(x) and exp(-x) functions) """
import os
import sys
from optparse import OptionParser
from collections import defaultdict
import fst
from fst.fst import *
from fst import io

usage = "USAGE: %prog <input_file>"
parser = OptionParser(usage=usage)

(options, args) = parser.parse_args()

if len(args) > 1:
	parser.error("incorrect number of arguments")

fd_in = None
if len(args) == 1:
	try:
		fd_in = open(args[0], "r")
	except IOError as e:
		print "Error opening input file: %s" % e.strerror
else:    
	fd_in = sys.stdin

fd_out = sys.stdout
############################################

	
reader = io.OpenFstTextReader(fd_in)
reader.read()
f = reader.create_fst()

for state in f.states:
	f.add_transition((state, state, '0','0', 0))

fd_out.write(str(f))
