#!/usr/bin/python

""" converts text fsts from log to real and vice versa (i.e. applies -log(x) and exp(-x) functions) """
import os
import sys
from optparse import OptionParser
from collections import defaultdict
import fst
from fst.fst import *
from fst import io

usage = "USAGE: %prog <input_file>"
parser = OptionParser(usage=usage)

parser.add_option("-f", "--from-log", action = "store_true", dest = "from_log", default = False, 
                  help = "Convert from log semiring (false)")
parser.add_option("-t", "--to-log", action = "store_true", dest = "to_log", default = False, 
                  help = "Convert to log semiring (false)")
parser.add_option("-n", "--normalize-transitions", action = "store_true", dest = "normalize_transitions", default = False,
                  help = "Normalize transitions after conversion?")
parser.add_option("-m", "--normalize-full", action = "store_true", dest = "normalize_full", default = False,
                  help = "Normalize transitions and final states after conversion (this implies -n)?")
parser.add_option("-a", "--normalize-alphabet", action = "store_true", dest = "normalize_alphabet", default = False,
                  help = "Normalize such that all outgoing transitions with the same input symbol sum to 1")
parser.add_option("-v", "--verbose", action = "store_true", dest = "verbose", default = False,
                  help = "be verbose")
parser.add_option("-k", "--multiply-weights", action = "store", dest = "weight_factor", default = 1.0, type="float", 
                  help = "multiply weights by factor")
parser.add_option("-l", "--add-to-weights", action = "store", dest = "weight_summand", default = 0.0, type="float",
                  help = "add constant to weights")



(options, args) = parser.parse_args()

if len(args) > 1:
    parser.error("incorrect number of arguments")

fd_in = None
if len(args) == 1:
    try:
        fd_in = open(args[0], "r")
    except IOError as e:
        print "Error opening input file: %s" % e.strerror
else:    
    fd_in = sys.stdin

fd_out = sys.stdout
############################################

    
reader = io.OpenFstTextReader(fd_in)
reader.read()
f = reader.create_fst()


if options.from_log:
    f.semiring = Fst.Semiring.LOG
else:
    f.semiring = Fst.Semiring.REAL

if options.to_log:
    FSTTools.convert_to_semiring(f, Fst.Semiring.LOG)
else:
    FSTTools.convert_to_semiring(f, Fst.Semiring.REAL)

if options.normalize_transitions or options.normalize_full:
    FSTTools.normalize(f, include_state_weights = options.normalize_full)
elif options.normalize_alphabet:
    FSTTools.normalize_alphabet(f, verbose = options.verbose)
    
FSTTools.add_to_weights(f, options.weight_summand)
FSTTools.multiply_weights(f, options.weight_factor)
 
fd_out.write(str(f))
