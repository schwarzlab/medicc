#!/bin/bash

fstencode --encode_labels $1 tmpcodex | fstdeterminize | fstminimize | fstencode --decode - tmpcodex

rm tmpcodex

