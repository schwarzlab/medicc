#!/usr/bin/python

""" maps the weights of a transducer to the output symbols, currently only works with integer weights """

import os
import sys
from optparse import OptionParser
from collections import defaultdict
import fst
from fst.fst import *
from fst import io

usage = "USAGE: %prog [<input_file>] [<output_file>]"
parser = OptionParser(usage=usage)

parser.add_option("-v", "--verbose", action = "store_true", dest = "verbose", default = False,
                  help = "be verbose")
parser.add_option("-k", "--keep-weights", action = "store_true", dest = "keep_weights", default = False,
                  help = "Keep weights")


(options, args) = parser.parse_args()

if len(args) > 2:
    parser.error("Too many arguments.")

fd_in = sys.stdin
fd_out = sys.stdout

if len(args) > 0:
    try:
        fd_in = open(args[0], "r")
    except IOError as e:
        print "Error opening input file: %s" % e.strerror
    if len(args) > 1:
        try:
            fd_out = open(args[1], "w")
        except IOError as e:
            print "Error opening output file: %s" % e.strerror

############################################

    
reader = io.OpenFstTextReader(fd_in)
reader.read()
fst = reader.create_fst()

FSTTools.map_weights_to_symbols(fst, remove_weights = not options.keep_weights)

fd_out.write(str(fst))

