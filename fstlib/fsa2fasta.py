#!env python

from fst  import *
import fst.openfst
from fst.factory import FSTFactory
from optparse import OptionParser
from Bio.SubsMat import MatrixInfo

import fst.io
import fst.algos

import cProfile
import pstats

import sys

usage = "USAGE: %prog <fst_file>"

parser = OptionParser(usage)

parser.add_option("-s", "--separate", action="store_true", dest = "separate", default = False, help = "name each path individually (default false)")
parser.add_option("-n", "--name", action = "store", dest = "name", default = "", 
                  help = "name for the sequence(s)")
parser.add_option("-t", "--split-char", action = "store", dest = "split_char", default = "", 
                  help = "character between tokens")
parser.add_option("-o", "--omit-header", action = "store_true", dest = "omit_header", default = False, 
                  help = "omit FASTA header (print sequences only, ignores 'name' option)")


(options, args) = parser.parse_args()

if len(args) == 0:
    infile = sys.stdin
else:
    infile = open(args[0],"r")
    
textfst = infile.read()

if options.name == "":
    if len(args)==0:
        name = "unknown_sequence"        
    else:
        name = args[0]
else:
    name = options.name

if not options.omit_header:
    print fst.openfst.OpenFSTUtilities.openfst_to_fasta(textfst, name, options.separate, options.split_char)
else:
    print fst.openfst.OpenFSTUtilities.openfst_to_string(textfst, options.separate, options.split_char)

infile.close()
    

