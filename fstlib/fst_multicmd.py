#!ENV python
""" performs a binary fst command on multiple files sequentially """

from optparse import OptionParser
import subprocess
from subprocess import CalledProcessError
from subprocess import Popen
import os
import sys
import shutil
import shlex
import tempfile

#TMPDIR = "/tmp"
TMPDIR = tempfile.gettempdir()
IS_WINDOWS = os.name=="nt"
usage = "USAGE: %prog <file1> [<file2>] [<file3>]... [<fileN>]"

parser = OptionParser(usage)

##parser.add_option("-r", "--real-semiring", action = "store_true", dest = "use_real_semiring", default = False,
##                 help = "use real semiring (default log)?")

parser.add_option("-c", "--command", action = "store", dest = "command", default = "fstunion",
                  help = "command to execute")

parser.add_option("-d", "--delete", action = "store_true", dest = "delete", default = False,
                  help = "delete the individual files")

##parser.add_option("-s", "--cluster-size", action = "store", dest = "cluster_size", default = 1,
##                  help = "apply method to <clustersize> files together (before any eventual pruning step)")

parser.add_option("-p", "--prune-weight", action = "store", type = "float", dest = "prune_weight", default = -1,
                  help = "prune the intermediate steps with the given weight factor")
parser.add_option("-q", "--prune-states", action = "store", type = "int", dest = "prune_states", default = -1,
                  help = "prune the intermediate steps with the given state factor")
parser.add_option("-a", "--alphabet-normalize", action = "store", dest = "alphanorm", default = "",
                  help = "alphabet normalize after each pruning step, symbol file needed")
parser.add_option("-m", "--minimize", action = "store_true", dest = "minimize", default = False,
                  help = "determinize and minimize result after each step, might not terminate")
parser.add_option("-s", "--sort", action = "store_true", dest = "sort", default = False,
                  help = "arcsort (output) result after each step")
parser.add_option("-r", "--repeat", action = "store", type = "int", dest = "repeat", default = 1,
                  help = "repeat the given operation on the first file n times; ignored if more than one file given")
parser.add_option("-k", "--keep", action = "store_true", dest = "keep", default = False,
                  help = "keep the intermediate results at the end of each iteration/step (will be dumped in current directory)?")
parser.add_option("-x", "--determinize-delta", action = "store", type = "float", dest = "delta_det", default = -1,
                  help = "delta for determinization")
parser.add_option("-y", "--minimize-delta", action = "store", type = "float", dest = "delta_min", default = -1,
                  help = "delta for minimization")


(options, args) = parser.parse_args()

if len(args) < 1:
    parser.error("at least one FST needed")
    sys.exit(1)

if len(args) == 1 and options.repeat == 1:
    subprocess.call(["cat", args[0]])
    sys.exit(0)
elif len(args) == 1 and options.repeat > 1:
    for i in range(1,options.repeat):
        args.append(args[0])

#print >> sys.stderr, "Temp dir: " + TMPDIR

## sort
if options.sort:
    try:
        subprocess.check_call(["fstarcsort",
                               "--sort_type=olabel",
                               "%s" % args[0],
                               "%s/sorted.bin" % TMPDIR ])
    except CalledProcessError as e:
        sys.exit(e.returncode)
    
## first command
try:
    subprocess.check_call([options.command,
                            "%s" % args[0] if not options.sort else "%s/sorted.bin" % TMPDIR,
                            "%s" % args[1],
                            "%s/cmd1.bin" % TMPDIR])
except CalledProcessError as e:
    sys.exit(e.returncode)

## minimize
if options.minimize:
    try:
        delta_param_det = " --delta=%f" % options.delta_det
        delta_param_min = " --delta=%f" % options.delta_min
        p=[]
        p.append(Popen(shlex.split("fstencode --encode_labels %s/cmd1.bin codex" % TMPDIR, posix=not IS_WINDOWS), stdout=subprocess.PIPE, stderr=subprocess.PIPE))
        p.append(Popen(shlex.split("fstdeterminize" + (delta_param_det if options.delta_det >=0 else ""), posix=not IS_WINDOWS), stdin=p[-1].stdout, stdout=subprocess.PIPE, stderr=subprocess.PIPE))
        p.append(Popen(shlex.split("fstminimize" + (delta_param_min if options.delta_min >=0 else ""), posix=not IS_WINDOWS), stdin=p[-1].stdout, stdout=subprocess.PIPE, stderr=subprocess.PIPE))
        p.append(Popen(shlex.split("fstencode --decode - codex %s/minimized.bin" % TMPDIR, posix=not IS_WINDOWS), 
                       stdin=p[-1].stdout, stdout=subprocess.PIPE, stderr=subprocess.PIPE))
        p[-1].communicate()
    except CalledProcessError as e:
        sys.exit(e.returncode)
    os.unlink("%s/cmd1.bin" % (TMPDIR))
    os.rename("%s/minimized.bin" % TMPDIR, "%s/cmd1.bin" % TMPDIR)

sys.stderr.write("..")
    
## sort
if options.sort:
    try:
        subprocess.check_call(["fstarcsort",
                               "--sort_type=olabel",
                               "%s/cmd1.bin" % TMPDIR,
                               "%s/sorted.bin" % TMPDIR ])
    except CalledProcessError as e:
        sys.exit(e.returncode)
    os.unlink("%s/cmd1.bin" % (TMPDIR))
    os.rename("%s/sorted.bin" % TMPDIR, "%s/cmd1.bin" % TMPDIR)

## keep
if options.keep:
    shutil.copy("%s/cmd1.bin" % TMPDIR, ".")
    
## middle unions
count = 1
for file in args[2:]:
    try:
        subprocess.check_call([options.command,
                                "%s/cmd%d.bin" % (TMPDIR, count),
                                "%s" % file,
                                "%s/cmd%d.bin" % (TMPDIR, count + 1)])
    except CalledProcessError as e:
        sys.exit(e.returncode)

    ## prune
    if options.prune_weight >= 0 or options.prune_states >= 0:
        arguments = ["fstprune",]
        if options.prune_weight >=0:
            arguments.append("--weight=%f" % options.prune_weight)
        if options.prune_states >=0:
            arguments.append("--nstate=%d" % options.prune_states)
        arguments.extend(["%s/cmd%d.bin" % (TMPDIR, count + 1), "%s/pruned.bin" % TMPDIR])
        try:
            subprocess.check_call(arguments)
        except CalledProcessError as e:
            sys.exit(e.returncode)
        os.unlink("%s/cmd%d.bin" % (TMPDIR, count + 1))
        os.rename("%s/pruned.bin" % TMPDIR, "%s/cmd%d.bin" % (TMPDIR, count + 1))
    count += 1

    ## normalize
    if options.alphanorm != "":
        try:
            subprocess.check_call([ "fstalphanormalize.sh",
                                    "%s/cmd%d.bin" % (TMPDIR, count),
                                    "%s" % options.alphanorm,
                                    "%s/normalized.bin" % (TMPDIR) ])
        except CalledProcessError as e:
            sys.exit(e.returncode)
        os.unlink("%s/cmd%d.bin" % (TMPDIR, count + 1))
        os.rename("%s/normalized.bin" % TMPDIR, "%s/cmd%d.bin" % (TMPDIR, count + 1))

    ## minimize
    if options.minimize:
        try:
            delta_param_det = " --delta=%f" % options.delta_det
            delta_param_min = " --delta=%f" % options.delta_min
            p=[]
            p.append(Popen(shlex.split("fstencode --encode_labels %s/cmd%d.bin codex" % (TMPDIR, count), posix=not IS_WINDOWS), stdout=subprocess.PIPE))
            p.append(Popen(shlex.split("fstdeterminize" + (delta_param_det if options.delta_det >=0 else ""), posix=not IS_WINDOWS), stdin=p[-1].stdout, stdout=subprocess.PIPE))
            p.append(Popen(shlex.split("fstminimize" + (delta_param_min if options.delta_min >=0 else ""), posix=not IS_WINDOWS), stdin=p[-1].stdout, stdout=subprocess.PIPE))
            p.append(Popen(shlex.split("fstencode --decode - codex %s/minimized.bin" % TMPDIR, posix=not IS_WINDOWS), 
                           stdin=p[-1].stdout, stdout=subprocess.PIPE))
            p[-1].communicate()
            
        except CalledProcessError as e:
            sys.exit(e.returncode)
        os.unlink("%s/cmd%d.bin" % (TMPDIR, count))
        os.rename("%s/minimized.bin" % TMPDIR, "%s/cmd%d.bin" % (TMPDIR, count))

    ## sort
    if options.sort:
        try:
            subprocess.check_call(["fstarcsort",
                                   "--sort_type=olabel",
                                   "%s/cmd%d.bin" % (TMPDIR, count), 
                                   "%s/sorted.bin" % TMPDIR ])
        except CalledProcessError as e:
            sys.exit(e.returncode)
        os.unlink("%s/cmd%d.bin" % (TMPDIR, count))
        os.rename("%s/sorted.bin" % TMPDIR, "%s/cmd%d.bin" % (TMPDIR, count))

    ## keep
    if options.keep:
        shutil.copy("%s/cmd%d.bin" % (TMPDIR, count), ".")

    sys.stderr.write(".")
    sys.stderr.flush()
    
    
## output last union
subprocess.call(["cat", "%s/cmd%d.bin" % (TMPDIR, count)])

## delete files
if options.delete:
    for file in args:
        os.unlink(file)

sys.stderr.write("\n")        
