#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <fst/fstlib.h>

//DEFINE_int32(v, 10, "v");

using namespace fst;

typedef StdArc Arc;


int main(int argc, char** argv) {
  if (argc<3) {
    cerr << "USAGE: " << argv[0] << " <fst1> <fst2> ... <fstn>" << endl;
    return 1;
  }

  VectorFst<Arc> *model[argc-1];
  for (int i=1;i<argc;i++) {
    model[i-1] = VectorFst<Arc>::Read(argv[i]);
  }

  ComposeFstOptions<Arc> opts;
  opts.gc_limit=10000000000000;

  cerr << "Composing...";
  ComposeFst<Arc> *comp[argc-2];
  comp[0] = new ComposeFst<Arc>(*model[0], *model[1], opts);
  
  for (int i=3;i<argc;i++) {
    comp[i-2] = new ComposeFst<Arc>(*comp[i-3], *model[i-1], opts);
  }
  cerr << "done." << endl;
  
  cerr << "Encoding...";
  EncodeMapper<Arc> encoder(kEncodeLabels, ENCODE);
  EncodeFst<Arc> ifst(*comp[argc-3], &encoder);
  cerr << "done." << endl;

  cerr << "Determinizing...";  
  DeterminizeFst<Arc> det(ifst);
  cerr << "done." << endl;

  //VectorFst<Arc> minfst;  
  //cerr << "Minimizing...";    
  //Minimize(det, &minfst);
  //cerr << "done." << endl;

  cerr << "Decoding...";    
  DecodeFst<Arc> dec(det, encoder);
  cerr << "done." << endl;

  cerr << "Expanding...";      
  VectorFst<Arc> ofst(dec);  
  cerr << "done." << endl;
  
  ofst.Write("");

}
