/*
 * pdtintersect.cpp
 *
 *  Created on: 18 Oct 2012
 *      Author: rfs
 */

#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <set>
#include "fst_library.h"
#include "fst/matcher.h"
#include "fst/encode.h"
#define DEBUG true

using namespace fst;

typedef StdArc Arc;
//typedef VectorFst<Arc> F;

map< pair<int64, int64>, int > parenMap;

void initializeMap(vector<pair<int64, int64> > parens) {
  int maxIndex = floor(sqrt(parens.size())); // we can maximally map sqrt parentheses
  if (DEBUG) cerr << "MaxIndex" << maxIndex << endl;
  int current = maxIndex;
  for (int i=0; i<maxIndex; i++) {
    for (int j=0; j<maxIndex; j++) {
      if (i==j) {
	parenMap[pair<int64, int64>(parens[i].first, parens[j].first)] = parens[i].first;
	parenMap[pair<int64, int64>(parens[i].second, parens[j].second)] = parens[i].second;
      } else {
	parenMap[pair<int64, int64>(parens[i].first, parens[j].first)] = parens[current].first;
	parenMap[pair<int64, int64>(parens[i].second, parens[j].second)] = parens[current].second;
	current++;
      }
    }
  }
}

void printMap(map< pair<int64, int64>, int > themap) {
  for (map< pair<int64, int64>, int >::iterator pos = themap.begin(); pos != themap.end(); ++pos) {
    cerr << "(" << pos->first.first << "," << pos->first.second << ") -> " << pos->second << endl;
  }
}

int findInParentheses(int64 query, vector<pair<int64, int64> > parens) {
  for (int i=0; i<parens.size(); i++) {
    if (parens[i].first == query) 
      return 1;
    else if (parens[i].second == query)
      return 2;
  }
  return 0;
}

int mapParentheses(int64 paren1, int64 paren2, vector<pair<int64, int64> > parens) {
  // ( ( -> (          100 100 -> 100
  // ( [ -> {          100 102 -> 104
  // [ ( -> <          102 100 -> 106
  // [ [ -> [          102 102 -> 102

  if ((paren1 == 0) && (paren2==0))
    cerr << "ERROR: Both parens are epsilon!" << endl;

  // treat epsilons
  if (paren1 == 0)
    paren1 = paren2;
  else if (paren2 == 0)
    paren2 = paren1;

  // first check if one is opening and one is closing
  int find1 = findInParentheses(paren1, parens);
  int find2 = findInParentheses(paren2, parens);
  if (find1 == 0 || find2 == 0) { // one of the two was not found
    cerr << "ERROR: One of the two parens to be mapped is not in the parentheses file" << endl;
    return -1;
  } else if (find1 != find2) { // one is opening, the other is closing
    cerr << "ERROR: One of the two parens to be mapped is opening, the other is closing" << endl;
    return -1;
  } else {
    return parenMap[pair<int64, int64>(paren1, paren2)];
  }
}

int main(int argc, char** argv) {
  if (argc!=4) {
    cerr << "USAGE: " << argv[0] << " <in:pdt1> <in:pdt2> <in:parentheses_file>" << endl;
    return 1;
  }

  // Reads in the pdts
  VectorFst<Arc>* input1 = VectorFst<Arc>::Read(argv[1]);
  if (input1==NULL) {
    cerr << "ERROR reading pdt file 1. Exiting." << endl;
    return 1;
  } else {
    if (DEBUG) cerr << "Succesfully read pdt file." << endl;
  }

  VectorFst<Arc>* input2 = VectorFst<Arc>::Read(argv[2]);
  if (input2==NULL) {
    cerr << "ERROR reading pdt file 1. Exiting." << endl;
    return 1;
  } else {
    if (DEBUG) cerr << "Succesfully read pdt file." << endl;
  }

  // Read in the parentheses file
  vector<pair<int64, int64> > parens;
  ReadLabelPairs(argv[3], &parens, false);
  initializeMap(parens);
  if (DEBUG) printMap(parenMap);

  // first some sanity checks
  // count states
  long state_count1=0;
  for (StateIterator< VectorFst<Arc> > siter1(*input1); !siter1.Done(); siter1.Next()) {
	  state_count1++;
  }
  long state_count2=0;
  for (StateIterator< VectorFst<Arc> > siter2(*input2); !siter2.Done(); siter2.Next()) {
	  state_count2++;
  }

  if (DEBUG) cerr << "States counted: " << state_count1 << "/" << state_count2 << endl;

  // now replace the output parentheses of pdt1 and the input parentheses of pdt2 with the gap symbol
  // we assume the gap symbol to have id 0 (always the case for openfst)
  // PDT1
  //input1->SetInputSymbols(NULL);
  //input1->SetOutputSymbols(NULL);

  for (StateIterator< VectorFst<Arc> > siter(*input1); !siter.Done(); siter.Next()) {
    Arc::StateId state_id = siter.Value();
    //cerr << "Current outer state: " << state_id1 << endl;
    MutableArcIterator< VectorFst<Arc> > aiter(input1, state_id);
    for (; !aiter.Done(); aiter.Next()) {
      Arc arc = aiter.Value();
      if (findInParentheses(arc.olabel, parens)) {
	arc.olabel = 0; 
	aiter.SetValue(arc);
      }
    }
  }

  //input2->SetInputSymbols(NULL);
  //input2->SetOutputSymbols(NULL);
  // PDT2
  for (StateIterator< VectorFst<Arc> > siter(*input2); !siter.Done(); siter.Next()) {
    Arc::StateId state_id = siter.Value();
    //cerr << "Current outer state: " << state_id1 << endl;
    MutableArcIterator< VectorFst<Arc> > aiter(input2, state_id);
    for (; !aiter.Done(); aiter.Next()) {
      Arc arc = aiter.Value();
      if (findInParentheses(arc.ilabel, parens)) {
	arc.ilabel = 0; 
	aiter.SetValue(arc);
      }
    }
  }

  // Compose the two PDTs via normal fst composition
  ArcSort(input1, OLabelCompare<Arc>());
  ComposeOptions opts(true, MATCH_FILTER);
  VectorFst<Arc> result;
  Compose<Arc>(*input1, *input2, &result, opts);

  // Now traverse the PDT, if an open bracket is together with a closing bracket throw an error
  // else encode the two opening or two closing brackets into a new bracket according to the following
  // scheme: 
  //
  // ( ( -> (          100 100 -> 100
  // ( [ -> {          100 102 -> 104
  // [ ( -> <          102 100 -> 106
  // [ [ -> [          102 102 -> 102
  //
  // (closing brackets accordingly)

  for (StateIterator< VectorFst<Arc> > siter(result); !siter.Done(); siter.Next()) {
    Arc::StateId state_id = siter.Value();
    //cerr << "Current outer state: " << state_id1 << endl;
    MutableArcIterator< VectorFst<Arc> > aiter(&result, state_id);
    for (; !aiter.Done(); aiter.Next()) {
      Arc arc = aiter.Value();
      if ((findInParentheses(arc.ilabel, parens) && findInParentheses(arc.olabel, parens)) ||
	  (findInParentheses(arc.ilabel, parens) && arc.olabel==0) || 
	  (arc.ilabel==0 && findInParentheses(arc.olabel, parens))) { // both parens (or one epsilon)
	int newlabel = mapParentheses(arc.ilabel, arc.olabel, parens);
	if (newlabel<0) {
	  cerr << "ERROR: mapping parens failed." << endl;
	  return 1;
	}
	arc.ilabel = newlabel;
	arc.olabel = newlabel;
	aiter.SetValue(arc);
      } else if ((findInParentheses(arc.ilabel, parens) && !findInParentheses(arc.olabel, parens)) || (!findInParentheses(arc.ilabel, parens) && findInParentheses(arc.olabel, parens))) { // only one parens
	// throw error
	cerr << "ERROR: Transition found where one of two labels was parenthesis" << endl;
	return 1;
      } else { // no parens, just regular symbols
	// pass
      }
    }
  }

  if (DEBUG) cerr << "...done!" << endl;
  result.Write("");
}
