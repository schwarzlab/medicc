/*
 * fst-library.h
 *
 *  Created on: 6 Sep 2010
 *      Author: rfs
 */

#ifndef FST_LIBRARY_H_
#define FST_LIBRARY_H_

#include <fst/fstlib.h>
#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <set>
#include <map>

DEFINE_int32(v, 10, "v");

using namespace fst;

const float mydelta = 1.0F/(8192.0F*4);

vector<StdArc::Weight> shortest_path(Fst<StdArc>* model, Fst<StdArc>* input, Fst<StdArc>* output, VectorFst<StdArc>* path) {
	StdArc::Weight retval = -1;

	cerr << "Sorting FSTs...";
	ArcSortFst<StdArc, OLabelCompare<StdArc> > input_sorted = ArcSortFst<StdArc, OLabelCompare<StdArc> >(*input, OLabelCompare<StdArc>());
	//ArcSort(model, ILabelCompare<StdArc>());
	ArcSortFst<StdArc, ILabelCompare<StdArc> > output_sorted = ArcSortFst<StdArc, ILabelCompare<StdArc> >(*output, ILabelCompare<StdArc>());
	cerr << "done." << endl;

	// set compose options
	ComposeFstOptions<StdArc> co;
	co.gc_limit=0;

	// Container for composition result.
	cerr << "Composing input with model..." << flush;
	ComposeFst<StdArc> middle = ComposeFst<StdArc>(*input,*model,co);
	//StdVectorFst middle;
	//Compose(*input, *model, &middle);
	cerr << "done." << endl;

	// Compose with output fst
	cerr << "Composing composition with output..." << flush;
	ComposeFst<StdArc> result = ComposeFst<StdArc>(middle,*output,co);
	//StdVectorFst result;
	//Compose(middle, *output, &result);
	cerr << "...done." << endl;

	cerr << "Computing shortest path..." << flush;
	vector<StdArc::Weight> distance;

	NaturalShortestFirstQueue<StdArc::StateId, StdArc::Weight> state_queue(distance);
	ShortestPathOptions<StdArc, NaturalShortestFirstQueue<StdArc::StateId, StdArc::Weight>, AnyArcFilter<StdArc> > opts(&state_queue, AnyArcFilter<StdArc>(), 1, false, false, mydelta);
	opts.first_path=true;
	// Write shortest path

	ShortestPath(result, path, &distance, opts);
	//ShortestPath(result, &shortestPath);
	//if(shortestPath.Write(cout, FstWriteOptions()))
	//  cerr << "done." << endl;

	cerr << "...done!" << endl;
	return distance;
}

void nshortest_path(VectorFst<StdArc>* model, VectorFst<StdArc>* input, VectorFst<StdArc>* output, VectorFst<StdArc>* path, int nshortest, StdArc::Weight weight) {
	StdArc::Weight retval = -1;

	cerr << "Sorting FSTs...";
	ArcSort(input, OLabelCompare<StdArc>());
	ArcSort(model, ILabelCompare<StdArc>());
	ArcSort(output, ILabelCompare<StdArc>());
	cerr << "done." << endl;

	// set compose options
	ComposeFstOptions<StdArc> co;
	//co.gc_limit=0;

	// Container for composition result.
	cerr << "Composing input with model..." << flush;
	ComposeFst<StdArc> middle = ComposeFst<StdArc>(*input,*model,co);
	//StdVectorFst middle;
	//Compose(*input, *model, &middle);
	cerr << "done." << endl;

	// Compose with output fst
	cerr << "Composing composition with output..." << flush;
	ComposeFst<StdArc> result = ComposeFst<StdArc>(middle,*output,co);
	//StdVectorFst result;
	//Compose(middle, *output, &result);
	cerr << "...done." << endl;

	cerr << "Computing shortest path..." << flush;
	vector<StdArc::Weight> distance;

	NaturalShortestFirstQueue<StdArc::StateId, StdArc::Weight> state_queue(distance);
	ShortestPathOptions<StdArc, NaturalShortestFirstQueue<StdArc::StateId, StdArc::Weight>, AnyArcFilter<StdArc> >
		opts(&state_queue, AnyArcFilter<StdArc>(), nshortest, false, false, mydelta);
	opts.first_path=false;
	opts.weight_threshold=weight;
	// Write shortest path

	ShortestPath(result, path, &distance, opts);
	//ShortestPath(result, &shortestPath);
	//if(shortestPath.Write(cout, FstWriteOptions()))
	//  cerr << "done." << endl;

	cerr << "...done!" << endl;

}

StdArc::Weight align(VectorFst<StdArc>* model, VectorFst<StdArc>* input, VectorFst<StdArc>* output) {
	VectorFst<StdArc> path;
	vector<StdArc::Weight> distance;
	shortest_path(model, input, output, &path);
	StdArc::StateId start = path.Start();
	ShortestDistance(path,&distance,true);

	if ((start<0) || (distance.size()<1)) {
	  return -1;
	} else {
	  return distance[start];
	} 
}

double align(VectorFst<LogArc>* model, VectorFst<LogArc>* input1, VectorFst<LogArc>* input2);

// -----------------------------------------------------------------------------------------------------

template <typename Arctpl> vector<Arctpl> get_next_transitions(set<typename Arctpl::StateId> &state_ids, VectorFst<Arctpl>* fst) {
  vector<Arctpl> transitions;
  //cerr << "state_ids.size:" << state_ids.size() << endl;

  for (typename set<typename Arctpl::StateId>::iterator it=state_ids.begin(); it != state_ids.end(); it++) {
    for (ArcIterator<VectorFst<Arctpl> > aiter(*fst, *it); !aiter.Done(); aiter.Next()) {
      const Arctpl &arc = aiter.Value();
      transitions.push_back(arc);
    }
  }
  //cerr << "transitions.size:" << transitions.size() << endl;
  return transitions;
}

template <typename Arctpl> vector<Arctpl> get_next_transitions(typename Arctpl::StateId state_id, VectorFst<Arctpl>* fst) {
  set<typename Arctpl::StateId> state_ids;
  state_ids.insert(state_id);
  return get_next_transitions<Arctpl>(state_ids, fst);
}

template <typename Arctpl> void decode(VectorFst<Arctpl>* fst_in, VectorFst<Arctpl>* fst_out) {
  fst_out->AddState();   // 1st state will be state 0 (returned by AddState)
  fst_out->SetStart(0);      // arg is state ID
  vector<typename Arctpl::Weight> distance;
  ShortestDistance(*fst_in, &distance);

  typename Arctpl::StateId fout_last_id = 0;
  typename Arctpl::StateId fout_next_id = 0;

  set<typename Arctpl::StateId> current_states;
  current_states.insert(fst_in->Start());
  set<typename Arctpl::StateId> next_states;

  while (current_states.size() > 0) {
    for (typename set<typename Arctpl::StateId>::iterator it = current_states.begin(); it != current_states.end(); it++) {
    	vector<Arctpl> transitions = get_next_transitions(*it, fst_in);
    	if (transitions.size() > 0 && fout_next_id == fout_last_id) { // transitions found and not new state added yet
    		fout_next_id = fst_out->AddState();
    	}
    	for (int i=0; i<transitions.size(); i++) {
    		typename Arctpl::Weight fout_new_weight = Times(transitions[i].weight, distance[*it]);
    		Arctpl fout_arc = Arctpl(transitions[i].ilabel, transitions[i].olabel, fout_new_weight, fout_next_id);
    		//cerr << "New Arc: ("<< fout_last_id << "->" << fout_next_id << ", " << fst_in->InputSymbols()->Find(fout_arc.ilabel) << ":" << fst_in->OutputSymbols()->Find(fout_arc.olabel) << " / " << transitions[i].weight << " + " << distance[*it] << " = " << fout_new_weight << ")" << endl;
    		fst_out->AddArc(fout_last_id, fout_arc);
    		next_states.insert(transitions[i].nextstate);
    	}
    }
    fout_last_id = fout_next_id;
    current_states.clear();
    current_states.swap(next_states);
  }

  fst_out->SetFinal(fout_last_id, Arctpl::Weight::One());
  fst_out->SetInputSymbols(fst_in->InputSymbols());
  fst_out->SetOutputSymbols(fst_in->OutputSymbols());
}



template <typename Arctpl> void normalize_alphabet(VectorFst<Arctpl>* fst) {
	// assumes Std or LogArcs without checking
	// also assumes fst to be input sorted (better check if sorted and sort otherwise)

	const SymbolTable* isyms = fst->InputSymbols();
	if (!isyms) {
		cerr << "No input symbol table, exiting!" << endl;
		exit(1);
	}

	// iterate over all states
	for (StateIterator< VectorFst<Arctpl> > siter(*fst); !siter.Done(); siter.Next()) {
		typename Arctpl::StateId state_id = siter.Value();
		map<int64, typename Arctpl::Weight> sums;

		/* SymbolTableIterator tableiter = SymbolTableIterator(*isyms); */
		/* for (; !tableiter.Done(); tableiter.Next()) { */
		/* 	int64 label = tableiter.Value(); */

		/* 	Matcher< VectorFst<Arctpl> > matcher(*fst, MATCH_INPUT); */
		/* 	matcher.SetState(state_id); */
		/* 	typename Arctpl::Weight weight_sum = Arctpl::Weight::Zero(); */
		/* 	// first determine sum,... */
		/* 	if (matcher.Find(label)) { */
		/* 		for (; !matcher.Done(); matcher.Next()) { */
		/* 			const Arctpl &arc = matcher.Value(); */
		/* 			weight_sum = Plus(weight_sum, arc.weight); */
		/* 		} */
		/* 	} */
		/* 	sums[label] = weight_sum; */
		/* 	//cerr << "label " << isyms->Find(label) << " at state " << state_id << " has sum: " */
		/* 	//		<< sums[label] << " (" << exp(-sums[label].Value()) << ")" << endl; */
		/* } */

		SymbolTableIterator tableiter = SymbolTableIterator(*isyms);
		for (; !tableiter.Done(); tableiter.Next()) { 
		  int64 label = tableiter.Value();
		  sums[label] = Arctpl::Weight::Zero();
		}

		MutableArcIterator< VectorFst<Arctpl> > aiter_count(fst, state_id);
		for (; !aiter_count.Done(); aiter_count.Next()) {
			Arctpl arc = aiter_count.Value();
			sums[arc.ilabel] = Plus(sums[arc.ilabel], arc.weight);
		}

		// ...then normalize
		MutableArcIterator< VectorFst<Arctpl> > aiter(fst, state_id);
		for (; !aiter.Done(); aiter.Next()) {
			Arctpl arc = aiter.Value();
			if (sums[arc.ilabel] != Arctpl::Weight::Zero()) {
				arc.weight = Divide(arc.weight, sums[arc.ilabel]);
				aiter.SetValue(arc);
			}
		}
	}
}

// Randomly generate paths through an FST; details controlled by
// RandGenOptions.
template<class Arc, class ArcSelector>
void RandGenWeighted(const Fst<Arc> &ifst, MutableFst<Arc> *ofst,
	     const RandGenOptions<ArcSelector> &opts) {
  typedef typename Arc::Weight Weight;

  if (opts.npath == 0 || opts.max_length == 0 || ifst.Start() == kNoStateId)
    return;

  if (opts.source == kNoStateId) {   // first call
    ofst->DeleteStates();
    ofst->SetInputSymbols(ifst.InputSymbols());
    ofst->SetOutputSymbols(ifst.OutputSymbols());
    ofst->SetStart(ofst->AddState());
    RandGenOptions<ArcSelector> nopts(opts);
    nopts.source = ifst.Start();
    nopts.dest = ofst->Start();
    for (; nopts.npath > 0; --nopts.npath)
      RandGenWeighted(ifst, ofst, nopts);
  } else {
    if (ifst.NumArcs(opts.source) == 0 &&
	ifst.Final(opts.source) == Weight::Zero())  // Non-coaccessible
      return;
    // Pick a random transition from the source state
    size_t n = opts.arc_selector(ifst, opts.source);
    if (n == ifst.NumArcs(opts.source)) {  // Take 'super-final' transition
      ofst->SetFinal(opts.dest, Weight::One());
    } else {
      ArcIterator< Fst<Arc> > aiter(ifst, opts.source);
      aiter.Seek(n);
      const Arc &iarc = aiter.Value();
      Arc oarc(iarc.ilabel, iarc.olabel, iarc.weight, ofst->AddState());
      ofst->AddArc(opts.dest, oarc);

      RandGenOptions<ArcSelector> nopts(opts);
      nopts.source = iarc.nextstate;
      nopts.dest = oarc.nextstate;
      --nopts.max_length;
      RandGenWeighted(ifst, ofst, nopts);
    }
  }
}


// Randomly generate a path through an FST with the uniform distribution
// over the transitions.
template<class Arc>
void RandGenWeighted(const Fst<Arc> &ifst, MutableFst<Arc> *ofst) {
  UniformArcSelector<Arc> uniform_selector;
  RandGenOptions< UniformArcSelector<Arc> > opts(uniform_selector);
  RandGenWeighted(ifst, ofst, opts);
}

//template <class A>
//struct OffsetMapper {
//  typedef typename A::Weight Weight;
//
//  explicit OffsetMapper(Weight w) : weight_(w) {}
//
//  A operator()(const A &arc) const {
//    if (arc.weight == Weight::Zero())
//      return arc;
//    Weight w = Plus(arc.weight, weight_);
//    return A(arc.ilabel, arc.olabel, w, arc.nextstate);
//  }
//
//  MapFinalAction FinalAction() const { return MAP_NO_SUPERFINAL; }
//
//  MapSymbolsAction InputSymbolsAction() const { return MAP_COPY_SYMBOLS; }
//
//  MapSymbolsAction OutputSymbolsAction() const { return MAP_COPY_SYMBOLS;}
//
//  uint64 Properties(uint64 props) const {
//    return props & kWeightInvariantProperties;
//  }
//
// private:
//
//  Weight weight_;
//};

struct OffsetMapper {

  explicit OffsetMapper(float w) : offset_(w) {}

  StdArc operator()(const StdArc &arc) const {
	  if (arc.nextstate == kNoStateId && arc.weight != StdArc::Weight::Zero() ) { // final state
		  return StdArc(arc.ilabel, arc.olabel, -offset_, arc.nextstate);
	  }

//	  if (arc.weight != StdArc::Weight::Zero) {
//		  float new_weight = abs ( arc.weight -  offset_ * arc.weight / pathlength_ );
//		  return StdArc(arc.ilabel, arc.olabel, -weight_, arc.nextstate);
//	  }

  	  return arc;
  }

  MapFinalAction FinalAction() const { return MAP_NO_SUPERFINAL; }

  MapSymbolsAction InputSymbolsAction() const { return MAP_COPY_SYMBOLS; }

  MapSymbolsAction OutputSymbolsAction() const { return MAP_COPY_SYMBOLS;}

  uint64 Properties(uint64 props) const {
    return props & kWeightInvariantProperties;
  }

 private:

  float offset_;
  float pathlength_;
};

// Absolute Tropical semiring: (abs(min), +, inf, 0)
template <class T>
class AbsTropicalWeightTpl : public FloatWeightTpl<T> {
 public:
  using FloatWeightTpl<T>::Value;

  typedef AbsTropicalWeightTpl<T> ReverseWeight;

  AbsTropicalWeightTpl() : FloatWeightTpl<T>() {}

  AbsTropicalWeightTpl(T f) : FloatWeightTpl<T>(f) {}

  AbsTropicalWeightTpl(const TropicalWeightTpl<T> &w) : FloatWeightTpl<T>(w) {}

  static const AbsTropicalWeightTpl<T> Zero() {
    return TropicalWeightTpl<T>(FloatLimits<T>::PosInfinity()); }

  static const AbsTropicalWeightTpl<T> One() {
    return TropicalWeightTpl<T>(0.0F); }

  static const string &Type() {
    static const string type = "absolute tropical" +
        FloatWeightTpl<T>::GetPrecisionString();
    return type;
  }

  bool Member() const {
    // First part fails for IEEE NaN
    return Value() == Value() && Value() != FloatLimits<T>::NegInfinity();
  }

  AbsTropicalWeightTpl<T> Quantize(float delta = kDelta) const {
    if (Value() == FloatLimits<T>::NegInfinity() ||
        Value() == FloatLimits<T>::PosInfinity() ||
        Value() != Value())
      return *this;
    else
      return AbsTropicalWeightTpl<T>(floor(Value()/delta + 0.5F) * delta);
  }

  AbsTropicalWeightTpl<T> Reverse() const { return *this; }

  static uint64 Properties() {
    return kLeftSemiring | kRightSemiring | kCommutative |
        kPath | kIdempotent;
  }
};

// Single precision TropicalWeight
typedef AbsTropicalWeightTpl<float> AbsTropicalWeight;

template <class T>
inline AbsTropicalWeightTpl<T> Plus(const AbsTropicalWeightTpl<T> &w1,
                                 const AbsTropicalWeightTpl<T> &w2) {
	cerr << "comparing " << w1.Value() << " with " << w2.Value() << ", returning " << (abs(w1.Value()) < abs(w2.Value()) ? w1 : w2) << endl;
  return abs(w1.Value()) < abs(w2.Value()) ? w1 : w2;
}

inline AbsTropicalWeightTpl<float> Plus(const AbsTropicalWeightTpl<float> &w1,
                                     const AbsTropicalWeightTpl<float> &w2) {
  return Plus<float>(w1, w2);
}

inline AbsTropicalWeightTpl<double> Plus(const AbsTropicalWeightTpl<double> &w1,
                                      const AbsTropicalWeightTpl<double> &w2) {
  return Plus<double>(w1, w2);
}

template <class T>
inline AbsTropicalWeightTpl<T> Times(const AbsTropicalWeightTpl<T> &w1,
                                  const AbsTropicalWeightTpl<T> &w2) {
  T f1 = w1.Value(), f2 = w2.Value();
  if (f1 == FloatLimits<T>::PosInfinity())
    return w1;
  else if (f2 == FloatLimits<T>::PosInfinity())
    return w2;
  else
    return AbsTropicalWeightTpl<T>(f1 + f2);
}

inline AbsTropicalWeightTpl<float> Times(const AbsTropicalWeightTpl<float> &w1,
                                      const AbsTropicalWeightTpl<float> &w2) {
  return Times<float>(w1, w2);
}

inline AbsTropicalWeightTpl<double> Times(const AbsTropicalWeightTpl<double> &w1,
                                       const AbsTropicalWeightTpl<double> &w2) {
  return Times<double>(w1, w2);
}

template <class T>
inline AbsTropicalWeightTpl<T> Divide(const AbsTropicalWeightTpl<T> &w1,
                                   const AbsTropicalWeightTpl<T> &w2,
                                   DivideType typ = DIVIDE_ANY) {
  T f1 = w1.Value(), f2 = w2.Value();
  if (f2 == FloatLimits<T>::PosInfinity())
    return FloatLimits<T>::NumberBad();
  else if (f1 == FloatLimits<T>::PosInfinity())
    return FloatLimits<T>::PosInfinity();
  else
    return TropicalWeightTpl<T>(f1 - f2);
}

inline AbsTropicalWeightTpl<float> Divide(const AbsTropicalWeightTpl<float> &w1,
                                       const AbsTropicalWeightTpl<float> &w2,
                                       DivideType typ = DIVIDE_ANY) {
  return Divide<float>(w1, w2, typ);
}

inline AbsTropicalWeightTpl<double> Divide(const AbsTropicalWeightTpl<double> &w1,
                                        const AbsTropicalWeightTpl<double> &w2,
                                        DivideType typ = DIVIDE_ANY) {
  return Divide<double>(w1, w2, typ);
}

typedef ArcTpl<AbsTropicalWeight> AbsStdArc;

#endif /* FST_LIBRARY_H_ */
