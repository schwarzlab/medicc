#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <fst/fstlib.h>

using namespace fst;

typedef LogArc Arc;
//typedef ArcTpl<LogWeightTpl<double> > Arc;

int FLAGS_v=2;
const float mydelta = 1.0F/(8192.0F*4);
//const double mydelta = 1.0L/(8192.0L*4);

int main(int argc, char** argv) {
  if (argc!=4) {
    cerr << "USAGE: " << argv[0] << " <in:model_fst> <in:input_fst> <in:output_fst>" << endl;
    return 1;
  }

  cerr << "------------------------------------------------------------------------------------------" << endl;
  cerr << " Aligning " << argv[2] << " to " << argv[3] << "." << endl;
  cerr << "------------------------------------------------------------------------------------------" << endl;
  
  // Reads in the transduction model. 
  VectorFst<Arc> *model = VectorFst<Arc>::Read(argv[1]);
  if (model==NULL) {
    cerr << "ERROR reading model file. Exiting." << endl;
    return 1;
  } else {
    cerr << "Succesfully read model file." << endl;
  }
  
  // Reads in an input FST. 
  VectorFst<Arc> *input = VectorFst<Arc>::Read(argv[2]);
  if (input==NULL) {
    cerr << "ERROR reading input file. Exiting." << endl;
    return 1;
  } else {
    cerr << "Succesfully read input file." << endl;
  }

  // Reads in the output FST
  VectorFst<Arc> *output = VectorFst<Arc>::Read(argv[3]);
  if (output==NULL) {
    cerr << "ERROR reading output file. Exiting." << endl;
    return 1;
  } else {
    cerr << "Succesfully read output file." << endl;
  }

  cerr << "Sorting FSTs...";
  ArcSort(input, OLabelCompare<Arc>());
  ArcSort(model, ILabelCompare<Arc>());
  ArcSort(output, ILabelCompare<Arc>());
  cerr << "done." << endl;

  // set compose options
  ComposeFstOptions<Arc> co;
  co.gc_limit=0;
  co.gc=true;

  // Container for composition result. 
  cerr << "Composing input with model..." << flush;
  ComposeFst<Arc> middle = ComposeFst<Arc>(*input,*model,co);
  //VectorFst<Arc> middle;
  //Compose(*input, *model, &middle);
  cerr << "done." << endl;

  // Compose with output fst
  cerr << "Composing composition with output..." << flush;
  ComposeFst<Arc> result = ComposeFst<Arc>(middle,*output,co);
  //VectorFst<Arc> result;
  //Compose(middle, *output, &result);
  cerr << "...done." << endl;

  cerr << "Computing shortest distance..." << flush;
  vector<Arc::Weight> distance;

  //NaturalShortestFirstQueue<Arc::StateId, Arc::Weight> state_queue(distance);
  //AutoQueue<Arc::StateId> state_queue(result, &distance, AnyArcFilter<Arc>());
  //ShortestDistanceOptions<Arc, NaturalShortestFirstQueue<Arc::StateId, Arc::Weight>, AnyArcFilter<Arc> > opts(&state_queue, AnyArcFilter<Arc>());
  //opts.first_path=false;
  // Write shortest path
  //VectorFst<Arc> shortestPath;
  //ShortestPath(result, &shortestPath, &distance, opts);
  //TopOrderQueue<Arc::StateId, Arc
  ShortestDistance(result, &distance, true, mydelta);
  //ShortestDistance(result, &distance, opts);
  std::cout << std::fixed << std::setprecision(8) << distance[0];
  //if(shortestPath.Write(cout, FstWriteOptions()))
  //  cerr << "done." << endl;

  cerr << "------------------------------------------------------------------------------------------" << endl;
  
}
