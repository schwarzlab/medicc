source("~/Documents/Projects/r-functions-source/trunk/lib-kernel.r")
source("~/Documents/Projects/r-functions-source/trunk/lib-general.r")
source("~/Documents/Projects/r-functions-source/trunk/lib-transducer.r")

args=commandArgs(TRUE)

scoreIsDistance = FALSE


arg = args[1]
if (length(args) > 1) {
  if (args[2] == "-d") {
    scoreIsDistance = TRUE
  }
}


url=strsplit(arg,"/")[[1]]
filename=url[length(url)]
pathname=paste(url[-length(url)],collapse="/")


mat=readMatrix(paste(pathname,filename,sep="/"))

if (!scoreIsDistance) {
  kernmat=to.kernmat(mat)
  ##eigen(kernmat)$values
  kernnorm=kernel.norm(kernmat)
  kerndist=kernel.dist(kernmat)
  kerndistnorm=kernel.dist(kernnorm)
} else {
  kerndist = as.dist(mat)
  kerndistnorm = NULL
}
  ##distances = as.matrix(kerndist)
  ##distances = exp(distances)-1
  ##writeLines(as.phylip.dist(as.dist(distances)), sprintf("%s/kern_exp.dist", pathname))

  ##kernproj = projectToPSD(kernmat)
  ##kernprojdist = kernel.dist(kernproj)

writeLines(as.phylip.dist(kerndist),sprintf("%s/kern.dist",pathname))
save(kerndist, file = sprintf("%s/kerndist.rsave",pathname))

if (!is.null(kerndistnorm)) {
  writeLines(as.phylip.dist(kerndistnorm),sprintf("%s/kernnorm.dist",pathname))
}

##writeLines(as.phylip.dist(kerndistsquare),sprintf("%s/kernsquare.dist",pathname))
##writeLines(as.phylip.dist(kernprojdist),sprintf("%s/kernproj.dist",pathname))
