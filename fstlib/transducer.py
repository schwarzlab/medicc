#!/usr/bin/python
""" Pipeline for rational kernels """

import sys
import subprocess
from subprocess import CalledProcessError
from subprocess import Popen
import glob
import shutil
from optparse import OptionParser

INSTALL_PATH = "/home/rfs/workspace/transducer-tools/trunk/src"

usage = "USAGE: %prog <fasta_file> <output_dir>"
parser = OptionParser(usage=usage)

parser.add_option("-t","--use-tropical", action = "store_true", dest = "use_tropical", default = False, 
                  help = "use tropical semiring (default is log)?")
parser.add_option("-f", "--fst-binary", action = "store", type = "string", dest = "fst_binary", default = "align_dna_new.fst.bin", 
                  help = "path to the fst binary used for aligning")
parser.add_option("-s", "--fst-symbols", action = "store", type = "string", dest = "fst_symbols", default = "align_dna_new.fst.symbols", 
                  help = "path to the fst symbols file")
parser.add_option("-d", "--is-distance", action = "store_true", dest = "is_distance", default = False, 
                  help = "the FST score is already a distance, don't convert")


(options, args) = parser.parse_args()

if len(args) != 2:
    parser.error("incorrect number of arguments")
    
if options.use_tropical:
    ARC_TYPE = "standard"
    ALIGN_BINARY = INSTALL_PATH + "/fst_align_std"
else:
    ARC_TYPE = "log"
    ALIGN_BINARY = INSTALL_PATH + "/fst_align_log"

FST_BINARY = options.fst_binary
##FST_BINARY = "trained.fst.bin"
SYMBOL_FILE = options.fst_symbols

FASTA2FSA_SCRIPT = INSTALL_PATH + "/fasta2fsa.py"
KERNMAT_NAME = "kernel_scores.txt"
RSCRIPT = INSTALL_PATH + "/analyse_scores.r"

infile = args[0]
outdir = args[1]

print "Using fst binary: %s" %  FST_BINARY

# step 1: create sequence FSAs
print "Creating sequence FSAs...",
p1 = Popen([FASTA2FSA_SCRIPT, infile, outdir], stdout=subprocess.PIPE)
seqs = p1.communicate()[0]

if p1.returncode!=0:
    sys.exit(p1.returncode)

print "done."

# step 2: compile all FSAs in output dir
print "Compiling sequences...",
files = glob.glob("%s/*.fst" % outdir)
for file in files:
    try:
        subprocess.check_call(["fstcompile",
                               "--arc_type=%s" % ARC_TYPE,
                               "--isymbols=%s" % SYMBOL_FILE,
                               "--osymbols=%s" % SYMBOL_FILE,
                               "--keep_isymbols",
                               "--keep_osymbols",
                               "--acceptor",
                               file,
                               "%s.bin" % file])
    except CalledProcessError as e:
        sys.exit(e.returncode)

print "done."        

# step 3: calc shortest distances
donelist = {}
result = []

seqs = seqs.strip().split("\n")

for s in seqs:
    for t in seqs:
        try:
            doit = donelist[(s,t)]
        except KeyError:
            p1 = Popen([ALIGN_BINARY,
                        FST_BINARY,
                        "%s/%s.bin" % (outdir, s),
                        "%s/%s.bin" % (outdir, t)], stdout=subprocess.PIPE)
            kernel_costs = p1.communicate()[0]
            
            if p1.returncode!=0:
                print ("Error aligning sequences. Exiting.")
                sys.exit(1)
            
            result.append("%s %s %s" % (s, t, kernel_costs))
            print ("Kernel costs: %s" % kernel_costs)
            donelist[(s,t)]=True
            donelist[(t,s)]=True

try:
    with open("%s/%s" % (outdir, KERNMAT_NAME), "w") as fd:
        fd.write("\n".join(result)+"\n")
except IOError:
    print "ERROR: Couldn't write output file %s. Exiting." % KERNMAT_NAME
    sys.exit(1)


# step 4: convert scores to distance matrix
print "Converting scores to distance matrices..."
try:
    subprocess.check_call(["Rscript",
                           "--vanilla",
                           RSCRIPT,
                           "%s/%s" % (outdir, KERNMAT_NAME),
                           "-d" if options.is_distance else ""])
except CalledProcessError as e:
    sys.exit(e.returncode)
print "done."
